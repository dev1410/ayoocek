$(document).ready(function () {
    var process_ajax, curr_val, id, td_id, order_id,
        product_id, name, value, prev_val, last_val,
        text_array, focus_out, field, data,
        checked_icon, change_url, td_remove,
        data_checked, contentEditable, span,
        dash_splitter_id, remark_add_text;
    checked_icon = '<i class="fi-check"></i>';
    change_url = 'order/change';

    $("td").unbind('click').bind('click ', function(){
        if($(this).attr("editable")){

            contentEditable = 'contentEditable';

            curr_val = 'curr-val';
            data_checked = 'data-checked';

            td_remove = function (elem) {
                $(td_id).removeAttr(elem);
            };

            if ($(this).prop(contentEditable))
            {
                // set current value in case update is fail
                if (!$(this).attr(curr_val))
                {
                    $(this).attr(curr_val, $(this).text().trim());
                }

                id = $(this).attr('id');
                td_id = '#'+id;
                name = id.split('-')[0];

                field = name.split('_')[0];

                value = $(this).text();
                order_id = id.split('-')[1];
                data = {};
                focus_out = 'focusout';
                span = $("#span-" + name.split('_')[1] + '-' + order_id);

                if (field == 'product')
                {
                    product_id = id.split('-')[2];

                    var price = $("#product_price-" + order_id + '-' + product_id);
                    var qty = $("#product_quantity-" + order_id + '-' + product_id);
                    var ship_cost = $("#product_shipping_cost-" + order_id + '-' + product_id);
                    var subtotal = $("#product_subtotal-" + order_id + '-' + product_id);
                    var grand_total = $("#order_total-" + order_id);

                    var price_value = parseInt(price.text().trim().replace('.', ''));
                    var qty_value = parseInt(qty.text().trim());
                    var ship_cost_value = parseInt(ship_cost.text().trim().replace('.', ''));
                }

                if ($(this).prop('id') == 'date_created')
                {
                    var date_input_text = $(this).attr('id') + '-input';
                    var date_input_id = '#' + date_input_text;
                    var date_input = '<input type="text" id="' + date_input_text + '" name="' +date_input_text+ '">';

                    order_id = $(this).attr('order_id');
                    name = $(this).attr('id');

                    if ($(date_input_id).length < 1)
                    {
                        $(this).text('');
                        $(date_input).appendTo($(this));

                        $(date_input_id).fdatepicker({
                            format: 'yyyy-mm-dd'
                        });
                    }
                }
                else if(name == 'order_paid')
                {
                    var paid_input_text = $(this).attr('id') + '-input';
                    var paid_input_id = '#' + paid_input_text;
                    var paid_input = '<input type="text" id="' + paid_input_text + '" name="' +paid_input_text+ '">';

                    if ($(this).attr(curr_val))
                    {
                        span.detach();
                        span.appendTo("body");
                        span.hide();
                    }

                    if ($(paid_input_id).length < 1)
                    {
                        $(this).text('');
                        $(paid_input).appendTo($(this));

                        $(paid_input_id).fdatepicker({
                            format: 'yyyy-mm-dd'
                        });
                    }
                }

                if (name == 'order_helpdesk')
                {
                    var help_input = '<input type="search" name="' + name +'" id="' + name +'">';
                    var help_input_selector = $('input[name="'+ name +'"]');

                    if (help_input_selector.length < 1)
                    {
                        $(this).text('');
                        $(help_input).appendTo($(this));
                    }

                    help_input_selector.autocomplete({
                        serviceUrl: base_url + 'order/get_helpdesks_ajax',
                        dataType: 'json',
                        type: 'POST',
                        transformResult: function(response) {
                            return {
                                suggestions: $.map(response.suggestions, function(dataItem) {
                                    return {
                                        value: dataItem.value,
                                        data: dataItem.data
                                    };
                                })
                            };
                        },
                        onSelect: function (suggestion) {
                            $('input[name="'+ name +'"]').remove();

                            $.ajax({
                                url: base_url + change_url,
                                type: 'POST',
                                dataType: 'json',
                                data: {value :suggestion.data, id:order_id, name:name},
                                success: function () {
                                    $(td_id).text(suggestion.value);
                                    td_remove(curr_val);
                                }
                            });
                        }
                    });
                }
                else if (name == 'order_sales')
                {
                    var sales_input = '<input type="search" name="' + name +'" id="' + name +'">';
                    var sales_input_selector = $('input[name="'+ name +'"]');

                    if (sales_input_selector.length < 1)
                    {
                        $(this).text('');
                        $(sales_input).appendTo($(this));
                    }

                    sales_input_selector.autocomplete({
                        serviceUrl: base_url + 'order/get_sales_ajax',
                        dataType: 'json',
                        type: 'POST',
                        transformResult: function(response) {
                            return {
                                suggestions: $.map(response.suggestions, function(dataItem) {
                                    return {
                                        value: dataItem.value,
                                        data: dataItem.data
                                    };
                                })
                            };
                        },
                        onSelect: function (suggestion) {
                            sales_input_selector.remove();

                            $.ajax({
                                url: base_url + change_url,
                                type: 'POST',
                                dataType: 'json',
                                data: {value :suggestion.data, id:order_id, name:name},
                                success: function () {

                                    $(td_id).text(suggestion.value);
                                    td_remove(curr_val);
                                }
                            });
                        }
                    });
                }
                else if (name == 'order_location')
                {
                    var location_input = '<input type="search" name="' + name +'" id="' + name +'">';
                    var location_input_selector = $('input[name="'+ name +'"]');

                    if (location_input_selector.length < 1)
                    {
                        $(this).text('');
                        $(location_input).appendTo($(this));
                    }

                    location_input_selector.autocomplete({
                        serviceUrl: base_url + 'order/package_location',
                        dataType: 'json',
                        type: 'POST',
                        paramName: 'query',
                        transformResult: function(response) {
                            return {
                                suggestions: $.map(response.suggestions, function(dataItem) {
                                    return {
                                        value: dataItem.value,
                                        data: dataItem.data
                                    };
                                })
                            };
                        },
                        onSelect: function (suggestion) {
                            location_input_selector.remove();

                            $.ajax({
                                url: base_url + change_url,
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    value: suggestion.value,
                                    curr_value: prev_val,
                                    id: order_id,
                                    name: name},
                                success: function () {

                                    $(td_id).text(suggestion.value);
                                    td_remove(curr_val);
                                }
                            });
                        }
                    });
                }
                else if(name == 'product_is_received')
                {
                    if ($(this).attr(data_checked))
                    {
                        var received_input = '<input type="checkbox" name="' + td_id +'-input" id="' + td_id +'" checked="checked">';
                    } else {
                        received_input = '<input type="checkbox" name="' + td_id +'-input" id="' + td_id +'">';
                    }
                    var received_input_selector = $('input[name="'+td_id+'-input"]');

                    if (received_input_selector.length < 1)
                     {
                         $(this).text('');
                         $(received_input).appendTo($(this));
                     }
                }
                else if(name == 'product_is_send')
                {
                    $(this).text($(this).attr(curr_val).split(" ")[0]);
                }
                else if (name == 'order_sp' || name == 'order_so' || name == 'order_inv')
                {
                    span.detach();
                    span.appendTo("body");
                    span.hide();

                    $(this).text($(this).attr(curr_val));
                }

                $(this).unbind(focus_out).bind(focus_out, function() {

                    $(this).removeAttr(contentEditable);

                    var text = $(td_id).text();
                    text_array = text.split("");

                    prev_val = $(td_id).attr(curr_val).trim();
                    last_val = $(td_id).text().trim();

                    if (name == 'order_number') {
                        for (var i = 0; i < text_array.length; i++) {
                            if ($.isNumeric(parseInt(text_array[i])) == false) {
                                process_ajax = false;
                                $(td_id).text(parseInt(prev_val));
                                td_remove(curr_val);
                            } else {
                                if (parseInt(last_val) > 0) {
                                    process_ajax = true;
                                } else {
                                    process_ajax = false;
                                    $(td_id).text(parseInt(prev_val));
                                    td_remove(curr_val);
                                }
                            }
                        }
                    }
                    else if (name == 'order_name')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if(name == 'order_quantity')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if ($.isNumeric(parseInt(text_array[i])) == false) {
                                process_ajax = false;
                                $(td_id).text(parseInt(prev_val));
                                td_remove(curr_val);
                            } else {
                                if (parseInt(last_val) > 0) {
                                    process_ajax = true;
                                } else {
                                    process_ajax = false;
                                    $(td_id).text(parseInt(prev_val));
                                    td_remove(curr_val);
                                }
                            }
                        }
                    }
                    else if(name == 'order_total')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if (last_val != prev_val)
                            {
                                if ($.isNumeric(parseInt(text_array[i])) == false) {
                                    process_ajax = false;
                                    $(td_id).text(prev_val);
                                    td_remove(curr_val);
                                } else {
                                    if (parseInt(last_val) > 0) {
                                        process_ajax = true;
                                    } else {
                                        process_ajax = false;
                                        $(td_id).text(prev_val);
                                        td_remove(curr_val);
                                    }
                                }
                            } else {
                                process_ajax = false;
                                $(td_id).text(prev_val);
                                td_remove(curr_val);
                            }
                        }
                    }
                    else if (name == 'order_unit')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'order_unit_address')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'order_description')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'order_location')
                    {
                        if (last_val.length < 5)
                        {
                            $('input[name="'+ name +'"]').remove();
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        }
                    }
                    else if (name == 'order_helpdesk')
                    {
                        if (last_val.length < 1)
                        {
                            $('input[name="'+ name +'"]').remove();
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        }
                    }
                    else if (name == 'order_sales')
                    {
                        if (last_val.length < 1)
                        {
                            $('input[name="'+ name +'"]').remove();
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        }
                    }
                    else if (name == 'pemesan_name')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'pemesan_email')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'pemesan_phone')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if ($.isNumeric(parseInt(text_array[i])) == false) {
                                process_ajax = false;
                                $(td_id).text(prev_val);
                                td_remove(curr_val);
                            } else {
                                if (last_val.length > 0) {
                                    process_ajax = true;
                                } else {
                                    process_ajax = false;
                                    $(td_id).text(parseInt(prev_val));
                                    td_remove(curr_val);
                                }
                            }
                        }
                    }
                    else if (name == 'pembeli_name')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'pembeli_title')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'pembeli_nip')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if ($.isNumeric(parseInt(text_array[i])) == false) {
                                process_ajax = false;
                                $(td_id).text(prev_val);
                                td_remove(curr_val);
                            } else {
                                if (last_val.length > 0) {
                                    process_ajax = true;
                                } else {
                                    process_ajax = false;
                                    $(td_id).text(prev_val);
                                    td_remove(curr_val);
                                }
                            }
                        }
                    }
                    else if (name == 'product_name')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            process_ajax = true;
                        }
                    }
                    else if (name == 'product_price')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if (last_val != prev_val)
                            {
                                if ($.isNumeric(parseInt(text_array[i])) == false) {
                                    process_ajax = false;
                                    $(td_id).text(prev_val);
                                    td_remove(curr_val);
                                } else {

                                    if (parseInt(last_val) > 0) {
                                        process_ajax = true;

                                        data['curr_qty'] = qty_value;
                                        data['curr_ship_cost'] = ship_cost_value;
                                    } else {
                                        process_ajax = false;
                                        $(td_id).text(prev_val);
                                        td_remove(curr_val);
                                    }
                                }
                            } else {
                                process_ajax = false;
                                $(td_id).text(prev_val);
                                td_remove(curr_val);
                            }
                        }
                    }
                    else if (name == 'product_quantity')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if (last_val != prev_val)
                            {
                                if ($.isNumeric(parseInt(text_array[i])) == false) {
                                    process_ajax = false;
                                    $(td_id).text(prev_val);
                                    td_remove(curr_val);
                                } else {
                                    if (parseInt(last_val) > 0) {
                                        process_ajax = true;

                                        data['curr_price'] = price_value;
                                        data['curr_ship_cost'] = ship_cost_value;
                                    } else {
                                        process_ajax = false;
                                        $(td_id).text(prev_val);
                                        td_remove(curr_val);
                                    }
                                }
                            } else {
                                process_ajax = false;
                                $(td_id).text(prev_val);
                                td_remove(curr_val);
                            }
                        }
                    }
                    else if (name == 'product_shipping_cost')
                    {
                        for (i = 0; i < text_array.length; i++) {
                            if (last_val != prev_val)
                            {
                                if ($.isNumeric(parseInt(text_array[i])) == false) {
                                    process_ajax = false;
                                    $(td_id).text(prev_val);
                                    td_remove(curr_val);
                                } else {
                                    if (parseInt(last_val) > 0) {
                                        returnVal = confirm('Yakin data sudah benar ? Proses ini tidak dapat diedit atau dibatalkan');
                                        process_ajax = returnVal;

                                        data['curr_price'] = price_value;
                                        data['curr_qty'] = qty_value;
                                    } else {
                                        process_ajax = false;
                                        $(td_id).text(prev_val);
                                        td_remove(curr_val);
                                    }
                                }
                            } else {
                                process_ajax = false;
                                $(td_id).text(prev_val);
                                td_remove(curr_val);
                            }
                        }
                    }
                    else if (name == 'product_po')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            returnVal = confirm('Yakin data sudah benar ? Proses ini tidak dapat diedit atau dibatalkan')
                            process_ajax = returnVal;
                        }
                    }
                    else if (name == 'product_do')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(td_id).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            returnVal = confirm('Yakin nomor DO sudah benar ? Proses tidak dapat diedit atau dibatalkan');
                            process_ajax = returnVal;
                        }
                    }
                    else if (name == 'product_is_send')
                    {
                        console.log(prev_val);
                        if (last_val.length < 3 || prev_val.split(' ')[0] == last_val)
                        {
                            process_ajax = false;
                            $(this).text(prev_val);
                            td_remove(curr_val);
                        } else {
                            data['value'] = last_val;
                            var returnVal =  confirm('Yakin sudah benar ? Proses tidak dapat diedit atau dibatalkan.');

                            if (returnVal)
                            {
                                var courier = prompt('Nama pengirim ?');
                                if(courier)
                                {
                                    data['courier'] = courier;
                                } else {
                                    $(td_id).text('');
                                    $(td_id).append($(td_id).attr(curr_val));
                                }
                            } else {
                                $(td_id).text('');
                                $(td_id).append(data['value']);
                            }
                            process_ajax = courier;
                        }
                    }
                    else if (name == 'order_sp')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(this).attr(curr_val, last_val);
                            $(this).text('');
                            span.detach();
                            span.appendTo($(this));
                            span.show();
                        } else {
                            returnVal = confirm('Yakin mengubah SP ?');

                            if(!returnVal)
                            {
                                span.detach();
                                $(this).text('');
                                span.appendTo($(this));
                                span.show();
                            } else {
                                span.detach();
                                span.appendTo("body");
                                span.hide();
                            }
                            process_ajax = returnVal;
                        }
                    }
                    else if (name == 'order_so')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(this).attr(curr_val, last_val);
                            $(this).text('');
                            span.detach();
                            span.appendTo($(this));
                            span.show();
                        } else {
                            returnVal = confirm('Yakin data SO sudah benar ? setelah ini tidak dapat diedit atau dibatalkan');

                            if(!returnVal)
                            {
                                span.detach();
                                $(this).text('');
                                span.appendTo($(this));
                                span.show();
                            } else {
                                span.detach();
                                span.appendTo("body");
                                span.hide();
                            }
                            process_ajax = returnVal;
                        }
                    }
                    else if (name == 'order_inv')
                    {
                        if (last_val.length < 3 || prev_val == last_val)
                        {
                            process_ajax = false;
                            $(this).attr(curr_val, last_val);
                            $(this).text('');
                            span.detach();
                            span.appendTo($(this));
                            span.show();
                        } else {
                            returnVal = confirm('Yakin mengubah nomor invoice ?');

                            if(!returnVal)
                            {
                                span.detach();
                                $(this).text('');
                                span.appendTo($(this));
                                span.show();
                            } else {
                                span.detach();
                                span.appendTo("body");
                                span.hide();
                            }
                            process_ajax = returnVal;
                        }
                    }

                    if (process_ajax)
                    {
                        data['value'] = last_val;
                        data['curr_value'] = prev_val;
                        data['id'] = order_id;
                        data['name'] = name;

                        if (field == 'product')
                        {
                            data['product_id'] = product_id;
                        }

                        $.ajax({
                            url: base_url + change_url,
                            type: 'POST',
                            dataType: 'json',
                            data: data,
                            success: function (response) {
                                if (response.value != "")
                                {
                                    $(td_id).text(response.value);
                                    td_remove(curr_val);

                                    if (field == 'product')
                                    {
                                        subtotal.text(response.subtotal);
                                        grand_total.text(response.grand_total);
                                    }

                                    if (name == 'order_sp' || name == 'order_so' || name == 'order_inv')
                                    {
                                        $(td_id).attr(curr_val, response.value);
                                        span.detach();
                                        $(td_id).text('');
                                        span.appendTo($(td_id));
                                        span.show();
                                        location.reload();
                                    }

                                } else {
                                    $(td_id).text(prev_val);
                                    td_remove(curr_val);
                                }
                            }
                        });
                    }
                });

                $(this).unbind('change').bind('change', function () {

                    if (name == 'date_created')
                    {
                        value = $(date_input_id).val();
                    }
                    else if(name == 'order_paid')
                    {
                        value = $(paid_input_id).val();
                    }

                    $.ajax({
                        url: base_url + change_url,
                        type: 'POST',
                        dataType: 'json',
                        data: {value :value, curr_val: $(td_id).attr(curr_val), id:order_id, name:name},
                        success: function (response) {
                            if (name == 'date_created')
                            {
                                $(date_input_id).remove();
                                $(td_id).text(response.value);
                            }
                            else if (name == 'order_paid')
                            {
                                $(paid_input_id).remove();
                                $(td_id).attr(curr_val, response.value);
                                location.reload();
                            }
                        }
                    });
                });
            }

            $(this).attr("contentEditable","true");

            if (name == 'product_is_received')
            {

                received_input_selector.unbind('change').bind('change', function () {
                    if (received_input_selector.is(':checked')) {
                        var returnVal = confirm('Yakin produk sudah diterima di gudang ? Proses tidak dapat diedit atau dibatalkan.');
                        received_input_selector.prop('checked', returnVal);

                        if (returnVal)
                        {
                            $.ajax({
                                url: base_url + change_url,
                                type: 'POST',
                                dataType: 'json',
                                data: {value :Boolean(returnVal), curr_val: prev_val, product_id: product_id,id:order_id, name:name},
                                success: function (response) {

                                    received_input_selector.remove();
                                    $(checked_icon).appendTo($(td_id));
                                    $(td_id).append(response.value);
                                    $(td_id).attr(curr_val, response.value);
                                    $(td_id).attr(data_checked, true);
                                }
                            });
                        } else {
                            td_remove(curr_val);
                            td_remove(contentEditable);
                            $(td_id).text('-');
                        }
                    } else {
                        returnVal = confirm('Yakin produk ternyata belum diterima di gudang ?');

                        if (returnVal)
                        {
                            received_input_selector.prop('checked', false);
                            received_input_selector.remove();
                            $.ajax({
                                url: base_url + change_url,
                                type: 'POST',
                                dataType: 'json',
                                data: {value :Boolean(false), curr_val: received_input_selector.attr('checked'), product_id: product_id,id:order_id, name:name},
                                success: function () {
                                    received_input_selector.remove();
                                    td_remove(curr_val);
                                    td_remove(data_checked);
                                    td_remove(contentEditable);
                                    $(td_id).text('-');
                                }
                            });
                        } else {
                            received_input_selector.remove();
                            $(checked_icon).appendTo(td_id);
                            $(td_id).append($(td_id).attr(curr_val));
                        }
                    }
                });
            }
        }
    });

    $(":button").unbind('click').bind('click', function () {
        id = $(this).attr('id');
        dash_splitter_id = id.split('-');

        name = dash_splitter_id[0];
        order_id = dash_splitter_id[1];
        var remark_add_text_selector = $('#remark_add_text-' + order_id);
        var remark_add_selector = $('#remark_add-'+ order_id);

        if (name == 'remark_add')
        {
            if (remark_add_text_selector.attr('hidden'))
            {

                remark_add_text_selector.show();
                remark_add_text_selector.removeAttr('hidden');
                $(this).prop('value', 'Tambahkan');
                remark_add_text_selector.focus();

            } else {



                if(confirm('Lanjut tambahkan ? proses ini tidak bisa tibatalkan ataupun dihapus.'))
                {
                    $.ajax({
                        url: location.href + '/order/remark',
                        type: 'POST',
                        dataType: 'json',
                        data: {order_id:order_id, value:remark_add_text_selector.val().trim()},
                        success: function () {
                            remark_add_text_selector.hide();
                            remark_add_text_selector.attr('hidden', true);
                            remark_add_selector.attr('value', 'Add remark');
                            remark_add_text_selector.val('');
                            location.reload();
                        }
                    });
                }
            }

            remark_add_text_selector.focusout(function () {

                if (remark_add_text_selector.val().length < 3)
                {
                    remark_add_text_selector.hide();
                    remark_add_text_selector.attr('hidden', true);
                    remark_add_selector.attr('value', 'Add remark');
                    remark_add_text_selector.val('');
                }
            });
        }
    });

    $.bind('focusout', function () {
        id = $(this).attr('id');
        dash_splitter_id = id.split('-');

        name = dash_splitter_id[0];
        order_id = dash_splitter_id[1];

        if (name == 'remark_add_text')
        {
            $(this).attr('hidden', true);
            $('#remark_add-'+order_id).attr('value', 'Tambahkan Remark');
        }
    });
});

$(document).ready(function () {

    var url = location.href.split('?')[0];

    var param_collector = function () {
        var object = {};
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i=0; i<vars.length; i++) {
            var pair = vars[i].split('=');
            var a = pair[0];

            if (a != "")
            {
                object[a] = pair[1];
            }
        }

        return object;
    };

    $("#helpdesk_filter").change(function () {

        var new_values = $(this).val();
        var collector = param_collector();
        collector['helpdesk'] = new_values;

        window.location = url + "?" + $.param(collector);
    });

    $("#status_filter").change(function () {

        var new_values = $(this).val();
        var collector = param_collector();
        collector['status'] = new_values;

        window.location = url + "?" + $.param(collector);
    });

    $("#year_filter").change(function () {

        var new_values = $(this).val();
        var collector = param_collector();
        collector['year'] = new_values;

        window.location = url + "?" + $.param(collector);
    });

    $("#month_filter").change(function () {

        var new_values = $(this).val();
        var collector = param_collector();
        collector['month'] = new_values;

        window.location = url + "?" + $.param(collector);
    });

    $('#location_filter').autocomplete({
        serviceUrl: base_url + 'order/package_location',
        minChars: 3,
        dataType: 'json',
        type: 'post',
        transformResult: function(response) {
            return {
                suggestions: $.map(response.suggestions, function(dataItem) {
                    return {
                        value: dataItem.value,
                        data: dataItem.data
                    };
                })
            };
        },
        onSelect: function (suggestion) {
            var new_values = suggestion.value;
            var collector = param_collector();
            collector['location'] = new_values;

            window.location = url + "?" + $.param(collector);
        }
    });
});
