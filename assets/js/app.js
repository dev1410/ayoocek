$(document).foundation();

$('.fdatepicker').fdatepicker({
    format: 'yyyy-mm-dd'
});

var total_calculator = function (current_subtotal, subtotal) {

    var subtotal_local = $(subtotal).val();
    var total_order = '#order_total';

    if ($(total_order).val() == '')
    {
        $(total_order).val(0);
    }

    if (subtotal_local)
    {
        var total_result = parseInt($(total_order).val()) - parseInt(current_subtotal) + parseInt($(subtotal).val());
        $(total_order).val(total_result);
    }
};

var calculator = function (qty, price, shipping, total) {
    var subtotal = $(total);
    var current_subtotal = 0;

    if (subtotal.val() != '' || subtotal.val() != 0)
    {
        current_subtotal = subtotal.val();
    }

    subtotal.val('');

    var qty_local = $(qty).val();
    var price_local = $(price).val();
    var ship = $(shipping).val();
    var result = (parseInt(qty_local) * parseInt(price_local)) + parseInt(ship);

    subtotal.val(result);

    total_calculator(current_subtotal, total)
};

/**
 * Chart
 */
var total_chart_data = {
    labels : [],
    datasets : [
        {
            label: "Completed Tender",
            fillColor : "rgba(0,153,51,0.2)",
            strokeColor : "rgba(0,153,51,1)",
            pointColor : "rgba(0,153,51,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(0,153,51,1)",
            data : []
        },
        {
            label: "On Process Tender",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,220)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : []
        },
        {
            label: "Cancelled Tender",
            fillColor : "rgba(255,0,0,0.2)",
            strokeColor : "rgba(255,0,0,1)",
            pointColor : "rgba(255,0,0,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(151,187,205,1)",
            data : []
        }
    ]
};
var total_chart = function () {
    var canvas  = $('#canvas-line');
    var ctx     = canvas.get(0).getContext("2d");
    window.chart= new Chart(ctx).Line(total_chart_data, {responsive: true});
};
var generate_chart_data = function (element) {

    total_chart();

    var trigger = $(element).val();
    var data_length = $('input[name="length"]');

    $.ajax({
        type: "POST",
        url: "order/get_total_yearly/",
        data: {year: trigger},
        dataType: "json",
        success: function (response) {

            var completed = 0;
            var total_order = 0;
            var cancelled = 0;

            if (parseInt(data_length.val()) > 0)
            {
                for (var i=0; i<parseInt(data_length.val()); i++)
                {
                    chart.removeData();
                }
            }

            for (i=0; i<response.length; i++)
            {
                chart.addData(response[i].data, response[i].label);
                completed += parseInt(response[i].data[0]);
                cancelled += parseInt(response[i].data[2]);
                total_order += parseInt(response[i].data[0]) +
                    parseInt(response[i].data[1]) + parseInt(response[i].data[2]) ;
            }

            data_length.val(response.length);
            $('#title').text('Pertahun ' + trigger);
            $('#completed_order_text').text('Completed : Rp ' + completed);
            $('#total_order_text').text('Total : Rp ' + total_order);
            $('#cancelled_order_text').text('Cancelled : Rp ' + cancelled);
        }
    });
};

/**
 * On change event changer function
 */
var changer = function (element) {
    $(element).change(function () {
        calculator(
            '#product_quantity',
            '#product_price',
            '#product_shipping_cost',
            '#product_subtotal');
    });
};

var changer_dynamic = function (element, dynamic) {
    $(element).change(function () {
        calculator(
            '#product_quantity-' + dynamic,
            '#product_price-' + dynamic,
            '#product_shipping_cost-' + dynamic,
            '#product_subtotal-' + dynamic);
    });
};

$(document).ready(function ($) {
    var selected_is_send = $("select[name=product_is_send]").val();
    if (selected_is_send == 1)
    {
        $("#validation_number").prop('type', 'text');
    }

    $('#package_location').autocomplete({
        serviceUrl: base_url + 'order/package_location',
        minChars: 3,
        dataType: 'json',
        type: 'post',
        transformResult: function(response) {
            return {
                suggestions: $.map(response.suggestions, function(dataItem) {
                    return {
                        value: dataItem.value,
                        data: dataItem.data
                    };
                })
            };
        }
    });

    changer('#product_quantity');
    changer('#product_price');
    changer('#product_shipping_cost');

    var products = 2;

    $("#add_product").click(function() {
        var cloner = $("#fieldset").clone();

        cloner.prop('id', 'fieldset-' + products);
        var title = cloner.find('legend');
        var name = title.next().find('input:first');
        var qty = title.next().next().find('input:first');
        var price = title.next().next().next().find('input:first');
        var ship = title.next().next().next().next().find('input:first');
        var total = title.next().next().next().next().next().find('input:first');
        var description = title.next().next().next().next().next().next().find('input:first');

        title.text('Produk ' + products);
        qty.prop('id', 'product_quantity-' + products);
        price.prop('id', 'product_price-' + products);
        ship.prop('id', 'product_shipping_cost-' + products);
        total.prop('id', 'product_subtotal-' + products);

        name.val('');
        qty.val('');
        price.val('');
        ship.val('');
        total.val('');
        description.val('');

        cloner.appendTo("#order_products");

        products += 1;
    });

    $('select[name="product_is_send"]').change(function () {
        var validation = $('input[name="validation_number"]');
        if (parseInt($(this).val()) == 0)
        {
            validation.prop('type', 'hidden');
            validation.removeAttr('required');
        } else {
            validation.prop('type', 'text');
            validation.attr('required', true);
        }
    });

    /**
     * Chart configuration block
     *
     */
    generate_chart_data('select[name="year"]');

    $('select[name="year"]').change(function () {
        generate_chart_data(this);
    });

});

$(document).keyup(function() {
    var active_element = $(':focus').prop('id');
    var active_id = active_element.split("-");
    var i = active_id[1];

    changer_dynamic('#product_quantity-' + i, i);
    changer_dynamic('#product_price-' + i, i);
    changer_dynamic('#product_shipping_cost-' + i, i);
});
