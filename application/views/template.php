<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo isset($_page_title) ? $_page_title : 'Selamat Datang! | Ayoocek.com'; ?></title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-icon/foundation-icons.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/datepicker.min.css'); ?>">

    <?php echo isset($css) ? $css : ''; ?>

    <link rel="stylesheet" href="<?php echo base_url('assets/css/app.css'); ?>">
</head>
<body>
<?php if ($is_logged_in) { ?>
<div class="top-bar">
    <div class="top-bar-title">
    <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
      <span class="menu-icon dark" data-toggle></span>
    </div>
    <div id="responsive-menu">
        <div class="top-bar-left">
            <ul class="dropdown menu" data-dropdown-menu>
                <li><a href="<?php echo base_url(); ?>">Beranda</a></li>
                <?php if ($is_admin == TRUE) { ?>
                <li>
                    <a href="javascript::0">Pengguna</a>
                    <ul class="menu vertical">
                        <li><a href="<?php echo base_url('auth'); ?>">Pengguna</a></li>
                        <li><a href="<?php echo base_url('auth/groups'); ?>"><?php echo ucwords('grup');?></a></li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
        <div class="top-bar-right">
            <ul class="dropdown menu" data-dropdown-menu>
                <li>
                    <a href="javascript::0"><?php echo isset($user) ? $user->first_name : 'Halo!'; ?></a>
                    <ul class="menu vertical">
                        <li><a href="<?php echo base_url('auth/edit_user/'. $user->id); ?>"><?php echo ucwords('Ubah Profil');?></a></li>
                        <li><a href="<?php echo base_url('auth/logout'); ?>"> Keluar</a> </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php } ?>

<div class="row columns">
<?php echo $_view_; ?>
    </div>

<script src="<?php echo base_url('assets/js/vendor/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/foundation.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/vendor/datepicker.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/jquery.autocomplete.min.js');?>"></script>
<?php echo isset($js) ? $js : ''; ?>

<script src="<?php echo base_url('assets/js/app.min.js');?>"></script>
</body>
</html>