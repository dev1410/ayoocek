<?php
if (!defined('BASEPATH')) exit('No direct script access allowed!');
class Template
{
    public function load($view = '', $data = array())
    {
        $CI = & get_instance ();

        $view != '' ? $data ['_view_'] = $CI->load->view ( $view, $data, true ) : $data ['_view_'] = '';

        $CI->load->view ( 'template', $data );
    }
}