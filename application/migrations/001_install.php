<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Install extends CI_Migration
{
    public function up()
    {
        /*
         -------------------------- ION AUTH ---------------------------
         */
        // Drop table 'groups' if it exists
        $this->dbforge->drop_table('groups', TRUE);

        // Table structure for table 'groups'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('groups');

        // Dumping data for table 'groups'
        $data = array(
            array(
                'id' => '1',
                'name' => 'Superadmin',
                'description' => 'Super user that used to control the whole thing on the website.'
            ),
            array(
                'id' => '2',
                'name' => 'Helpdesk',
                'description' => 'Helpdesk Group'
            ),
            array(
                'id' => '3',
                'name' => 'Purchasing',
                'description' => 'Purchasing Group'
            ),
            array(
                'id' => '4',
                'name' => 'Warehouse',
                'description' => 'Warehouse Group'
            ),
            array(
                'id' => '5',
                'name' => 'Finance',
                'description' => 'Finance Group'
            ),
            array(
                'id' => '6',
                'name' => 'Admin',
                'description' => 'Admin Group'
            ),
            array(
                'id' => '7',
                'name' => 'Adminso',
                'description' => 'Adminso Group'
            ),
            array(
                'id' => '8',
                'name' => 'Admindo',
                'description' => 'Admindo Group'
            ),
            array(
                'id' => '9',
                'name' => 'Admininv',
                'description' => 'Admininv Group'
            ),
            array(
                'id' => '10',
                'name' => 'Sales',
                'description' => 'Sales Group'
            ),
            array(
                'id' => '11',
                'name' => 'Delivery',
                'description' => 'Delivery Group'
            ),
            array(
                'id' => '12',
                'name' => 'Adminprod',
                'description' => 'Admin Produk'
            ),
        );
        $this->db->insert_batch('groups', $data);


        // Drop table 'users' if it exists
        $this->dbforge->drop_table('users', TRUE);

        // Table structure for table 'users'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'ip_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '16'
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'salt' => array(
                'type' => 'VARCHAR',
                'constraint' => '40'
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'activation_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => TRUE
            ),
            'forgotten_password_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => TRUE
            ),
            'forgotten_password_time' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'null' => TRUE
            ),
            'remember_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => TRUE
            ),
            'created_on' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
            ),
            'last_login' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'null' => TRUE
            ),
            'active' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'unsigned' => TRUE,
                'null' => TRUE
            ),
            'first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ),
            'last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ),
            'company' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            )

        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('users');

        // Dumping data for table 'users'
        $data = array(
            'id' => '1',
            'ip_address' => '127.0.0.1',
            'username' => 'administrator',
            'password' => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
            'salt' => '',
            'email' => 'admin@admin.com',
            'activation_code' => '',
            'forgotten_password_code' => NULL,
            'created_on' => '1268889823',
            'last_login' => '1268889823',
            'active' => '1',
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'company' => 'Ayoocek',
            'phone' => '0',
        );
        $this->db->insert('users', $data);


        // Drop table 'users_groups' if it exists
        $this->dbforge->drop_table('users_groups', TRUE);

        // Table structure for table 'users_groups'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE
            ),
            'group_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('users_groups');

        // Dumping data for table 'users_groups'
        $data = array(
            array(
                'id' => '1',
                'user_id' => '1',
                'group_id' => '1',
            )
        );
        $this->db->insert_batch('users_groups', $data);


        // Drop table 'login_attempts' if it exists
        $this->dbforge->drop_table('login_attempts', TRUE);

        // Table structure for table 'login_attempts'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'ip_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '16'
            ),
            'login' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null', TRUE
            ),
            'time' => array(
                'type' => 'INT',
                'constraint' => '11',
                'unsigned' => TRUE,
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('login_attempts');

        /*
         -------------------------- END ION AUTH ---------------------------
         */


        /*
         --------------------------- ORDER ---------------------------------
         */

        // Drop table `order_pemesan` if exists
        $this->dbforge->drop_table('order_pemesan', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
            ),
            'phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_pemesan');


        // Drop table `order_pembeli` if exists
        $this->dbforge->drop_table('order_pembeli', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'nip' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_pembeli');


        // Drop table `order_status` if exists
        $this->dbforge->drop_table('order_status', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
            ),
            'operation' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'remark' => array(
                'type' => 'VARCHAR',
                'constraint' => '120',
                'null' => TRUE
            ),
            'updated_date' => array(
                'type' => 'TIMESTAMP',
            ),
            'updated_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_status');


        // Drop table `order_detail` if exists
        $this->dbforge->drop_table('order_detail', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'product_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'quantity' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
            ),
            'price' => array(
                'type' => 'INT',
                'constraint' => '15',
            ),
            'shipping_cost' => array(
                'type' => 'INT',
                'constraint' => '15',
            ),
            'subtotal' => array(
                'type' => 'INT',
                'constraint' => '15',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'is_processed' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'sp_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'so_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'po_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'is_received' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'received_date' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'do_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'is_send' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'validation_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'courier' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'send_date' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'inv_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'is_paid' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'paid_date' => array(
                'type' => 'DATE',
                'null' => TRUE
            ),
            'updated_date' => array(
                'type' => 'TIMESTAMP',
            ),
            'updated_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_detail');


        // Drop table `order` if exists
        $this->dbforge->drop_table('order', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_number' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '100',
                'unique' => TRUE
            ),
            'order_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'instance' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'package_location' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ),
            'work_unit' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ),
            'work_unit_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '256',
                'null' => TRUE
            ),
            'date_created' => array(
                'type' => 'DATE',
            ),
            'order_quantity' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '5',
            ),
            'order_total' => array(
                'type' => 'INT',
                'constraint' => '13',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '256',
                'null' => TRUE
            ),
            'is_processed' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_completed' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_cancelled' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'updated_date' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE
            ),
            'created_date' => array(
                'type' => 'TIMESTAMP',
            ),
            'created_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'updated_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order');

        // Drop table `kota` if exists
        $this->dbforge->drop_table('kota', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'kota' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('kota');

        // Drop table `order_helpdesk` if exists
        $this->dbforge->drop_table('order_helpdesk', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'helpdesk_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_helpdesk');

        // Drop table `order_sales` if exists
        $this->dbforge->drop_table('order_sales', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'sales_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_sales');

        // Drop table `order_step` if exists
        $this->dbforge->drop_table('order_step', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'step' => array(
                'type' => 'varchar',
                'constraint' => '20',
                'null' => TRUE
            ),
            'updated_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'null' => TRUE
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_step');

        // Drop table `order_remark` if exists
        $this->dbforge->drop_table('order_remark', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'remark' => array(
                'type' => 'varchar',
                'constraint' => '200',
                'null' => TRUE
            ),
            'created_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '5',
                'null' => TRUE
            ),
            'created_date' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_remark');

        /*
         --------------------------- END ORDER ---------------------------------
         */

    }

    public function down()
    {
        $this->dbforge->drop_table('users', TRUE);
        $this->dbforge->drop_table('groups', TRUE);
        $this->dbforge->drop_table('users_groups', TRUE);
        $this->dbforge->drop_table('login_attempts', TRUE);

        $this->dbforge->drop_table('order_pemesan', TRUE);
        $this->dbforge->drop_table('order_pembeli', TRUE);
        $this->dbforge->drop_table('order_status', TRUE);
        $this->dbforge->drop_table('order_detail', TRUE);
        $this->dbforge->drop_table('order', TRUE);

        $this->dbforge->drop_table('kota', TRUE);

        $this->dbforge->drop_table('order_helpdesk', TRUE);
        $this->dbforge->drop_table('order_sales', TRUE);
        $this->dbforge->drop_table('order_step', TRUE);
    }
}