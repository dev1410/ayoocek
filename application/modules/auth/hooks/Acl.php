<?php
defined('BASEPATH') OR exit('No direct script access allowed!');
/**
 * Created by PhpStorm.
 * User: dev1410
 * Date: 13/03/16
 * Time: 15:10
 */
class Acl
{
    protected $CI;

    private $is_admin = FALSE;

    private $logged_in = FALSE;

    protected $role = array();

    protected $group_role = array();

    /**
     * Construct in order to set the rules
     */
    public function __construct()
    {
        $this->role = [
            'auth' => [],
            'dashboard' => []
        ];

        $this->CI =& get_instance();

        $this->CI->load->database();

        if (!$this->CI->db->table_exists('users'))
        {
            $this->CI->load->library('migration');
            if ($this->CI->migration->current() === FALSE)
            {
                show_error($this->CI->migration->error_string());
            }
        }
    }

    /**
     * The main method, determine if the user is allowed to view
     * the sites.
     */
    public function auth()
    {
        $module = $this->CI->router->module;

        $class = $this->CI->router->fetch_class();

        $method = $this->CI->router->fetch_method();

        $module_accessing = strtolower($module);

        $class_accessing = strtolower($class);

        $method_accessing = strtolower($method);

        // If session library not already loaded.
        if (! isset($this->CI->session))
            $this->CI->load->library('session');

        // If ion_auth library not already loaded
        if (! isset($this->CI->ion_auth))
            $this->CI->load->library('auth/ion_auth');

        // If permission library not already loaded
        if (! isset($this->CI->permission))
            $this->CI->load->library('auth/permission');

        // If the user is not logged_in
        if (
            $this->CI->ion_auth->logged_in() == FALSE &&
            $class_accessing != 'auth' &&
            $method_accessing != 'login')
        {
            // Let the user login first
            redirect('auth');

        } elseif (
            $this->CI->ion_auth->logged_in() == TRUE &&
            $this->CI->ion_auth->is_admin() == TRUE &&
            $class_accessing == 'auth' &&
            $method_accessing == 'login')
        {

            /**
             * If the user try to access login page while they are logged in
             * Ignore the access
             */
            redirect(base_url());

        } else {

            /**
             * Set local variable to TRUE
             * The purpose is for sync between ion_auth and
             * local variable for security reason if someone
             * trying to inject CI session
             */

            $this->is_admin = TRUE;
            $this->logged_in = TRUE;
        }

        /*
        |-------------------------------------------------------
        | Let start to control user access permission here.
        | It still fall under admin module
        | ------------------------------------------------------
        */
        /*if (
            $this->is_admin == $this->CI->ion_auth->is_admin() &&
            $this->logged_in == $this->CI->ion_auth->logged_in())
        {
            echo "The user is real logged_in";
            echo '<br> ' . $module . ' - ' . $class . ' - ' . $method . '<br>';
        }*/

    }
}