<div class="row">
    <?php echo form_open(uri_string());?>

    <h2><?php echo lang('edit_user_heading');?></h2>
    <hr/>
    <p><?php echo lang('edit_user_subheading');?></p>

    <div class="row columns">

        <div class="column">

            <div id="infoMessage" <?php if (isset($message)) echo 'class="alert label"'; ?>><?php echo $message;?></div>

            <fieldset class="fieldset">
                <legend>General Information</legend>

                <div class="medium-6 column">
                    <label for="first_name">
                        <?php echo lang('edit_user_fname_label', 'first_name');?>
                        <?php echo form_input($first_name);?>
                    </label>
                </div>

                <div class="medium-6 column">
                    <label for="last_name">
                        <?php echo lang('edit_user_lname_label', 'last_name');?>
                        <?php echo form_input($last_name);?>
                    </label>
                </div>

                <div class="medium-8 column">
                    <label for="user_company">
                        <?php echo lang('edit_user_company_label', 'company');?>
                        <?php echo form_input($company);?>
                    </label>
                </div>

                <div class="medium-4 column">
                    <label for="user_phone">
                        <?php echo lang('edit_user_phone_label', 'phone');?>
                        <?php echo form_input($phone);?>
                    </label>
                </div>

                <div class="column">
                    <label for="password">
                        <?php echo lang('edit_user_password_label', 'password');?>
                        <?php echo form_input($password);?>
                    </label>
                </div>

                <div class="column">
                    <label for="confirm_password">
                        <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>
                        <?php echo form_input($password_confirm);?>
                    </label>
                </div>

            </fieldset>

            <?php if ($this->ion_auth->is_admin()): ?>
                <fieldset class="fieldset">
                    <legend>Member of group</legend>
                    <div class="column">
                        <label for="group">
                            <select name="groups[]">

                                <?php foreach ($groups as $group) { ?>
                                    <option value="<?=$group['id'];?>"
                                        <?php echo $group['id'] == $csg->id ? 'selected' : ''; ?>>
                                            <?=$group['name'];?>
                                    </option>
                                <?php } ?>
                            </select>

                        </label>
                    </div>
                </fieldset>
            <?php endif ?>

        </div>

    </div>

    <div class="column">
        <div class="small-8 right">
            <button type="submit" class="button right"><?php echo lang('edit_user_submit_btn') ?></button>
            <a href="<?php echo site_url('auth') ?>" class="secondary button">Cancel</a>

            <?php if ($has_delete_user) { ?>
            <a href="<?php echo site_url('auth/delete/' . $user->id) ?>" class="alert button"
               onclick="javasciprt: return confirm('Apakah anda yakin ?')">
                <i class="fi-x"></i> Hapus pengguna ini</a>
            <?php } ?>
        </div>
    </div>

    <?php echo form_hidden('id', $user->id);?>
    <?php echo form_hidden($csrf); ?>

    <?php echo form_close();?>
    <hr/>

</div>
