<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo isset($_page_title) ? $_page_title : 'Login | Ayoocek'; ?></title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.min.css'); ?>">
    <?php echo isset($css) ? $css : ''; ?>
</head>
<style>
    .login-button {
        background-color: #CF252B;
        border-radius: 3px;
    }
    .login-button:hover{
        background-color: #f42d33;
    }
    p a {
        color: #555;
    }
    a:hover {
        color: #2a2a2a;
    }
    input:checked~.switch-paddle {
        background: #CF252B;
    }
</style>
<body>

<div class="row" data-equalizer data-equalize-by-row="true" style="padding-top: 5%;">
    <div class="medium-6 medium-centered large-4 large-centered columns">


        <?php echo form_open("auth/login");?>
        <div class="row column data-equalizer-watch">
            <img src="<?php echo base_url('assets/img/logo.jpg');?>">
            <h6 class="text-center">
                <?php //echo lang('login_subheading');?>
                <?php if ($message) { ?>
                    <div id="infoMessage" class="alert callout"><?php echo $message; ?></div>
                <?php } ?>
            </h6>

            <label>
                <?php // echo lang('login_identity_label', 'identity');?>
                <?php echo form_input($identity, 'required');?>
            </label>

            <label>
                <?php // echo lang('login_password_label', 'password');?>
                <?php echo form_input($password);?>
            </label>

            <p><?php echo lang('login_remember_label');?></p>
            <div class="switch tiny">
                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember" class="switch-input"');?>
                <label class="switch-paddle" for="remember">
                    <span class="show-for-sr"><?php echo lang('login_remember_label');?></span>
                    <span class="switch-active" aria-hidden="true">Yes</span>
                    <span class="switch-inactive" aria-hidden="true">No</span>
                </label>
            </div>

            <p><input type="submit" class="button expanded login-button" value="Log In" style="font-weight: bold;"></p>
            <p class="text-center">
                <a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
            </p>
        </div>
        <?php echo form_close();?>

    </div>
</div>
<script src="<?php echo base_url('assets/js/vendor/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/foundation.min.js');?>"></script>
<?php echo isset($js) ? $js : ''; ?>
<script>
    $(document).ready(function(){
        $(document).foundation();
    });
</script>
</body>
</html>
