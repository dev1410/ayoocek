<div class="row">

    <h2><?php echo lang('groups_heading');?></h2>
    <hr/>
    <p><?php echo lang('groups_subheading');?></p>

    <div class="row columns">
        <div id="infoMessage"><?php echo isset($message) ? $message : '';?></div>

        <div class="column">

            <div class="table-scroll">
                <table class="hover">
                    <thead>
                    <tr>
                        <th><?php echo lang('groups_name_th');?></th>
                        <th><?php echo lang('groups_description_th');?></th>
                        <th><?php echo lang('groups_action_th');?></th>
                    </tr>
                    </thead>
                    <?php foreach ($groups as $group):?>
                        <tr>
                            <td><?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo htmlspecialchars($group->description,ENT_QUOTES,'UTF-8');?></td>
                            <td><?php echo anchor("auth/edit_group/".$group->id, 'Edit') ;?></td>
                        </tr>
                    <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>

    <hr/>

</div>