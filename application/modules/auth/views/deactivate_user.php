<h1><?php echo lang('deactivate_heading');?></h1>
<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

<?php echo form_open("auth/deactivate/".$user->id);?>

<div class="switch small">
    <input class="switch-input" id="exampleRadioSwitch1" type="radio" checked value="yes" name="confirm">
    <label class="switch-paddle" for="exampleRadioSwitch1">
        <span class="switch-active" aria-hidden="true">Yes</span>
        <span class="show-for-sr"></span>
    </label>
</div>
<div class="switch small">
    <input class="switch-input" id="exampleRadioSwitch2" type="radio"  value="no" name="confirm">
    <label class="switch-paddle" for="exampleRadioSwitch2">
        <span class="switch-active" aria-hidden="true">No</span>
        <span class="show-for-sr"><?php echo lang('deactivate_confirm_n_label', 'confirm');?></span>
    </label>
</div>
  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>

  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>

<?php echo form_close();?>