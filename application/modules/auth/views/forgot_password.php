<!doctype html>
<html class="no-js" lang="en">
<head>
      <meta charset="utf-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title><?php echo isset($_page_title) ? $_page_title : 'Forgot Password | Popbox Asia'; ?></title>
      <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.min.css'); ?>">
      <link rel="stylesheet" href="<?php echo base_url('assets/css/font-icon/foundation-icons.css')?>"
      <?php echo isset($css) ? $css : ''; ?>
</head>
<style>
      .reset-button {
            background-color: #CF252B;
      }
      .reset-button:hover{
            background-color: #f42d33;
      }
      p a {
            color: #555;
      }
      a:hover {
            color: #2a2a2a;
      }
</style>
<body>

<div class="row" data-equalizer data-equalize-by-row="true" style="padding-top: 5%;">
      <div class="medium-6 medium-centered large-4 large-centered columns">

            <div id="infoMessage"><?php echo $message;?></div>

            <?php echo form_open("auth/forgot_password");?>
            <div class="row column data-equalizer-watch">

                  <img src="<?php echo base_url('assets/img/logo.jpg');?>">

                  <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

                  <div class="input-group">
                        <span class="input-group-label"><i class="fi-mail"></i></span>
                        <?php $identity['class'] = 'input-group-field' ;?>
                        <?php $identity['placeholder'] = 'Enter your email here'; ?>
                        <?php echo form_input($identity);?>

                        <div class="input-group-button">
                              <?php echo form_submit('submit', lang('forgot_password_submit_btn'), 'class="button reset-button"');?>
                        </div>
                  </div>

            </div>

            <?php echo form_close();?>

      </div>
</div>
<script src="<?php echo base_url('assets/js/vendor/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/foundation.min.js');?>"></script>
<?php echo isset($js) ? $js : ''; ?>
<script>
      $(document).ready(function(){
            $(document).foundation();
      });
</script>
</body>
</html>