<?php
if (!defined('BASEPATH')) exit('No direct script access allowed!');
class Permission
{
    /**
     * @var CI_Controller
     */
    protected $CI;

    /**
     * @var bool
     */
    public $has_permission;

    /**
     * @var ion_auth
     */
    protected $auth;

    /**
     * Permission constructor.
     */
    function __construct()
    {
        $this->CI =& get_instance();

        if (!isset($this->ion_auth))
            $this->CI->load->library('ion_auth');

        $this->auth = $this->CI->ion_auth;
    }

    /**
     * has_add_tender
     *
     * @return bool
     */
    public function has_add_tender()
    {
        if ($this->auth->is_admin() || $this->auth->is_helpdesk())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_tender
     *
     * @return bool
     */
    public function has_change_tender()
    {
        if ($this->auth->is_admin())
        {

            $this->has_permission = TRUE;
        }
        else {

            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }

    /**
     * has_cancel_tender
     *
     * @return bool
     */
    public function has_cancel_tender()
    {
        if ($this->auth->is_admin() || $this->auth->is_helpdesk())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_print_precall
     *
     * @return bool
     */
    public function has_print_precall()
    {
        if ($this->auth->is_admin() || $this->auth->is_helpdesk())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_proses
     *
     * @return bool
     */
    public function has_add_detail_proses()
    {
        if ($this->auth->is_admin() || $this->auth->is_helpdesk())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_proses
     *
     * @return bool
     */
    public function has_change_detail_proses()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_sp_number
     *
     * @return bool
     */
    public function has_add_detail_sp_number()
    {
        if ($this->auth->is_admin() || $this->auth->is_helpdesk())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_sp_number
     *
     * @return bool
     */
    public function has_change_detail_sp_number()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_so_number
     *
     * @return bool
     */
    public function has_add_detail_so_number()
    {
        if ($this->auth->is_admin() || $this->auth->is_adminso())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_so_number
     *
     * @return bool
     */
    public function has_change_detail_so_number()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_po_number
     *
     * @return bool
     */
    public function has_add_detail_po_number()
    {
        if ($this->auth->is_admin() || $this->auth->is_purchasing())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_po_number
     *
     * @return bool
     */
    public function has_change_detail_po_number()
    {
        if ($this->auth->is_admin() || $this->auth->is_purchasing())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_do_number
     *
     * @return bool
     */
    public function has_add_detail_do_number()
    {
        if ($this->auth->is_admin() || $this->auth->is_admindo())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_do_number
     *
     * @return bool
     */
    public function has_change_detail_do_number()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_inv_number
     *
     * @return bool
     */
    public function has_add_detail_inv_number()
    {
        if ($this->auth->is_admin() || $this->auth->is_admininv())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_inv_number
     *
     * @return bool
     */
    public function has_change_detail_inv_number()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_received
     *
     * @return bool
     */
    public function has_add_detail_received()
    {
        if ($this->auth->is_admin() || $this->auth->is_warehouse())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_received
     *
     * @return bool
     */
    public function has_change_detail_received()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_send
     *
     * @return bool
     */
    public function has_add_detail_send()
    {
        if ($this->auth->is_admin() || $this->auth->is_delivery())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_send
     *
     * @return bool
     */
    public function has_change_detail_send()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_detail_paid
     *
     * @return bool
     */
    public function has_add_detail_paid()
    {
        if ($this->auth->is_admin() || $this->auth->is_finance())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_change_detail_paid
     *
     * @return bool
     */
    public function has_change_detail_paid()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_delete_user
     *
     * @return bool
     */
    public function has_delete_user()
    {
        if ($this->auth->is_admin())
            $this->has_permission = TRUE;
        else
            $this->has_permission = FALSE;

        return $this->has_permission;
    }

    /**
     * has_add_remark
     *
     * @return bool
     */
    public function has_add_remark()
    {
        if ($this->auth->is_admin()
            || $this->auth->is_admin_staff()
            || $this->auth->is_warehouse()
            || $this->auth->is_purchasing()
            || $this->auth->is_helpdesk()
            || $this->auth->is_admindo()
            || $this->auth->is_adminso()
            || $this->auth->is_admininv()
            || $this->auth->is_delivery()
            || $this->auth->is_finance())
        {
            $this->has_permission = TRUE;

        } else {
            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }

    /**
     * has_filter_helpdesk
     *
     * @return bool
     */
    public function has_filter_helpdesk()
    {
        if ($this->auth->is_admin()
            || $this->auth->is_admin_staff()
            || $this->auth->is_warehouse()
            || $this->auth->is_purchasing()
            || $this->auth->is_helpdesk()
            || $this->auth->is_admindo()
            || $this->auth->is_adminso()
            || $this->auth->is_admininv()
            || $this->auth->is_delivery()
            || $this->auth->is_finance()
            || $this->auth->is_sales())
        {
            $this->has_permission = TRUE;

        } else {
            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }


    /**
     * has_filter_status
     *
     * @return bool
     */
    public function has_filter_status()
    {
        if ($this->auth->is_admin()
            || $this->auth->is_admin_staff()
            || $this->auth->is_warehouse()
            || $this->auth->is_purchasing()
            || $this->auth->is_helpdesk()
            || $this->auth->is_admindo()
            || $this->auth->is_adminso()
            || $this->auth->is_admininv()
            || $this->auth->is_delivery()
            || $this->auth->is_finance()
            || $this->auth->is_sales())
        {
            $this->has_permission = TRUE;

        } else {
            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }

    /**
     * has_filter_date
     *
     * @return bool
     */
    public function has_filter_date()
    {
        if ($this->auth->is_admin()
            || $this->auth->is_admin_staff()
            || $this->auth->is_warehouse()
            || $this->auth->is_purchasing()
            || $this->auth->is_helpdesk()
            || $this->auth->is_admindo()
            || $this->auth->is_adminso()
            || $this->auth->is_admininv()
            || $this->auth->is_delivery()
            || $this->auth->is_finance()
            || $this->auth->is_sales())
        {
            $this->has_permission = TRUE;

        } else {
            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }

    /**
     * has_filter_location
     *
     * @return bool
     */
    public function has_filter_location()
    {
        if ($this->auth->is_admin()
            || $this->auth->is_admin_staff()
            || $this->auth->is_warehouse()
            || $this->auth->is_purchasing()
            || $this->auth->is_helpdesk()
            || $this->auth->is_admindo()
            || $this->auth->is_adminso()
            || $this->auth->is_admininv()
            || $this->auth->is_delivery()
            || $this->auth->is_finance()
            || $this->auth->is_sales())
        {
            $this->has_permission = TRUE;

        } else {
            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }

    public function has_see_total_summary()
    {
        if ($this->auth->is_admin())
        {
            $this->has_permission = TRUE;

        } else {
            $this->has_permission = FALSE;
        }

        return $this->has_permission;
    }

}