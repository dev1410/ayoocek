<?php
if (!defined('BASEPATH')) exit('No direct script access allowed!');
class Order extends MX_Controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('auth/ion_auth');
        $this->load->library('form_validation');

        $this->load->model('Order_model');
        $this->load->model('Order_pembeli_model');
        $this->load->model('Order_pemesan_model');
        $this->load->model('Order_detail_model');
        $this->load->model('Order_status_model');
        $this->load->model('Kota_model');

        $this->data['user'] = $this->ion_auth->user()->row();
        $this->data['is_admin'] = $this->ion_auth->is_admin();
        $this->data['is_logged_in'] = $this->ion_auth->logged_in();
    }

    public function index()
    {
        $this->data = $this->_permission();

        $this->data['js'] = '<script src="'. base_url('assets/js/editable.min.js') .'"></script>';

        $this->data['status_filter_option'] = array(
            1 => 'completed',
            2 => 'processing',
            3 => 'cancelled',
            4 => 'New added',);
        $this->data['year_filter_option'] = $this->Order_model->get_available_years();
        $this->data['month_filter_option'] = $this->Order_model->get_available_months();

        $helpdesk_filter = intval(urldecode($this->input->get('helpdesk', TRUE)));
        $status_filter = intval(urldecode($this->input->get('status', TRUE)));
        $year_filter = urldecode($this->input->get('year', TRUE));
        $month_filter = urldecode($this->input->get('month', TRUE));
        $location_filter = urldecode($this->input->get('location', TRUE));

        isset($helpdesk_filter) ? $this->data['helpdesk_filter_selected'] = $helpdesk_filter : false;
        isset($status_filter) ? $this->data['status_filter_selected'] = $status_filter : false;
        isset($year_filter) ? $this->data['year_filter_selected'] = $year_filter : false;
        isset($month_filter) ? $this->data['month_filter_selected'] = $month_filter : false;
        isset($location_filter) ? $this->data['location_filter_selected'] = $location_filter : false;

        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url'] = base_url() . 'order/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'order/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'order/';
            $config['first_url'] = base_url() . 'order/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Order_model->total_rows($q, $helpdesk_filter);

        $orders = $this->_orders(
            $config['per_page'], $start, $q,
            $helpdesk_filter, $status_filter,
            $year_filter, $month_filter, $location_filter);

        foreach ($orders as $order)
        {
            $step = $this->Order_model->get_step($order->id);
            if (count($step) > 0)
            {
                $order->step = $step->step;
                $order->is_paid = $this->Order_detail_model->get_is_paid_by_order_id($order->id);
                $order->step = (object) array(
                    'step' => $step->step,
                    'sp' => $this->Order_detail_model->get_sp_by_order_id($order->id),
                    'so' => $this->Order_detail_model->get_so_by_order_id($order->id),
                    'invoice' => $this->Order_detail_model->get_invoice_by_order_id($order->id)
                );
            }

            $order->helpdesk = $this->Order_model->get_helpdesk($order->id);
            $order->sales = $this->Order_model->get_sales($order->id);

            $order->pemesan = $this->Order_pemesan_model->get_by_order_id($order->id);
            $order->pembeli = $this->Order_pembeli_model->get_by_order_id($order->id);

            $order->produk = $this->Order_detail_model->get_by_order_id($order->id);

            $order->sp_number = $this->Order_detail_model->get_sp_by_order_id($order->id);
            $order->so_number = $this->Order_detail_model->get_so_by_order_id($order->id);
            $order->inv_number = $this->Order_detail_model->get_invoice_by_order_id($order->id);
            $order->is_paid = $this->Order_detail_model->get_is_paid_by_order_id($order->id);
            $order->paid_date = $this->Order_detail_model->get_paid_date_by_order_id($order->id);

            $order->remarks = $this->Order_model->get_remarks($order->id);
        }
        $this->data['helpdesk_list'] = $this->Order_model->get_helpdesk_list();

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $this->data['order_data']   = $orders;
        $this->data['q']            = $q;
        $this->data['pagination']   = $this->pagination->create_links();
        $this->data['total_rows']   = $config['total_rows'];
        $this->data['start']        = $start;
        $this->data['total_price']  = $this->Order_model->get_total_price();

        return $this->template->load('order/index', $this->data);
    }

    public function read($id)
    {
        $row = $this->Order_model->get_by_id($id);
        $pembeli = $this->Order_pembeli_model->get_by_order_id($id);
        $pemesan = $this->Order_pemesan_model->get_by_order_id($id);
        $this->data['order_detail'] = $this->Order_detail_model->get_by_order_id($id);
        $this->data['order_status'] = $this->Order_status_model->get_by_order_id($id);
        $created_user = $this->ion_auth->user($row->created_user)->row();
        $created_user_group = $this->ion_auth->get_users_groups($row->created_user)->row();
        $updated_user = $this->ion_auth->user($row->updated_user)->row();
        $updated_user_group = $this->ion_auth->get_users_groups($row->updated_user)->row();

        $this->data['css'] = '<link rel="stylesheet" href="'.base_url('assets/css/reset.css').'">
            <link rel="stylesheet" href="'.base_url('assets/css/style.css').'">
        ';
        $this->data['js'] = '<script src="' . base_url('assets/js/main.js') .'"></script>';

        $this->data['status'] = $this->Order_status_model->get_status($id);

        foreach ($this->data['status'] as $stat)
        {
            $op = $this->ion_auth->user($stat->updated_user)->row();
            $group = $this->ion_auth->get_users_groups($stat->updated_user)->row();
            $stat->updated_user = $op->first_name . ' ' . $op->last_name . ' (' .$group->name. ')';
        }


        if ($row) {
            $this->data['id']               = $row->id;
            $this->data['order_number']     = $row->order_number;
            $this->data['order_name']       = $row->order_name;
            $this->data['instance']         = $row->instance;
            $this->data['package_location'] = $row->package_location;
            $this->data['work_unit']        = $row->work_unit;
            $this->data['work_unit_address']= $row->work_unit_address;
            $this->data['date_created']     = $row->date_created;
            $this->data['order_quantity']   = $row->order_quantity;
            $this->data['order_total']      = $row->order_total;
            $this->data['description']      = $row->description;
            $this->data['is_processed']     = $row->is_processed;
            $this->data['is_completed']     = $row->is_completed;
            $this->data['is_cancelled']     = $row->is_cancelled;
            $this->data['updated_date']     = $row->updated_date;
            $this->data['created_date']     = $row->created_date;
            $this->data['created_user']     =
                $created_user->first_name . ' ' .
                $created_user->last_name . ' (' . $created_user_group->name . ')';
            $this->data['updated_user']     =
                $updated_user->first_name . ' ' .
                $updated_user->last_name . ' (' . $updated_user_group->name . ')';

            $this->template->load('order_read', $this->data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('order'));
        }
    }

    public function create()
    {
        $this->data['button']           = 'Create';
        $this->data['action']           = site_url('order/create_action');
        $this->data['id']               = set_value('id');
        $this->data['order_number']     = set_value('order_number');
        $this->data['order_name']       = set_value('order_name');
        $this->data['instance']         = set_value('instance');
        $this->data['package_location'] = set_value('package_location');
        $this->data['work_unit']        = set_value('work_unit');
        $this->data['work_unit_address']= set_value('work_unit_address');
        $this->data['date_created']     = set_value('date_created');
        $this->data['order_quantity']   = set_value('order_quantity');
        $this->data['description']      = set_value('description');
        $this->data['is_processed']     = set_value('is_processed');
        $this->data['is_completed']     = set_value('is_completed');
        $this->data['is_cancelled']     = set_value('is_cancelled');
        $this->data['updated_date']     = set_value('updated_date');
        $this->data['created_date']     = set_value('created_date');
        $this->data['created_user']     = set_value('created_user');
        $this->data['updated_user']     = set_value('updated_user');

        /* Pemesan / Pembeli */

        $this->data['pemesan_id']       = set_value('pemesan_id');
        $this->data['pemesan_name']     = set_value('pemesan_name');
        $this->data['pemesan_email']    = set_value('pemesan_email');
        $this->data['pemesan_phone']    = set_value('pemesan_phone');

        $this->data['pembeli_id']       = set_value('pembeli_id');
        $this->data['pembeli_name']     = set_value('pembeli_name');
        $this->data['pembeli_title']    = set_value('pembeli_title');
        $this->data['pembeli_nip']      = set_value('pembeli_nip');

        $this->data['product_name']     = set_value('');
        $this->data['product_quantity'] = set_value('');
        $this->data['product_price']    = set_value('');
        $this->data['product_shipping_cost'] = set_value('');
        $this->data['product_subtotal'] = set_value('');
        $this->data['product_description'] = set_value('');

        $this->data['order_helpdesk']   = $this->Order_model->get_helpdesk_list();
        $this->data['order_sales']      = $this->Order_model->get_sales_list();

        $this->template->load('order_form', $this->data);
    }

    public function create_action()
    {
        $this->data = $this->_permission();

        $is_exists = $this->Order_model->order_number_exists(
            $this->input->post('order_number', TRUE));

        if ($is_exists)
        {
            $this->session->set_flashdata('message', 'Nomor Tender telah digunakan!');

            $this->create();
        } else {

            $this->_rules();

            if ($this->form_validation->run() == FALSE) {
                $this->create();
            } else {
                if ($this->data['has_add_tender'])
                {
                    $order_data = array(
                        'order_number' => $this->input->post('order_number', TRUE),
                        'order_name' => $this->input->post('order_name', TRUE),
                        'instance' => $this->input->post('instance', TRUE),
                        'package_location' => $this->input->post('package_location', TRUE),
                        'work_unit' => $this->input->post('work_unit', TRUE),
                        'work_unit_address' => $this->input->post('work_unit_address', TRUE),
                        'date_created' => $this->input->post('date_created', TRUE),
                        'order_quantity' => $this->input->post('order_quantity', TRUE),
                        'order_total' => $this->input->post('order_total', TRUE),
                        'description' => $this->input->post('description', TRUE),

                        'updated_date' => date('Y-m-d H:i:s'),
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_user' => $this->data['user']->id,
                        'updated_user' => $this->data['user']->id,
                    );
                    $this->Order_model->insert($order_data);

                    $order_id = $this->db->insert_id();

                    $pemesan_data = array(
                        'order_id' => $order_id,
                        'name' => $this->input->post('pemesan_name', TRUE),
                        'email' => $this->input->post('pemesan_email', TRUE),
                        'phone' => $this->input->post('pemesan_phone', TRUE)
                    );
                    $this->Order_pemesan_model->insert($pemesan_data);

                    $pembeli_data = array(
                        'order_id' => $order_id,
                        'name' => $this->input->post('pembeli_title', TRUE),
                        'title' => $this->input->post('pembeli_name', TRUE),
                        'nip' => $this->input->post('pembeli_nip', TRUE)
                    );
                    $this->Order_pembeli_model->insert($pembeli_data);

                    $order_detail_data = array();

                    $prod_name = $this->input->post('product_name', TRUE);
                    $prod_price = $this->input->post('product_price', TRUE);
                    $prod_qty = $this->input->post('product_quantity', TRUE);
                    $prod_ship = $this->input->post('product_shipping_cost', TRUE);
                    $prod_subtotal = $this->input->post('product_subtotal', TRUE);
                    $prod_desc = $this->input->post('product_description', TRUE);

                    for ($i = 0; $i < count($prod_name); $i++) {
                        if ($prod_name[$i] != '' && $prod_price[$i] > 0 && $prod_qty[$i] > 0 && $prod_subtotal[$i])
                        {
                            $obj = array(
                                'order_id' => $order_id,
                                'product_name' => $prod_name[$i],
                                'price' => $prod_price[$i],
                                'quantity' => $prod_qty[$i],
                                'shipping_cost' => $prod_ship[$i],
                                'subtotal' => $prod_subtotal[$i],
                                'description' => $prod_desc[$i],
                                'updated_user' => $this->data['user']->id,
                            );
                            array_push($order_detail_data, $obj);
                        }
                    }

                    if (count($order_detail_data) > 0)
                    {
                        $this->Order_detail_model->bulk_insert($order_detail_data);
                    } else {
                        $this->session->set_flashdata('message', 'Tender minimal memiliki sebuah produk. tidak boleh kosong');
                        $this->create();
                    }

                    $order_helpdesk = array(
                        'order_id' => $order_id,
                        'helpdesk_id' => $this->input->post('order_helpdesk')
                    );
                    $this->Order_model->insert_helpdesk($order_helpdesk);

                    $order_sales = array(
                        'order_id' => $order_id,
                        'sales_id' => $this->input->post('order_sales')
                    );
                    $this->Order_model->insert_sales($order_sales);

                    if ($this->db->affected_rows() > 0) {
                        $order_status = array(
                            'order_id' => $order_id,
                            'operation' => 'Admin ' . $this->data['user']->first_name . ' menambahkan tender baru',
                            'updated_user' => $this->data['user']->id,
                        );

                        $this->Order_status_model->insert($order_status);
                    }

                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'helpdesk');
                    $this->Order_model->insert_step($order_step);

                    $this->session->set_flashdata('message', 'Berhasil menambahkan tender baru!');
                    redirect(site_url('order'));

                } else {
                    $this->session->set_flashdata(
                        'message',
                        'Penambahan tender digagalkan. Anda tidak memiliki izin untuk menambahkan tender baru.');
                    $this->create();
                }
            }
        }
    }

    public function update($id)
    {
        $row = $this->Order_model->get_by_id($id);
        $pemesan = $this->Order_pemesan_model->get_by_order_id($id);
        $pembeli = $this->Order_pembeli_model->get_by_order_id($id);

        $this->data['has_change_tender']    = $this->permission->has_change_tender();
        $this->data['has_print_precall']    = $this->permission->has_print_precall();

        if ($row) {

            $this->data['button']           = 'Update';
            $this->data['action']           = site_url('order/update_action');
            $this->data['id']               = set_value('id', $row->id);
            $this->data['order_number']     = set_value('order_number', $row->order_number);
            $this->data['order_name']       = set_value('order_name', $row->order_name);
            $this->data['instance']         = set_value('instance', $row->instance);
            $this->data['package_location'] = set_value('package_location', $row->package_location);
            $this->data['work_unit']        = set_value('work_unit', $row->work_unit);
            $this->data['work_unit_address']= set_value('work_unit_address', $row->work_unit_address);
            $this->data['date_created']     = set_value('date_created', $row->date_created);
            $this->data['order_quantity']   = set_value('order_quantity', $row->order_quantity);
            $this->data['order_total']      = set_value('order_total', $row->order_total);
            $this->data['description']      = set_value('description', $row->description);
            $this->data['is_processed']     = set_value('is_processed', $row->is_processed);
            $this->data['is_completed']     = set_value('is_completed', $row->is_completed);
            $this->data['is_cancelled']     = set_value('is_cancelled', $row->is_cancelled);
            $this->data['updated_date']     = set_value('updated_date', $row->updated_date);
            $this->data['created_date']     = set_value('created_date', $row->created_date);
            $this->data['created_user']     = set_value('created_user', $row->created_user);
            $this->data['updated_user']     = set_value('updated_user', $row->updated_user);

            if ($pemesan) {
                $this->data['pemesan_name'] = set_value('pemesan_name', $pemesan->name);
                $this->data['pemesan_email']= set_value('pemesan_email', $pemesan->email);
                $this->data['pemesan_phone']= set_value('pemesan_phone', $pemesan->phone);
                $this->data['pemesan_id']   = set_value('pemesan_id', $pemesan->id);
            }

            if ($pembeli) {
                $this->data['pembeli_name'] = set_value('pembeli_name', $pembeli->name);
                $this->data['pembeli_title']= set_value('pembeli_title', $pembeli->title);
                $this->data['pembeli_nip']  = set_value('pembeli_nip', $pembeli->nip);
                $this->data['pembeli_id']   = set_value('pembeli_id', $pembeli->id);
            }

            $this->data['products'] = $this->Order_detail_model->get_by_order_id($id);

            $this->data['order_helpdesk']   = $this->Order_model->get_helpdesk_list();
            $this->data['order_sales']      = $this->Order_model->get_sales_list();
            $this->data['helpdesk_selected']= $this->Order_model->get_helpdesk($row->id);
            $this->data['sales_selected']   = $this->Order_model->get_sales($row->id);

            $this->template->load('order_form_update', $this->data);

        } else {
            $this->session->set_flashdata('message', 'Tender tidak ditemukan');
            redirect(site_url('order'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'order_number' => $this->input->post('order_number',TRUE),
                'order_name' => $this->input->post('order_name',TRUE),
                'instance' => $this->input->post('instance',TRUE),
                'package_location' => $this->input->post('package_location',TRUE),
                'work_unit' => $this->input->post('work_unit',TRUE),
                'work_unit_address' => $this->input->post('work_unit_address',TRUE),
                'date_created' => $this->input->post('date_created',TRUE),
                'order_quantity' => $this->input->post('order_quantity',TRUE),
                'order_total' => $this->input->post('order_total',TRUE),
                'description' => $this->input->post('description',TRUE),
                'is_processed' => $this->input->post('is_processed',TRUE),
                'is_completed' => $this->input->post('is_completed',TRUE),
                'is_cancelled' => $this->input->post('is_cancelled',TRUE),

                'updated_date' => date('Y-m-d H:i:s'),
                'updated_user' => $this->data['user']->id,
            );

            $this->Order_model->update($this->input->post('id', TRUE), $data);

            $order_id = $this->input->post('id', TRUE);

            $pemesan_data = array(
                'order_id'  => $order_id,
                'name'      => $this->input->post('pemesan_name',TRUE),
                'email'     => $this->input->post('pemesan_email',TRUE),
                'phone'     => $this->input->post('pemesan_phone',TRUE)
            );
            $this->Order_pemesan_model->update(
                $this->input->post('pemesan_id', TRUE),
                $pemesan_data);

            $pembeli_data = array(
                'order_id'  => $order_id,
                'name'      => $this->input->post('pembeli_name',TRUE),
                'title'     => $this->input->post('pembeli_title',TRUE),
                'nip'       => $this->input->post('pembeli_nip',TRUE)
            );
            $this->Order_pembeli_model->update(
                $this->input->post('pembeli_id', TRUE),
                $pembeli_data);

            if ($this->input->post('product_name')) {

                $order_detail_data = array();

                $prod_id        =  $this->input->post('product_id', TRUE);

                $prod_name      = $this->input->post('product_name', TRUE);
                $prod_price     = $this->input->post('product_price', TRUE);
                $prod_qty       = $this->input->post('product_quantity', TRUE);
                $prod_ship      = $this->input->post('product_shipping_cost', TRUE);
                $prod_subtotal  = $this->input->post('product_subtotal', TRUE);
                $prod_desc      = $this->input->post('product_description', TRUE);

                for ($i=0; $i<count($prod_name); $i++)
                {
                    $obj = array(
                        'id'            => $prod_id[$i],
                        'order_id'      => $order_id,
                        'product_name'  => $prod_name[$i],
                        'price'         => $prod_price[$i],
                        'quantity'      => $prod_qty[$i],
                        'shipping_cost' => $prod_ship[$i],
                        'subtotal'      => $prod_subtotal[$i],
                        'description'   => $prod_desc[$i],
                        'updated_user'  => $this->data['user']->id,
                    );
                    array_push($order_detail_data, $obj);
                }
                $this->Order_detail_model->bulk_update($order_detail_data);
            }

            $order_helpdesk = array(
                'order_id'      => $order_id,
                'helpdesk_id'   => $this->input->post('order_helpdesk')
            );
            $this->Order_model->update_helpdesk($order_helpdesk);

            $order_sales = array(
                'order_id'      => $order_id,
                'sales_id'   => $this->input->post('order_sales')
            );
            $this->Order_model->update_sales($order_sales);

            if ($this->db->affected_rows() > 0)
            {
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Super Admin ' .$this->data['user']->first_name . ' mengubah data tender',
                    'updated_user' => $this->data['user']->id,
                );

                $this->Order_status_model->insert($order_status);
            }


            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('order'));
        }
    }

    public function delete($id)
    {
        $row = $this->Order_model->get_by_id($id);

        if ($row) {
            $this->Order_model->delete($id);
            $this->Order_pemesan_model->delete_by_order_id($id);
            $this->Order_pembeli_model->delete_by_order_id($id);
            $this->Order_detail_model->delete_by_order_id($id);
            $this->Order_status_model->delete_by_order_id($id);

            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('order'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('order'));
        }
    }

    public function cancel($id)
    {
        $row = $this->Order_model->get_by_id($id);

        if ($row) {
            if ($row->is_completed != TRUE || $row->is_cancelled)
            {
                $this->Order_model->cancel($id);

                if ($this->db->affected_rows() > 0)
                {
                    $order_step = array(
                        'order_id' => $id,
                        'step' => 'cancel',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }
            }

            if ($this->db->affected_rows() > 0)
            {
                $this->session->set_flashdata('message', 'Berhasil membatalkan tender');
                redirect(site_url('order'));
            } else
            {
                $this->session->set_flashdata('message', 'Gagal membatalkan tender');
                redirect(site_url('order'));
            }
        } else {
            $this->session->set_flashdata('message', 'Tender tidak ditemukan');
            redirect(site_url('order'));
        }
    }

    public function detail($detail_id)
    {
        $this->data['has_add_proses']           = $this->permission->has_add_detail_proses();
        $this->data['has_change_proses']        = $this->permission->has_change_detail_proses();

        $this->data['has_add_sp_number']        = $this->permission->has_add_detail_sp_number();
        $this->data['has_change_sp_number']     = $this->permission->has_change_detail_sp_number();

        $this->data['has_add_so_number']        = $this->permission->has_add_detail_so_number();
        $this->data['has_change_so_number']     = $this->permission->has_change_detail_so_number();

        $this->data['has_add_po_number']        = $this->permission->has_add_detail_po_number();
        $this->data['has_change_po_number']     = $this->permission->has_change_detail_po_number();

        $this->data['has_add_do_number']        = $this->permission->has_add_detail_do_number();
        $this->data['has_change_do_number']     = $this->permission->has_change_detail_do_number();

        $this->data['has_add_inv_number']       = $this->permission->has_add_detail_inv_number();
        $this->data['has_change_inv_number']    = $this->permission->has_change_detail_inv_number();

        $this->data['has_add_received']         = $this->permission->has_add_detail_received();
        $this->data['has_change_received']      = $this->permission->has_change_detail_received();

        $this->data['has_add_send']             = $this->permission->has_add_detail_send();
        $this->data['has_change_send']          = $this->permission->has_change_detail_send();

        $this->data['has_add_paid']             = $this->permission->has_add_detail_paid();
        $this->data['has_change_paid']          = $this->permission->has_change_detail_paid();

        $data = $this->Order_detail_model->get_by_id($detail_id);

        $this->data['id']               = $data->id;
        $this->data['order_id']         = $data->order_id;
        $this->data['product_name']     = $data->product_name;
        $this->data['quantity']         = $data->quantity;
        $this->data['price']            = $data->price;
        $this->data['shipping_cost']    = $data->shipping_cost;
        $this->data['subtotal']         = $data->subtotal;
        $this->data['description']      = $data->description;
        $this->data['is_processed']     = $data->is_processed;
        $this->data['sp_number']        = $data->sp_number;
        $this->data['so_number']        = $data->so_number;
        $this->data['po_number']        = $data->po_number;
        $this->data['is_received']      = $data->is_received;
        $this->data['do_number']        = $data->do_number;
        $this->data['is_send']          = $data->is_send;
        $this->data['validation_number']= $data->validation_number ;
        $this->data['inv_number']       = $data->inv_number;
        $this->data['is_paid']          = $data->is_paid;
        $this->data['updated_date']     = $data->updated_date;
        $this->data['updated_user']     = $data->updated_user;

        $this->data['button']           = 'Edit';
        $this->data['action']           = site_url('order/detail_action');

        $this->template->load('order_detail_form', $this->data);
    }

    public function detail_action()
    {
        $processed_detail_order = $this->input->post('product_is_processed', TRUE);
        $id                     = $this->input->post('id', TRUE);
        $order_id               = $this->input->post('order_id', TRUE);
        $sp_number              = $this->input->post('product_sp_number', TRUE);
        $so_number              = $this->input->post('product_so_number', TRUE);
        $po_number              = $this->input->post('product_po_number', TRUE);
        $is_received            = $this->input->post('product_is_received', TRUE);
        $do_number              = $this->input->post('product_do_number', TRUE);
        $is_send                = $this->input->post('product_is_send', TRUE);
        $inv_number             = $this->input->post('product_inv_number', TRUE);
        $is_paid                = $this->input->post('product_is_paid', TRUE);
        $validation             = $this->input->post('validation_number', TRUE);
        $produk                 = $this->Order_detail_model->get_by_id($id);

        $products               = $this->Order_detail_model->get_by_order_id($order_id);

        /**
         * DATA VERIFICATION
         * STAKEHOLDER: Helpdesk
         */
        if (count($processed_detail_order) > 0)
        {
            if ($sp_number != '') {
                $detail_data = array(
                    'is_processed' => $processed_detail_order == 0 ? FALSE : TRUE
                );

                if ($sp_number != '')
                {
                    $this->Order_detail_model->update($id, $detail_data);
                }

                if ($this->db->affected_rows() > 0) {
                    $status = array();

                    $proses = $processed_detail_order == 1 ? 'diproses.' : 'tidak diproses.';

                    $order_detail_status = array(
                        'order_id' => $order_id,
                        'operation' => 'Helpdesk ' . $this->data['user']->first_name .
                            ' memperbarui status produk ' . $produk->product_name .
                            ' untuk ' . $proses,
                        'updated_user' => $this->data['user']->id,
                    );
                    array_push($status, $order_detail_status);

                    $processed_products = $this->Order_detail_model->get_processed($order_id);

                    if (count($products) == count($processed_products)) {

                        $processed = array(
                            'is_processed' => TRUE
                        );

                        $this->Order_model->update($order_id, $processed);

                        $order_step = array(
                            'order_id' => $order_id,
                            'step' => 'adminso',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_model->update_step($order_step);

                        $order_status = array(
                            'order_id' => $order_id,
                            'operation' => 'Semua data produk sudah diverifikasi, Tender diproses.',
                            'updated_user' => $this->data['user']->id,
                        );
                        array_push($status, $order_status);
                    }

                    $this->Order_status_model->bulk_insert($status);
                }
            } else {
                redirect(site_url('order/detail/' . $id));
            }
        }

        /**
         * SP NUMBER INPUT
         * STAKEHOLDER : Helpdesk
         */
        if (count($sp_number) > 0)
        {
            if ($sp_number != '')
            {
                $detail_data = array(
                    'sp_number' => $sp_number
                );

                $this->Order_detail_model->update($id, $detail_data);
            } else {
                $this->session->set_flashdata('message', 'No. SP diperlukan');
                redirect(site_url('order/detail/' . $id));
            }

            if ($this->db->affected_rows() > 0)
            {
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Helpdesk ' .$this->data['user']->first_name .
                        ' menambahkan nomor SP untuk produk ' . $produk->product_name,
                    'updated_user' => $this->data['user']->id
                );

                $this->Order_status_model->insert($order_status);
            }
        }

        /**
         * SO NUMBER INPUT IF NEED TO BUY
         * STAKEHOLDER : Admin SO
         */
        if (count($so_number) > 0)
        {
            if ($so_number != '') {
                $detail_data = array(
                    'so_number' => $so_number
                );
                $this->Order_detail_model->update($id, $detail_data);
            }

            if ($this->db->affected_rows() > 0)
            {
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Admin SO ' .$this->data['user']->first_name .
                        ' menambahkan nomor SO untuk produk ' . $produk->product_name,
                    'updated_user' => $this->data['user']->id
                );

                $this->Order_status_model->insert($order_status);

                $have_so = $this->Order_detail_model->get_have_so($order_id);

                if (count($products) == count($have_so))
                {
                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'purchasing',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }
            }
        }

        /*
         * PO NUMBER INPUT IF SO EXISTS
         * STAKEHOLDER : Purchasing
         */
        if (count($po_number) > 0)
        {
            $detail_data = array(
                'po_number' => $po_number
            );

            $this->Order_detail_model->update($id, $detail_data);

            if ($this->db->affected_rows() > 0)
            {
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Purchasing ' .$this->data['user']->first_name .
                        ' menambahkan nomor PO untuk produk ' . $produk->product_name,
                    'updated_user' => $this->data['user']->id
                );

                $this->Order_status_model->insert($order_status);

                $have_po = $this->Order_detail_model->get_have_po($order_id);

                if (count($products) == count($have_po))
                {
                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'warehouse',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }
            }
        }

        /*
         * IF Product has beed purchased and warehouse pending
         * STAKEHOLDER : Warehouse
         */

        if (count($is_received) > 0)
        {
            if ($is_received == 1)
            {
                $detail_data = array(
                    'is_received' => $is_received == 1 ? TRUE : FALSE
                );
                $this->Order_detail_model->update($id, $detail_data);
            } else {
                redirect(site_url('order/update' .  $order_id));
            }


            if ($this->db->affected_rows() > 0)
            {
                $status = array();

                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Warehouse ' .$this->data['user']->first_name .
                        ' menerima produk ' . $produk->product_name .
                        ' di gudang.',
                    'updated_user' => $this->data['user']->id
                );
                array_push($status, $order_status);

                $received_products = $this->Order_detail_model->get_received($order_id);

                if (count($products) == count($received_products))
                {
                    $order_status = array(
                        'order_id' => $order_id,
                        'operation' => 'Semua produk telah diterima oleh warehouse.',
                        'updated_user' => $this->data['user']->id,
                    );
                    array_push($status, $order_status);

                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'admindo',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }

                $this->Order_status_model->bulk_insert($status);
            }
        }

        /*
         * DO NUMBER
         * STAKEHOLDER : Admin DO
         */
        if (count($do_number) > 0)
        {
            if ($do_number != '')
            {
                $detail_data = array(
                    'do_number' => $do_number
                );
                $this->Order_detail_model->update($id, $detail_data);
            }

            if ($this->db->affected_rows() > 0)
            {
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Admin DO ' .$this->data['user']->first_name .
                        ' menambahkan nomor DO untuk produk ' . $produk->product_name,
                    'updated_user' => $this->data['user']->id
                );

                $this->Order_status_model->insert($order_status);

                $have_do = $this->Order_detail_model->get_have_do($order_id);

                if (count($products) == count($have_do))
                {
                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'delivery',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }
            }
        }

        /*
         * If product has been send to buyer from warehouse
         * STAKEHOLDER : Delivery
         */
        if (count($is_send) > 0)
        {
            if (count($validation) > 0 && $validation != '')
            {
                $detail_data = array(
                    'is_send' => $is_send == 1 ? TRUE : FALSE,
                    'validation_number' => $validation
                );

                $this->Order_detail_model->update($id, $detail_data);
            } else {
                $this->session->set_flashdata('message', 'Nomor pengiriman tidak boleh kosong');
                redirect(site_url('order/detail/'.$id));
            }

            if ($this->db->affected_rows() > 0)
            {
                $status = array();

                $order_detail_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Delivery ' . $this->data['user']->first_name .
                        ' telah mengirim produk ' . $produk->product_name . ' ke pembeli.',
                    'updated_user' => $this->data['user']->id
                );
                array_push($status, $order_detail_status);

                $sent_products = $this->Order_detail_model->get_sent($order_id);

                if (count($products) == count($sent_products))
                {
                    $order_status = array(
                        'order_id' => $order_id,
                        'operation' => 'Semua produk sudah dikirim ke pembeli, Tender selesai.',
                        'updated_user' => $this->data['user']->id,
                    );
                    array_push($status, $order_status);

                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'admininv',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }

                $this->Order_status_model->bulk_insert($status);
            }
        }

        /*
         * INVOICE NUMBER INPUT
         * STAKEHOLDER : Admin INV
         */
        if (count($inv_number) > 0)
        {
            if ($inv_number != '')
            {
                $detail_data = array(
                    'inv_number' => $inv_number
                );
                $this->Order_detail_model->update($id, $detail_data);
            }

            if ($this->db->affected_rows() > 0)
            {
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Admin invoice ' .$this->data['user']->first_name .
                        ' menambahkan nomor invoice untuk produk ' . $produk->product_name,
                    'updated_user' => $this->data['user']->id
                );

                $this->Order_status_model->insert($order_status);

                $have_inv = $this->Order_detail_model->get_have_inv($order_id);

                if (count($products) == count($have_inv))
                {
                    $order_step = array(
                        'order_id' => $order_id,
                        'step' => 'finance',
                        'updated_user' => $this->data['user']->id);
                    $this->Order_model->update_step($order_step);
                }
            }
        }


        /*
         * If product has been paid from buyer
         * STAKEHOLDER = Finance
         * TODO: Make order completed
         */
        if (count($is_paid) > 0)
        {
            $detail_data = array(
                'is_paid' => $is_paid == 0 ? FALSE : TRUE
            );

            $this->Order_detail_model->update($id, $detail_data);

            if ($this->db->affected_rows() > 0)
            {
                $paid_products = $this->Order_detail_model->get_paid($order_id);
                $status = array();

                $edited_product = $this->Order_detail_model->get_by_id($id);
                $order_status = array(
                    'order_id' => $order_id,
                    'operation' => 'Finance ' . $this->data['user']->first_name .
                        ' menerima pembayaran untuk produk "'
                        . $edited_product->product_name. '".',
                    'updated_user' => $this->data['user']->id
                );

                array_push($status, $order_status);

                if (count($products) == count($paid_products))
                {
                    $completed = array(
                        'is_completed' => TRUE
                    );

                    $this->Order_model->update($order_id, $completed);

                    $obj = array(
                        'order_id' => $order_id,
                        'operation' => 'Finance grup telah menerima semua pembayaran
                        untuk produk tender. Tender selesai',
                        'updated_user' => $this->data['user']->id
                    );

                    array_push($status, $obj);
                }

                $this->Order_status_model->bulk_insert($status);
            }
        }

        redirect(site_url('order/update/'.$order_id));
    }

    public function change()
    {
        $field = $this->input->post('name', TRUE);

        $order_id = $this->input->post('id', TRUE);
        $product_id = $this->input->post('product_id', TRUE);
        $value = $this->input->post('value', TRUE);
        $curr_val = $this->input->post('curr_val', TRUE);
        $courier = $this->input->post('courier', TRUE);

        $response = array(
            'value' => "",
            'message' => "");

        if (explode('_', $field)[0] == 'product')
        {
            $qty = intval($this->input->post('curr_qty', TRUE));
            $price = intval($this->input->post('curr_price', TRUE));
            $ship_cost = intval($this->input->post('curr_ship_cost', TRUE));
        }

        switch ($field)
        {
            case 'order_number':

                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan pada nomor tender.";
                } else if (intval($value) < 1) {
                    $response['message'] = "Nomor tender tidak boleh 0.";
                } else {
                    $data = array(
                        'order_number' => intval($value)
                    );
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = intval($value);
                    } else {
                        $response['message'] = "Gagal mengubah nomor tender";
                    }
                }
                break;
            case 'order_name':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan pada ID Paket.";
                } else if (strlen($value) < 3) {
                    $response['message'] = "ID Paket harus lebih dari 3 karakter.";
                } else {
                    $data = array(
                        'order_name' => $value);

                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah ID Paket";
                    }
                }
                break;
            case 'order_quantity':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan pada kuantitas.";
                } else if (intval($value) < 1) {
                    $response['message'] = "Kuantitas tidak boleh 0.";
                } else {
                    $data = array(
                        'order_quantity' => intval($value)
                    );
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = intval($value);
                    } else {
                        $response['message'] = "Gagal mengubah kuantitas";
                    }
                }
                break;
            case 'order_total':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan pada total tender.";
                } else if (intval($value) < 1) {
                    $response['message'] = "total tidak boleh 0.";
                } else {
                    $data = array(
                        'order_total' => intval($value)
                    );
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = intval($value);
                    } else {
                        $response['message'] = "Gagal mengubah total";
                    }
                }
                break;
            case 'date_created':
                if (mysql_to_unix($value) == human_to_unix($curr_val))
                {
                    $response['message'] = "Tidak ada perubahan data pada tanggal buat";
                } else {
                    $data = array(
                        'date_created' => $value);
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = date('d M Y', mysql_to_unix($value));
                    } else {
                        $response['message'] = "Gagal mengubah tanggal buat";
                    }
                }
                break;
            case 'order_location':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada lokasi paket";
                } else {
                    $data = array(
                        'package_location' => $value);
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah lokasi paket";
                    }
                }
                break;
            case 'order_unit':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada satuan kerja";
                } else {
                    $data = array(
                        'work_unit' => $value);
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah satuan kerja";
                    }
                }
                break;
            case 'order_unit_address':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada alamat satuan kerja";
                } else {
                    $data = array(
                        'work_unit_address' => $value);
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah alamat satuan kerja";
                    }
                }
                break;
            case 'order_description':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada deskripsi tender";
                } else {
                    $data = array(
                        'description' => $value);
                    $change = $this->Order_model->update($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah deskripsi tender";
                    }
                }
                break;
            case 'pemesan_name':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada nama pemesan";
                } else {
                    $data = array(
                        'name' => $value);
                    $change = $this->Order_pemesan_model->update_by_order_id($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah nama pemesan";
                    }
                }
                break;
            case 'pemesan_email':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada email pemesan";
                } else {
                    $data = array(
                        'email' => $value);
                    $change = $this->Order_pemesan_model->update_by_order_id($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah email pemesan";
                    }
                }
                break;
            case 'pemesan_phone':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada telp. pemesan";
                } else {
                    $data = array(
                        'phone' => $value);
                    $change = $this->Order_pemesan_model->update_by_order_id($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah telp. pemesan" . $change;
                    }
                }
                break;
            case 'pembeli_name':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada nama pembeli";
                } else {
                    $data = array(
                        'name' => $value);
                    $change = $this->Order_pembeli_model->update_by_order_id($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah nama pembeli";
                    }
                }
                break;
            case 'pembeli_title':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada jabatan pembeli";
                } else {
                    $data = array(
                        'title' => $value);
                    $change = $this->Order_pembeli_model->update_by_order_id($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah jabatan pembeli";
                    }
                }
                break;
            case 'pembeli_nip':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada NIP pembeli";
                } else {
                    $data = array(
                        'nip' => intval($value));
                    $change = $this->Order_pembeli_model->update_by_order_id($order_id, $data);

                    if ($change > 0)
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah NIP pemesan";
                    }
                }
                break;
            case 'order_helpdesk':
                $data = array(
                    'order_id' => $order_id,
                    'helpdesk_id' => intval($value));
                $change = $this->Order_model->update_helpdesk($data);

                if ($change > 0)
                {
                    $response['value'] = "";
                } else {
                    $response['message'] = "Gagal mengubah helpdesk tender";
                }
                break;
            case 'order_sales':
                $data = array(
                    'order_id' => $order_id,
                    'sales_id' => intval($value));
                $change = $this->Order_model->update_sales($data);

                if ($change > 0)
                {
                    $response['value'] = "";
                } else {
                    $response['message'] = "Gagal mengubah sales tender";
                }
                break;
            case 'product_name':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada nama produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'product_name' => $value);
                    $this->Order_detail_model->update_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah nama produk";
                    }
                }
                break;
            case 'product_price':
                $subtotal = (intval($value) * $qty) + $ship_cost;

                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada harga produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'price' => intval($value),
                        'subtotal' => $subtotal);
                    $this->Order_detail_model->update_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = number_format(intval($value), 0, '', '.');
                        $response['subtotal'] = number_format($subtotal, 0, '', '.');

                        $subtotal_products = $this->Order_detail_model->get_subtotal_by_order_id($order_id);
                        $grand_total = [];
                        foreach($subtotal_products as $item)
                        {
                            array_push($grand_total, intval($item['subtotal']));
                        }

                        $this->Order_model->update($order_id, array('order_total' => array_sum($grand_total)));

                        $response['grand_total'] = number_format(array_sum($grand_total), 0, '', '.');
                    } else {
                        $response['message'] = 'Gagal mengubah harga produk';
                    }
                }
                break;
            case 'product_quantity':
                $subtotal = ($price * intval($value)) + $ship_cost;

                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada kuantitas produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'quantity' => intval($value),
                        'subtotal' => $subtotal);
                    $this->Order_detail_model->update_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = intval($value);
                        $response['subtotal'] = number_format($subtotal, 0, '', '.');

                        $subtotal_products = $this->Order_detail_model->get_subtotal_by_order_id($order_id);
                        $grand_total = [];
                        foreach($subtotal_products as $item)
                        {
                            array_push($grand_total, intval($item['subtotal']));
                        }

                        $this->Order_model->update($order_id, array('order_total' => array_sum($grand_total)));

                        $response['grand_total'] = number_format(array_sum($grand_total), 0, '', '.');
                    } else {
                        $response['message'] = 'Gagal mengubah kuantitas produk';
                    }
                }
                break;
            case 'product_shipping_cost':
                $subtotal = ($price * $qty) + intval($value);

                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada ongkir produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'shipping_cost' => intval($value),
                        'subtotal' => $subtotal);
                    $this->Order_detail_model->update_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = number_format(intval($value), 0, '', '.');
                        $response['subtotal'] = number_format($subtotal, 0, '', '.');

                        $subtotal_products = $this->Order_detail_model->get_subtotal_by_order_id($order_id);
                        $grand_total = [];
                        foreach($subtotal_products as $item)
                        {
                            array_push($grand_total, intval($item['subtotal']));
                        }

                        $this->Order_model->update($order_id, array('order_total' => array_sum($grand_total)));

                        $response['grand_total'] = number_format(array_sum($grand_total), 0, '', '.');
                    } else {
                        $response['message'] = 'Gagal mengubah ongkir produk';
                    }
                }
                break;
            case 'product_po':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data PO pada produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'po_number' => $value);
                    $this->Order_detail_model->update_by_order_id($data);

                    if ($this->db->affected_rows() > 0)
                    {
                        $trigger_filled_all = array(
                            'order_id' => $order_id,
                            'field' => 'po_number !=',
                            'condition' => null);
                        $tender_product_qty = $this->Order_detail_model->is_filled_all($trigger_filled_all);

                        if ($tender_product_qty)
                        {
                            $order_step = array(
                                'order_id' => $order_id,
                                'step' => 'warehouse',
                                'updated_user' => $this->data['user']->id);
                            $this->Order_model->update_step($order_step);

                            $obj = array(
                                'order_id' => $order_id,
                                'operation' => $this->data['user']->first_name .' telah membuat PO, menunggu diterima gudang.',
                                'updated_user' => $this->data['user']->id);
                            $this->Order_status_model->insert($obj);
                        }

                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah No.PO produk";
                    }
                }
                break;
            case 'product_is_received':

                $data = array(
                    'order_id' => $order_id,
                    'product_id' => $product_id,
                    'is_received' => $value ==  'true' ? true : false,
                    'received_date' => $value ==  'true' ? date('Y-m-d H:i:s') : null);

                $this->Order_detail_model->update_by_order_id($data);

                if ($this->db->affected_rows() > 0)
                {
                    $trigger_filled_all = array(
                        'order_id' => $order_id,
                        'field' => 'is_received',
                        'condition' => TRUE);
                    $tender_product_qty = $this->Order_detail_model->is_filled_all($trigger_filled_all);

                    if ($tender_product_qty)
                    {
                        $order_step = array(
                            'order_id' => $order_id,
                            'step' => 'admindo',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_model->update_step($order_step);

                        $obj = array(
                            'order_id' => $order_id,
                            'operation' => $this->data['user']->first_name .' telah menerima semua produk tender, lanjut pembuatan DO.',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_status_model->insert($obj);
                    }

                    $response['value'] = $value ==  'true' ? mdate('%d %M %Y %h:%i %a', mysql_to_unix($data['received_date'])) : null;
                } else {
                    $response['message'] = "Gagal mengubah produk yang diterima gudang";
                }

                break;
            case 'product_do':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data DO pada produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'do_number' => $value);
                    $this->Order_detail_model->update_by_order_id($data);

                    if ($this->db->affected_rows() > 0)
                    {
                        $trigger_filled_all = array(
                            'order_id' => $order_id,
                            'field' => 'do_number !=',
                            'condition' => null);
                        $tender_product_qty = $this->Order_detail_model->is_filled_all($trigger_filled_all);

                        if ($tender_product_qty)
                        {
                            $order_step = array(
                                'order_id' => $order_id,
                                'step' => 'delivery',
                                'updated_user' => $this->data['user']->id);
                            $this->Order_model->update_step($order_step);

                            $obj = array(
                                'order_id' => $order_id,
                                'operation' => $this->data['user']->first_name .' telah membuat DO, lanjut pengiriman.',
                                'updated_user' => $this->data['user']->id);
                            $this->Order_status_model->insert($obj);
                        }

                        $response['value'] = $value;
                    } else {
                        $response['message'] = "Gagal mengubah No.DO produk";
                    }
                }
                break;
            case 'product_is_send':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data pada pengiriman produk";
                } else {
                    if ($value == '')
                    {
                        $response['message'] = "Data pengiriman tidak boleh kosong";
                    } else {
                        $data = array(
                            'order_id' => $order_id,
                            'product_id' => $product_id,
                            'is_send' => $value != '' ? true : false,
                            'validation_number' => $value != '' ? $value : null,
                            'courier' => $value != '' ? $courier : null,
                            'send_date' => $value != '' ? date('Y-m-d H:i:s') : null);
                        $this->Order_detail_model->update_by_order_id($data);

                        if ($this->db->affected_rows())
                        {
                            $trigger_filled_all = array(
                                'order_id' => $order_id,
                                'field' => 'is_send',
                                'condition' => TRUE);
                            $tender_product_qty = $this->Order_detail_model->is_filled_all($trigger_filled_all);

                            if ($tender_product_qty)
                            {
                                $order_step = array(
                                    'order_id' => $order_id,
                                    'step' => 'admininv',
                                    'updated_user' => $this->data['user']->id);
                                $this->Order_model->update_step($order_step);

                                $obj = array(
                                    'order_id' => $order_id,
                                    'operation' => $this->data['user']->first_name .' telah mengirim produk tender, lanjut pembuatan invoice.',
                                    'updated_user' => $this->data['user']->id);
                                $this->Order_status_model->insert($obj);
                            }

                            $response['value'] = $value;
                        } else {
                            $response['message'] = "Gagal mengubah pengiriman produk";
                        }
                    }
                }
                break;
            case 'order_sp':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data SP pada tender";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'is_processed' => TRUE,
                        'sp_number' => $value);
                    $this->Order_detail_model->update_detail_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $this->Order_model->update($order_id, array('is_processed' => TRUE));
                        $response['value'] = $value;
                        $order_step = array(
                            'order_id' => $order_id,
                            'step' => 'adminso',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_model->update_step($order_step);

                        $obj = array(
                            'order_id' => $order_id,
                            'operation' => $this->data['user']->first_name .'ctelah membuat SP, Tender dapat diproses.',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_status_model->insert($obj);
                    } else {
                        $response['message'] = "Gagal mengubah No.SP produk";
                    }
                }
                break;
            case 'order_so':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data SO pada tender";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'so_number' => $value);
                    $this->Order_detail_model->update_detail_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = $value;
                        $order_step = array(
                            'order_id' => $order_id,
                            'step' => 'purchasing',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_model->update_step($order_step);

                        $obj = array(
                            'order_id' => $order_id,
                            'operation' => $this->data['user']->first_name .' telah membuat SO, lanjut ke pembelian',
                            'updated_user' => $this->data['user']->id
                        );
                        $this->Order_status_model->insert($obj);
                    } else {
                        $response['message'] = "Gagal mengubah No.SO tender";
                    }
                }
                break;
            case 'order_inv':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data invoice pada produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'inv_number' => $value);
                    $this->Order_detail_model->update_detail_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = $value;
                        $order_step = array(
                            'order_id' => $order_id,
                            'step' => 'finance',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_model->update_step($order_step);

                        $obj = array(
                            'order_id' => $order_id,
                            'operation' => $this->data['user']->first_name .' telah membuat invoice, lanjut finance mengecek pembayaran.',
                            'updated_user' => $this->data['user']->id
                        );
                        $this->Order_status_model->insert($obj);

                    } else {
                        $response['message'] = "Gagal mengubah No.invoice produk";
                    }
                }
                break;
            case 'order_paid':
                if ($value == $curr_val)
                {
                    $response['message'] = "Tidak ada perubahan data terbayar pada produk";
                } else {
                    $data = array(
                        'order_id' => $order_id,
                        'is_paid' => $value != '' ? true : false,
                        'paid_date' => $value != '' ? $value : null);
                    $this->Order_detail_model->update_detail_by_order_id($data);

                    if ($this->db->affected_rows())
                    {
                        $response['value'] = $value;

                        $completed = array(
                            'is_completed' => TRUE
                        );

                        $this->Order_model->update($order_id, $completed);

                        $order_step = array(
                            'order_id' => $order_id,
                            'step' => 'completed',
                            'updated_user' => $this->data['user']->id);
                        $this->Order_model->update_step($order_step);

                        $obj = array(
                            'order_id' => $order_id,
                            'operation' => 'Finance grup telah menerima pembayaran
                                untuk tender. Tender selesai',
                            'updated_user' => $this->data['user']->id
                        );
                        $this->Order_status_model->insert($obj);
                    } else {
                        $response['message'] = "Gagal mengubah tender terbayar";
                    }
                }
                break;
            case 'status':
                $state = 'status';
                break;
        }
        echo json_encode($response);
    }

    public function get_helpdesks_ajax()
    {
        $results = array();
        $results['suggestions'] = array();
        $helpdesks = $this->Order_model->get_helpdesk_list();

        header('Content-Type:application/javascript');

        foreach ($helpdesks as $helpdesk)
        {
            $obj = array(
                'value' => $helpdesk->first_name,
                'data' => $helpdesk->id
            );

            array_push($results['suggestions'], $obj);
        }

        echo json_encode($results);
    }

    public function get_sales_ajax()
    {
        $results = array();
        $results['suggestions'] = array();

        $sales = $this->Order_model->get_sales_list();

        header('Content-Type:application/javascript');

        foreach ($sales as $item)
        {
            $obj = array(
                'value' => $item->first_name,
                'data' => $item->id
            );

            array_push($results['suggestions'], $obj);
        }

        echo json_encode($results);
    }

    public function precall($order_id)
    {
        $order_data = $this->Order_model->get_by_id($order_id);

        if ($order_data)
        {
            $helpdesk = $this->Order_model->get_helpdesk($order_data->id);

            $this->data['helpdesk']         = $helpdesk->first_name . " " . $helpdesk->last_name;
            $this->data['page_title']       = " Precall Tender No." . $order_data->order_number;
            $this->data['order_number']     = $order_data->order_number;
            $this->data['order_name']       = $order_data->order_name;
            $this->data['order_quantity']   = $order_data->order_quantity;
            $this->data['order_total']      = $order_data->order_total;
            $this->data['date_created']     = $order_data->date_created;
            $this->data['package_location'] = $order_data->package_location;

            $this->data['products']         = $this->Order_detail_model->get_by_order_id($order_id);

            $this->load->view('precall', $this->data);
        }

    }

    public function total()
    {
        if (!$this->input->post('year'))
            $year = date('Y');
        else
            $year = $this->input->post('year');

        $this->data['years']    = $this->Order_model->get_total_available_years();
        $this->data['year']     = $year;
        $this->data['title']    = 'Pertahun ' . $year;
        $this->data['js']       = '<script src="' . base_url('assets/js/vendor/Chart.min.js') . '"></script>';

        $this->template->load('total', $this->data);
    }

    public function get_total_yearly()
    {
        if ($this->input->post('year') == 0)
            $year = date('Y');
        else
            $year = $this->input->post('year');

        $data_this_year = $this->Order_model->get_total_yearly($year);

        echo json_encode($data_this_year);
    }

    public function get_total_monthly()
    {
        $results = array();
        $months = range(0, 12);

        $data_this_year = $this->Order_model->get_total('yearly');

        foreach ($data_this_year as $item)
        {
            if (date('m', mysql_to_unix($item->date_created)) == date('m'))
            {
                echo "same month : " . date('m'). "<br> ";
            }
        }

        var_dump($data_this_year);
    }

    /**
     * _orders
     *
     * @param $per_page
     * @param $start
     * @param $q
     * @return mixed
     */
    public function _orders($per_page, $start, $q, $h, $s, $y, $m, $l)
    {
        if ($this->ion_auth->is_admin())
            return $this->Order_model->get_limit_data(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        /*if ($this->ion_auth->is_helpdesk())
            return $this->Order_model->get_limit_data_helpdesk(
                $per_page, $start, $q, $this->data['user']->id);*/

        if ($this->ion_auth->is_sales())
            return $this->Order_model->get_limit_data_sales(
                $per_page, $start, $this->data['user']->id, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_helpdesk())
            return $this->Order_model->get_limit_data(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_adminso())
            return $this->Order_model->get_limit_data_adminso(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_purchasing())
            return $this->Order_model->get_limit_data_purchasing(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_warehouse())
            return $this->Order_model->get_limit_data_warehouse(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_admindo())
            return $this->Order_model->get_limit_data_admindo(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_delivery())
            return $this->Order_model->get_limit_data_delivery(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_admininv())
            return $this->Order_model->get_limit_data_admininv(
                $per_page, $start, $q, $h, $s, $y, $m, $l);

        if ($this->ion_auth->is_finance())
            return $this->Order_model->get_limit_data_finance(
                $per_page, $start, $q, $h, $s, $y, $m, $l);
    }

    public function package_location()
    {
        $keyword = $this->input->post('query');

        $result = $this->Kota_model->get_data($keyword);

        $data = array();
        $data['suggestions'] = array();

        foreach ($result as $key)
        {
            $obj = array(
                'value' => $key->kota,
                'data' => $key->id
            );
            array_push($data['suggestions'], $obj);
        }

        header('Content-type: application/json');

        echo json_encode($data);

    }

    public function _rules()
    {
        $this->form_validation->set_rules(
            'order_number', 'order number',
            'trim|required|min_length[1]');

        $this->form_validation->set_rules('order_name', 'order name', 'trim|required');
        $this->form_validation->set_rules('instance', 'instance', 'trim|min_length[3]');
        $this->form_validation->set_rules('package_location', 'package location', 'trim|min_length[3]');
        $this->form_validation->set_rules('work_unit', 'work unit', 'trim|min_length[3]');
        $this->form_validation->set_rules('work_unit_address', 'work unit address', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('order_quantity', 'order quantity', 'trim|required');
        $this->form_validation->set_rules('order_total', 'order total', 'trim|required');
        $this->form_validation->set_rules('description', 'description', 'trim|min_length[3]');
        $this->form_validation->set_rules('order_helpdesk', 'order_helpdesk', 'trim|required');
        $this->form_validation->set_rules('order_sales', 'order_sales', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function remark()
    {
        $response = array();
        $response['value'] = '';
        $data = array();
        $remark_id = $this->input->post('id', TRUE);
        $order_id = $this->input->post('order_id', TRUE);
        $curr_val = $this->input->post('curr_val', TRUE);
        $remark = $this->input->post('value', TRUE);

        if (!$remark || $remark == $curr_val || $order_id && $remark_id)
        {
            $response['message'] = 'Cannot add duplicate or empty data.';
        } else {
            $order_id ? $data['order_id'] = $order_id : $data['id'] = $remark_id;
            $data['remark'] = $remark;
            $data['created_user'] = $this->data['user']->id;
            $data['created_date'] = date('Y-m-d H:i:s');

            $this->Order_model->change_remark($data);

            if ($this->db->affected_rows() > 0)
            {
                $response['value'] = $remark;
            }
        }

        echo json_encode($response);
    }

    public function _permission()
    {
        $this->data['has_change_tender']    = $this->permission->has_change_tender();
        $this->data['has_add_tender']       = $this->permission->has_add_tender();
        $this->data['has_cancel_tender']    = $this->permission->has_cancel_tender();

        $this->data['has_add_proses']       = $this->permission->has_add_detail_proses();
        $this->data['has_change_proses']    = $this->permission->has_change_detail_proses();

        $this->data['has_add_sp']           = $this->permission->has_add_detail_sp_number();
        $this->data['has_change_sp']        = $this->permission->has_change_detail_sp_number();

        $this->data['has_add_so']           = $this->permission->has_add_detail_so_number();
        $this->data['has_change_so']        = $this->permission->has_change_detail_so_number();

        $this->data['has_add_po']           = $this->permission->has_add_detail_po_number();
        $this->data['has_change_po']        = $this->permission->has_change_detail_po_number();

        $this->data['has_add_do']           = $this->permission->has_add_detail_do_number();
        $this->data['has_change_do']        = $this->permission->has_change_detail_do_number();

        $this->data['has_add_inv']          = $this->permission->has_add_detail_inv_number();
        $this->data['has_change_inv']       = $this->permission->has_change_detail_inv_number();

        $this->data['has_add_received']     = $this->permission->has_add_detail_received();
        $this->data['has_change_received']  = $this->permission->has_change_detail_received();

        $this->data['has_add_send']         = $this->permission->has_add_detail_send();
        $this->data['has_change_send']      = $this->permission->has_change_detail_send();

        $this->data['has_add_paid']         = $this->permission->has_add_detail_paid();
        $this->data['has_change_paid']      = $this->permission->has_change_detail_paid();

        $this->data['has_print_precall']    = $this->permission->has_print_precall();

        $this->data['has_add_remark']       = $this->permission->has_add_remark();

        $this->data['has_filter_helpdesk']  = $this->permission->has_filter_helpdesk();
        $this->data['has_filter_status']    = $this->permission->has_filter_status();
        $this->data['has_filter_date']      = $this->permission->has_filter_date();
        $this->data['has_filter_location']  = $this->permission->has_filter_location();
        $this->data['has_see_total_summary']= $this->permission->has_see_total_summary();

        return $this->data;
    }
}