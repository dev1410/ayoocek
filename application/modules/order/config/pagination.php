<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['query_string_segment'] = 'start';

$config['full_tag_open'] = '<ul class="pagination text-right" role="navigation" aria-label="Pagination">';
$config['full_tag_close'] = '</ul>';

$config['first_link'] = 'Pertama';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';

$config['last_link'] = 'Terakhir';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

$config['next_link'] = 'Selanjutnya';
$config['next_tag_open'] = '<li class="pagination-next">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = 'Sebelumnya';
$config['prev_tag_open'] = '<li class="pagination-previous">';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="current">';
$config['cur_tag_close'] = '</li>';

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';


/* End of file pagination.php */
/* Location: ./application/config/pagination.php */