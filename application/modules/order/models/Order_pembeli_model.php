<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_pembeli_model extends CI_Model
{

    public $table = 'order_pembeli';
    public $id = 'id';
    public $order_id = 'order_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get data by order_id
    function get_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('order_id', $q);
        $this->db->or_like('name', $q);
        $this->db->or_like('title', $q);
        $this->db->or_like('nip', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('order_id', $q);
        $this->db->or_like('name', $q);
        $this->db->or_like('title', $q);
        $this->db->or_like('nip', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    function update_by_order_id($order_id, $data)
    {
        $this->db->where($this->order_id, $order_id);
        return $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    // delete data by order_id
    function delete_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->delete($this->table);
    }

}

/* End of file Order_pembeli_model.php */
/* Location: ./application/models/Order_pembeli_model.php */
