<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_detail_model extends CI_Model
{

    public $table = 'order_detail';
    public $id = 'id';
    public $order_id = 'order_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get processed product by order id
    function get_processed($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('is_processed', TRUE);
        return $this->db->get($this->table)->result();
    }

    // get have_so product by order id
    function get_have_so($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('so_number !=', NULL);
        return $this->db->get($this->table)->result();
    }

    // get have_po product by order id
    function get_have_po($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('po_number !=', NULL);
        return $this->db->get($this->table)->result();
    }

    // get have_do product by order id
    function get_have_do($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('do_number !=', NULL);
        return $this->db->get($this->table)->result();
    }

    // get received product by order id
    function get_received($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('is_received', TRUE);
        return $this->db->get($this->table)->result();
    }

    // get data for adminso by grouping order_id
    function get_adminso_detail()
    {
        $this->db->where('sp_number !=', NULL);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get data for purchasing by grouping order_id
    function get_purchasing_detail()
    {
        $this->db->where('so_number !=', NULL);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get data for warehouse by grouping order_id
    function get_warehouse_detail()
    {
        $this->db->where('po_number !=', NULL);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get data for admindo by grouping order_id
    function get_admindo_detail()
    {
        $this->db->where('is_received', TRUE);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get data for delivery by grouping order_id
    function get_delivery_detail()
    {
        $this->db->where('do_number !=', NULL);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get data for admininv by grouping order_id
    function get_admininv_detail()
    {
        $this->db->where('validation_number !=', NULL);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get data for finance by grouping order_id
    function get_finance_detail()
    {
        $this->db->where('inv_number !=', NULL);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->result();
    }

    // get sent product by order id
    function get_sent($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('is_send', TRUE);
        return $this->db->get($this->table)->result();
    }

    // get have_inv product by order id
    function get_have_inv($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('inv_number !=', NULL);
        return $this->db->get($this->table)->result();
    }

    // get delivered by order id
    function get_paid($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->where('is_paid', TRUE);
        return $this->db->get($this->table)->result();
    }

    // get data by order_id
    function get_by_order_id($id)
    {
        $this->db->where($this->order_id, $id);
        return $this->db->get($this->table)->result();
    }

    // get invoice no by order id
    function get_invoice_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->row()->inv_number;
    }

    // get SP by order id
    function get_sp_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->row()->sp_number;
    }

    // get SO by order id
    function get_so_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->row()->so_number;
    }

    // get is_paid by order id
    function get_is_paid_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->row()->is_paid;
    }
    // get paid_date by order id
    function get_paid_date_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        $this->db->group_by('order_id');
        return $this->db->get($this->table)->row()->paid_date;
    }

    // get subtotal by order_id
    function get_subtotal_by_order_id($order_id)
    {
        $this->db->select('subtotal');
        $this->db->from($this->table);
        $this->db->where($this->order_id, $order_id);
        return $this->db->get()->result_array();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('order_id', $q);
        $this->db->or_like('product_name', $q);
        $this->db->or_like('quantity', $q);
        $this->db->or_like('price', $q);
        $this->db->or_like('shipping_cost', $q);
        $this->db->or_like('subtotal', $q);
        $this->db->or_like('description', $q);
        $this->db->or_like('in_stock', $q);
        $this->db->or_like('is_processed', $q);
        $this->db->or_like('is_received', $q);
        $this->db->or_like('is_send', $q);
        $this->db->or_like('is_delivered', $q);
        $this->db->or_like('validation_number', $q);
        $this->db->or_like('updated_date', $q);
        $this->db->or_like('updated_user', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('order_id', $q);
        $this->db->or_like('product_name', $q);
        $this->db->or_like('quantity', $q);
        $this->db->or_like('price', $q);
        $this->db->or_like('shipping_cost', $q);
        $this->db->or_like('subtotal', $q);
        $this->db->or_like('description', $q);
        $this->db->or_like('in_stock', $q);
        $this->db->or_like('is_processed', $q);
        $this->db->or_like('is_received', $q);
        $this->db->or_like('is_send', $q);
        $this->db->or_like('is_delivered', $q);
        $this->db->or_like('validation_number', $q);
        $this->db->or_like('updated_date', $q);
        $this->db->or_like('updated_user', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function bulk_insert($data)
    {
        $this->db->insert_batch($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    function update_by_order_id($data)
    {
        $id = $data['product_id'];
        $order_id = $data['order_id'];

        unset($data['product_id']);
        unset($data['order_id']);

        $this->db->where($this->id, $id);
        $this->db->where($this->order_id, $order_id);
        return $this->db->update($this->table, $data);
    }

    function update_detail_by_order_id($data)
    {
        $order_id = $data['order_id'];

        unset($data['order_id']);

        $this->db->where($this->order_id, $order_id);
        return $this->db->update($this->table, $data);
    }

    function bulk_update($data)
    {
        $this->db->update_batch($this->table, $data, 'id');
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    // delete data by order_id
    function delete_by_order_id($order_id)
    {
        $this->db->where($this->order_id, $order_id);
        return $this->db->delete($this->table);
    }

    // function count_tender_products
    function count_tender_products($order_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->get($this->table)->result();
        return $this->db->affected_rows();
    }

    /**
     * is_filled_all
     *
     * @param $data['order_id'];
     * @param $data['field'];
     * @param $data['condition'];
     * @param $data
     * @return bool
     */
    function is_filled_all($data)
    {
        $total_detail = $this->count_tender_products($data['order_id']);

        $this->db->where('order_id', $data['order_id']);
        $this->db->where($data['field'], $data['condition']);
        $triggered_result = $this->db->get($this->table)->result();

        if ($total_detail == count($triggered_result))
        {
            return true;
        } else {
            return false;
        }
    }

}

/* End of file Order_detail_model.php */
/* Location: ./application/models/Order_detail_model.php */