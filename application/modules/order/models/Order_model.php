<?php

/**
 * Created by PhpStorm.
 * User: dev1410
 * Date: 18/03/16
 * Time: 20:50
 */
class Order_model extends CI_Model
{
    public $table = 'order';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        $this->load->model('Order_detail_model');
    }

    // check order_number_exists

    function order_number_exists($order_number)
    {
        $this->db->where('order_number', $order_number);
        $this->db->get($this->table)->row();

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get order_total_price
    function get_total_price()
    {
        $this->db->select('order_total');
        $this->db->from($this->table);
        $totals = $this->db->get()->result();
        $result = [];

        foreach ($totals as $total)
            array_push($result, $total->order_total);

        return array_sum($result);
    }

    // get sales list
    function get_sales_list()
    {
        $this->db->where('group_id', 10);
        $sales = $this->db->get('users_groups')->result();

        $results = array();

        foreach ($sales as $value)
        {
            $user = $this->ion_auth->user($value->user_id)->row();
            $result = (object) array(
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            );

            array_push($results, $result);
        }

        return $results;
    }

    // get sales
    function get_sales($order_id)
    {
        $this->db->where('order_id', $order_id);
        $order_sales = $this->db->get('order_sales')->row();

        return $this->ion_auth->user($order_sales->sales_id)->row();
    }

    // get helpdesk list
    function get_helpdesk_list()
    {
        $this->db->where('group_id', 2);
        $sales = $this->db->get('users_groups')->result();

        $results = array();

        foreach ($sales as $value)
        {
            $user = $this->ion_auth->user($value->user_id)->row();
            $result = (object) array(
                'id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name
            );

            array_push($results, $result);
        }

        return $results;
    }

    // get helpdesk
    function get_helpdesk($order_id)
    {
        $this->db->where('order_id', $order_id);
        $order_sales = $this->db->get('order_helpdesk')->row();

        return $this->ion_auth->user($order_sales->helpdesk_id)->row();
    }

    // get order_helpdesk
    function get_order_helpdesk($helpdesk_id)
    {
        $this->db->where('helpdesk_id', $helpdesk_id);
        return $this->db->get('order_helpdesk')->result();
    }

    // get order_sales
    function get_order_sales($sales_id)
    {
        $this->db->where('sales_id', $sales_id);
        return $this->db->get('order_sales')->result();
    }

    // get order adminso
    function get_order_adminso()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_adminso_detail();
        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }
        return $results;
    }

    // get order purchasing
    function get_order_purchasing()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_purchasing_detail();

        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }

        return $results;
    }

    // get order warehouse
    function get_order_warehouse()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_warehouse_detail();

        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }

        return $results;
    }

    // get order_admindo
    function get_order_admindo()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_admindo_detail();

        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }

        return $results;
    }

    // get order delivery
    function get_order_delivery()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_delivery_detail();

        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }

        return $results;
    }

    // get order admininv
    function get_order_admininv()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_admininv_detail();

        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }

        return $results;
    }

    // get order finance
    function get_order_finance()
    {
        $results = array();

        $from_detail = $this->Order_detail_model->get_finance_detail();

        if ($this->db->affected_rows() > 0)
        {
            foreach ($from_detail as $item)
            {
                array_push($results, $this->get_by_id($item->order_id));
            }
        }

        return $results;
    }

    // get step
    function get_step($order_id)
    {
        $this->db->where('order_id', $order_id);
        return $this->db->get('order_step')->row();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    /**
     * total_rows
     *
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @return int
     */
    function total_rows($q = NULL, $h = null, $s = NULL, $y = NULL) {
        if ($h != null)
        {
            $this->db->select('order_id');
            $this->db->from('order_helpdesk');
            $this->db->where('helpdesk_id', $h);
            $this->db->group_by('order_id');
            $order_from_helpdesk = $this->db->get()->result();

            $result = array();

            foreach ($order_from_helpdesk as $order)
            {
                if ($s != NULL)
                {
                    switch ($s) {
                        case 1:
                            $this->db->where('is_completed', TRUE);
                            break;
                        case 2:
                            $this->db->where('is_completed', FALSE);
                            $this->db->where('is_cancelled', FALSE);
                            $this->db->where('is_processed', TRUE);
                            break;
                        case 3:
                            $this->db->where('is_cancelled', TRUE);
                    }
                }

                if ($y !== NULL)
                {
                    $this->db->where('YEAR(date_created)', $y);
                }

                $this->db->where('id', $order->order_id);
                $obj = $this->db->get($this->table)->row();
                if ($obj != null)
                {
                    array_push($result, $obj);
                }
            }

            return count($result);
        }
        $this->db->like('id', $q);
        $this->db->or_like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    /**
     * get_limit_data
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return mixed
     */
    function get_limit_data($limit, $start = 0, $q = NULL, $h = NULL, $s = NULL, $y = NULL, $m = NULL, $l = NULL) {

        $this->db->like('id', $q);
        $this->db->limit($limit, $start);
        $results = $this->db->get($this->table)->result();

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_helpdesk
     *
     * NOTE : NOT USED!
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param $helpdesk_id
     * @return array
     */
    function get_limit_data_helpdesk($limit, $start = 0, $q = NULL, $helpdesk_id) {

        $results = array();
        $helpdesk_having_orders = array();

        $order_helpdesk = $this->get_order_helpdesk($helpdesk_id);

        if ($this->db->affected_rows() > 0)
            foreach ($order_helpdesk as $item)
                array_push($helpdesk_having_orders, $item->order_id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $helpdesk_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        return $results;
    }

    /**
     * get_limit_data_sales
     *
     * @param $limit
     * @param int $start
     * @param $sales_id
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_sales(
        $limit, $start = 0, $sales_id, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $sales_having_orders = array();

        $order_sales = $this->get_order_sales($sales_id);

        if ($this->db->affected_rows() > 0)
            foreach ($order_sales as $item)
                array_push($sales_having_orders, $item->order_id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            foreach($query_result as $order)
            {
                if (in_array($order->id, $sales_having_orders))
                    array_push($results, $order);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_adminso
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_adminso(
        $limit, $start = 0, $q = NULL, $h = NULL, $s = NULL,
        $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $adminso_having_orders = array();

        $order_adminso = $this->get_order_adminso();
        if ($this->db->affected_rows() > 0)
            foreach ($order_adminso as $item)
                array_push($adminso_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $adminso_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_purchasing
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_purchasing(
        $limit, $start = 0, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $purchasing_having_orders = array();

        $order_purchasing = $this->get_order_purchasing();

        if ($this->db->affected_rows() > 0)
            foreach ($order_purchasing as $item)
                array_push($purchasing_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $purchasing_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_warehouse
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_warehouse(
        $limit, $start = 0, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $warehouse_having_orders = array();

        $order_warehouse = $this->get_order_warehouse();

        if ($this->db->affected_rows() > 0)
            foreach ($order_warehouse as $item)
                array_push($warehouse_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $warehouse_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_admindo
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_admindo(
        $limit, $start = 0, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $admindo_having_orders = array();

        $order_admindo = $this->get_order_admindo();

        if ($this->db->affected_rows() > 0)
            foreach ($order_admindo as $item)
                array_push($admindo_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $admindo_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_delivery
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_delivery(
        $limit, $start = 0, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $delivery_having_orders = array();

        $order_delivery = $this->get_order_delivery();

        if ($this->db->affected_rows() > 0)
            foreach ($order_delivery as $item)
                array_push($delivery_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $delivery_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_admininv
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_admininv(
        $limit, $start = 0, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $admininv_having_orders = array();

        $order_admininv = $this->get_order_admininv();

        if ($this->db->affected_rows() > 0)
            foreach ($order_admininv as $item)
                array_push($admininv_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $admininv_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_limit_data_finance
     *
     * @param $limit
     * @param int $start
     * @param null $q
     * @param null $h
     * @param null $s
     * @param null $y
     * @param null $m
     * @param null $l
     * @return array
     */
    function get_limit_data_finance(
        $limit, $start = 0, $q = NULL, $h = NULL,
        $s = NULL,  $y = NULL, $m = NULL, $l = NULL) {

        $results = array();
        $finance_having_orders = array();

        $order_finance = $this->get_order_finance();

        if ($this->db->affected_rows() > 0)
            foreach ($order_finance as $item)
                array_push($finance_having_orders, $item->id);
        else
            return $results;

        $this->db->order_by($this->id, $this->order);
        $this->db->like('order_number', $q);
        $this->db->or_like('order_name', $q);

        $this->db->limit($limit, $start);

        $query_result = $this->db->get($this->table)->result();

        if ($this->db->affected_rows() > 0)
        {
            for ($i=0; $i<count($query_result); $i++)
            {
                if (!in_array($query_result[$i]->id, $finance_having_orders))
                    unset($query_result[$i]);
                else
                    array_push($results, $query_result[$i]);
            }
        }

        if ($h != null)
        {
            for ($i=0; $i<count($results); $i++)
            {
                $this->db->select('order_id');
                $this->db->from('order_helpdesk');
                $this->db->where('helpdesk_id', $h);
                $this->db->where('order_id', $results[$i]->id);
                $this->db->get()->row();

                if ($this->db->affected_rows() < 1)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($s != null)
        {
            switch (intval($s)) {
                case 1:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_completed && $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 2:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed || $results[$i]->is_cancelled && !$results[$i]->is_processed)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 3:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if (!$results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
                case 4:
                    for ($i=0; $i<count($results); $i++)
                    {
                        if ($results[$i]->is_completed && $results[$i]->is_processed || $results[$i]->is_cancelled)
                        {
                            unset($results[$i]);
                        }
                    }
                    break;
            }
        }

        if ($y != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%Y', mysql_to_unix($results[$i]->date_created)) != $y)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($m != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if (mdate('%m', mysql_to_unix($results[$i]->date_created)) != $m)
                {
                    unset($results[$i]);
                }
            }
        }

        if ($l != NULL)
        {
            for($i=0; $i<count($results); $i++)
            {
                if ($results[$i]->package_location != $l)
                {
                    unset($results[$i]);
                }
            }
        }

        return $results;
    }

    /**
     * get_available_years
     *
     * @return array
     */
    function get_available_years()
    {
        $available_years = array();

        $this->db->order_by('year(date_created)', 'DESC');
        $this->db->group_by('year(date_created)');
        $available_years_data = $this->db->get($this->table)->result();

        foreach($available_years_data as $year)
        {
            array_push($available_years, mdate('%Y', mysql_to_unix($year->date_created)));
        }
        return $available_years;
    }

    function get_available_months()
    {
        $available_years = array();

        $this->db->order_by('month(date_created)', 'DESC');
        $this->db->group_by('month(date_created)');
        $available_years_data = $this->db->get($this->table)->result();

        foreach($available_years_data as $year)
        {
            array_push($available_years, mdate('%m', mysql_to_unix($year->date_created)));
        }
        return $available_years;
    }

    /**
     * get_total_available_years
     *
     * @return array
     */
    function get_total_available_years()
    {
        $available_years = array();

        $this->db->order_by('year(date_created)', 'DESC');
        $this->db->group_by('year(date_created)');
        $available_years_data = $this->db->get($this->table)->result();

        foreach ($available_years_data as $years_data)
        {
            array_push(
                $available_years,
                date('Y', mysql_to_unix($years_data->date_created)));
        }

        return $available_years;
    }

    function get_total_yearly($year)
    {
        $results = array();

        $available_month = array();

        $this->db->where('year(date_created)', $year);

        $this->db->order_by('month(date_created)', 'ASC');

        $this->db->group_by('month(date_created)');

        $get_month_data = $this->db->get($this->table)->result();

        foreach ($get_month_data as $months)
        {
            $this->db->select('sum(order_total) AS total');
            $this->db->where('is_completed', TRUE);
            $this->db->where('month(date_created)', date('m', mysql_to_unix($months->date_created)));
            $this->db->from('order');
            $completed = $this->db->get()->row();

            $this->db->select('sum(order_total) AS total');
            $this->db->where('is_cancelled', TRUE);
            $this->db->where('month(date_created)', date('m', mysql_to_unix($months->date_created)));
            $this->db->from('order');
            $cancelled = $this->db->get()->row();

            $this->db->select('sum(order_total) AS total');
            $this->db->where('is_cancelled', NULL);
            $this->db->where('is_completed', NULL);
            $this->db->where('month(date_created)', date('m', mysql_to_unix($months->date_created)));
            $this->db->from('order');
            $onprocess = $this->db->get()->row();

            $query_result = array(
                'label' => date('M', mysql_to_unix($months->date_created)),
                'data' => [
                    $completed->total == null ? 0 : $completed->total,
                    $onprocess->total == null ? 0 : $onprocess->total,
                    $cancelled->total == null ? 0 : $cancelled->total
                ]);

            array_push($results, $query_result);
        }

        return $results;
    }

    // get total order
    function get_total($reference)
    {
        switch ($reference)
        {
            case 'yearly':
                $this->db->where('year(date_created)', date('Y'));
                break;
            case 'monthly':
                $this->db->where('month(date_created)', date('m'));
                break;
            case 'daily':
                $this->db->where('day(date_created)', date('d'));
        }

        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // insert order_helpdesk
    function insert_helpdesk($data)
    {
        $this->db->insert('order_helpdesk', $data);
    }

    // insert order_sales
    function insert_sales($data)
    {
        $this->db->insert('order_sales', $data);
    }

    // insert order_step
    function insert_step($data)
    {
        $this->db->insert('order_step', $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $data);
    }

    // update order_helpdesk
    function update_helpdesk($data)
    {
        $order_id = $data['order_id'];
        unset($data['order_id']);

        return $this->db->update('order_helpdesk', $data, array('order_id' =>$order_id));
    }

    // update order_sales
    function update_sales($data)
    {
        $order_id = $data['order_id'];
        unset($data['order_id']);
        $this->db->update('order_sales', $data, array('order_id' => $order_id));
    }

    // update order_step
    function update_step($data)
    {
        $this->db->update(
            'order_step',
            $data,
            array('order_id' => $data['order_id']));
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    // cancel order
    function cancel($id)
    {
        return $this->db->update(
            'order',
            array('is_cancelled' => TRUE),
            array('id' => $id));
    }

    // insert or update order remark
    function change_remark($data)
    {
        array_key_exists('id', $data) ? $id = $data['id'] : '';

        if(isset($id))
        {
            unset($data['id']);
        }

        isset($id) ? $this->db->where($this->id, $id) : '';

        return isset($id) ?
            $this->db->update('order_remark', $data, array('id' => $id)) :
            $this->db->insert('order_remark', $data);
    }

    // get order remarks order by date DESC
    function get_remarks($order_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->order_by('created_date', 'desc');
        $remarks = $this->db->get('order_remark')->result();

        foreach($remarks as $remark)
        {
            $remark->created_user = $this->ion_auth->user($remark->created_user)->row();
            $remark->created_user->group = $this->ion_auth->get_users_groups($remark->created_user->id)->row();
        }
        return $remarks;
    }

    // delete remark by id or order_id
    function delete_remark($data)
    {
        array_key_exists('id', $data) ? $id = $data['id'] : $order_id = $data['order_id'];
        return isset($id) ?
            $this->db->delete('order_remark', array($this->id => $id)) :
            $this->db->delete('order_remark', array('order_id' => $order_id));
    }

}