<?php if (!defined('BASEPATH')) exit('No direct script access allowed!');

class Migration_Install_order extends CI_Migration
{
    public function up()
    {
        // Drop table `order_pemesan` if exists
        $this->dbforge->drop_table('order_pemesan', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'unique' => TRUE
            ),
            'phone' => array(
                'type' => 'INT',
                'constraint' => '13',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_pemesan');


        // Drop table `order_pembeli` if exists
        $this->dbforge->drop_table('order_pembeli', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'unique' => TRUE
            ),
            'nip' => array(
                'type' => 'INT',
                'constraint' => '30',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_pembeli');


        // Drop table `order_status` if exists
        $this->dbforge->drop_table('order_status', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'status' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'position' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'notification_by' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'updated_date' => array(
                'type' => 'TIMESTAMP',
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_status');


        // Drop table `order_negotiation` if exists
        $this->dbforge->drop_table('order_negotiation', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'buyer' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'buyer_date' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            ),
            'supplier' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
            ),
            'supplier_date' => array(
                'type' => 'DATETIME',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_negotiation');


        // Drop table `order_detail` if exists
        $this->dbforge->drop_table('order_detail', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unique' => TRUE,
            ),
            'product_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'quantity' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
            ),
            'price' => array(
                'type' => 'INT',
                'constraint' => '15',
            ),
            'shipping_cost' => array(
                'type' => 'INT',
                'constraint' => '15',
            ),
            'subtotal' => array(
                'type' => 'INT',
                'constraint' => '15',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
                'null' => TRUE
            ),
            'in_stock' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_processed' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_received' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_send' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_delivered' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'validation_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ),
            'updated_date' => array(
                'type' => 'TIMESTAMP',
            ),
            'updated_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order_detail');


        // Drop table `order` if exists
        $this->dbforge->drop_table('order', TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'order_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'unique' => TRUE
            ),
            'order_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'instance' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'package_location' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ),
            'work_unit' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => TRUE
            ),
            'work_unit_address' => array(
                'type' => 'VARCHAR',
                'constraint' => '256',
                'null' => TRUE
            ),
            'date_created' => array(
                'type' => 'DATE',
            ),
            'date_updated' => array(
                'type' => 'DATE',
            ),
            'order_quantity' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '5',
            ),
            'order_total' => array(
                'type' => 'INT',
                'constraint' => '13',
            ),
            'description' => array(
                'type' => 'VARCHAR',
                'constraint' => '256',
                'null' => TRUE
            ),
            'is_processed' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_completed' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'is_cancelled' => array(
                'type' => 'BOOLEAN',
                'null' => TRUE
            ),
            'updated_date' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE
            ),
            'created_date' => array(
                'type' => 'TIMESTAMP',
            ),
            'created_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'updated_user' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'null' => TRUE
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('order');

    }

    public function down()
    {
        $this->dbforge->drop_table('order_pemesan', TRUE);
        $this->dbforge->drop_table('order_pembeli', TRUE);
        $this->dbforge->drop_table('order_status', TRUE);
        $this->dbforge->drop_table('order_negotiation', TRUE);
        $this->dbforge->drop_table('order_detail', TRUE);
        $this->dbforge->drop_table('order', TRUE);
    }
}