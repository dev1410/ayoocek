<div class="row">
    <h2>Memperbarui status produk tender</h2>
    <hr/>

    <form action="<?php echo $action; ?>" method="post">

        <div class="row">
            <div class="columns text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>

        <div class="row columns">
            <fieldset class="fieldset">

                <legend>Detail Produk</legend>

                <div class="small-4 column">
                    <label for="product_name">Nama Produk
                        <?php echo form_error('product_name') ?>
                        <input type="text" name="product_name" id="product_name"
                               value="<?=$product_name; ?>" disabled/>
                    </label>
                </div>

                <div class="small-2 column">
                    <label for="product_quantity">Kuantitas <?php echo form_error('product_quantity') ?>
                        <input type="number" name="product_quantity" id="product_quantity"
                               value="<?=$quantity; ?>" disabled/>
                    </label>
                </div>

                <div class="small-2 column">
                    <label for="product_price">Harga Satuan <?php echo form_error('price') ?>
                        <input type="number" name="product_price" id="product_price"
                               value="<?=$price; ?>" disabled/>
                    </label>
                </div>

                <div class="small-2 column">
                    <label for="product_shipping_cost">Ongkos Kirim
                        <?php echo form_error('product_shipping_cost') ?>
                        <input type="number" name="product_shipping_cost" id="product_shipping_cost"
                               value="<?=$shipping_cost; ?>" disabled/>
                    </label>
                </div>

                <div class="small-2 column">
                    <label for="product_subtotal">Subtotal
                        <?php echo form_error('product_subtotal') ?>
                        <input type="number" name="product_subtotal" id="product_subtotal"
                               value="<?=$subtotal; ?>" disabled/>
                    </label>
                </div>

                <div class="column">
                    <label for="product_description">Catatan Tambahan
                        <?php echo form_error('product_description') ?></label>
                    <input type="text" name="product_description" id="product_description"
                           placeholder="Description"
                           value="<?=$description; ?>" disabled/>
                </div>

                <fieldset class="fieldset">
                    <legend>Status</legend>

                    <div class="row columns">

                        <?php if ($has_add_proses) { ?>
                        <div class="columns">
                            <label for="product_is_processed">
                                <?=$is_processed ? 'Telah diproses' : 'Proses tender ini'; ?>
                                <?=form_error('product_is_processed'); ?>
                                <select name="product_is_processed"
                                    <?=$is_processed != null && !$has_change_proses ? 'disabled' : ''; ?>>
                                    <option value="1"
                                        <?=$is_processed ? 'selected="selected"' : '';?>>
                                        Iya
                                    </option>
                                    <option value="0"
                                        <?=$is_processed == false or $is_processed != null ? 'selected="selected"' : '';?>>
                                        Tidak
                                    </option>
                                </select>
                            </label>
                        </div>
                        <?php } ?>

                        <!-------------   IF PROCESSED ---------------------->
                        <?php if ($is_processed == TRUE) { ?>

                            <!----------- IF SP Number ------------------->
                            <?php if ($sp_number != null) { ?>

                            <!------------ IF SO Number ------------------>
                            <?php if ($so_number != NULL) { ?>

                                <?php if ($po_number != null) { ?>

                                    <?php if ($has_add_received) { ?>
                                    <div class="columns">
                                        <label for="product_is_received">Diterima gudang
                                            <?=form_error('product_is_received'); ?>
                                            <select name="product_is_received"
                                                <?=$is_received != null && !$has_change_received ? 'disabled' : ''; ?>>
                                                <option value="1"
                                                    <?=$is_received ? 'selected="selected"' : ''; ?>>
                                                    Sudah
                                                </option>
                                                <option value="0"
                                                    <?=$is_received == false ? 'selected="selected"' : ''; ?>>
                                                    Belum
                                                </option>
                                            </select>
                                        </label>
                                    </div>
                                    <?php } ?>

                                    <?php if ($is_received == TRUE) { ?>

                                        <?php if ($do_number != '') { ?>

                                        <?php if ($has_add_send) { ?>
                                        <div class="columns">
                                            <label for="product_is_send"> Sudah dikirim ke pembeli ?
                                                <?=form_error('product_is_send'); ?>
                                                <select name="product_is_send"
                                                    <?=$is_send != null && !$has_change_send ? 'disabled' : ''; ?>>
                                                    <option value="1"
                                                        <?=$is_send ? 'selected="selected"' : ''; ?>>
                                                        Sudah
                                                    </option>
                                                    <option value="0"
                                                        <?=$is_send == false ? 'selected="selected"' : ''; ?>>
                                                        Belum
                                                    </option>
                                                </select>

                                                <input type="hidden" name="validation_number"
                                                       id="validation_number" placeholder="Nomor Pengiriman"
                                                       value="<?php echo $validation_number; ?>"
                                                    <?php echo $validation_number != null && !$has_change_send ? 'disabled':''; ?> />
                                            </label>
                                        </div>
                                        <?php } ?>

                                        <?php if ($inv_number != NULL) { ?>

                                            <?php if ($has_add_paid) { ?>
                                            <div class="columns">
                                                <label for="product_is_paid">Sudah dibayar oleh pembeli ?
                                                    <?=form_error('product_is_paid'); ?>
                                                    <select name="product_is_paid"
                                                        <?=$is_paid != null && !$has_change_paid ? 'disabled' : ''; ?>>
                                                        <option value="1"
                                                            <?=$is_paid ? 'selected="selected"' : ''; ?>>
                                                            Sudah
                                                        </option>

                                                        <option value="0"
                                                            <?=$is_paid == false ? 'selected="selected"' : ''; ?>>
                                                            Belum
                                                        </option>
                                                    </select>
                                                </label>
                                            </div>
                                            <?php } ?>

                                        <?php } ?>

                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <!------------ ENDIF SO Number ------------------>
                            <?php } ?>
                            <!------------ ENDIF SP Number ----------------->

                        <?php } ?>
                        <!-------------   ENDIF PROCESSED ---------------------->

                    </div>

                </fieldset>

                    <fieldset class="fieldset">

                        <div class="row column">

                                <?php if ($has_add_sp_number) { ?>
                                <div class="medium-4 column">
                                    <label for="product_sp_number"> No. SP
                                        <?php echo form_error('product_sp_number'); ?>
                                        <input type="text" name="product_sp_number" id="product_sp_number"
                                               placeholder="Mohon isikan No. SP"
                                               value="<?=$sp_number;?>"
                                            <?=$sp_number != '' && !$has_change_sp_number ? 'disabled' : ''; ?>/>
                                    </label>
                                </div>
                                <?php } ?>

                            <?php if ($sp_number != NULL) { ?>

                                <?php if ($has_add_so_number) { ?>
                                <div class="medium-4 column">
                                    <label for="product_so_number"> No. SO
                                        <?php echo form_error('product_so_number'); ?>
                                        <input type="text" name="product_so_number" id="product_so_number"
                                               placeholder="Mohon isikan No. SO"
                                               value="<?=$so_number;?>"
                                            <?=$so_number != '' && !$has_change_so_number ? 'disabled' : ''; ?>/>
                                    </label>
                                </div>
                                <?php } ?>

                            <?php if ($so_number != NULL) { ?>

                                <?php if ($has_add_po_number) { ?>
                                <div class="medium-4 column">
                                    <label for="product_po_number"> No. PO
                                        <?=form_error('product_po_number'); ?>
                                        <input type="text" name="product_po_number" id="product_po_number"
                                               placeholder="Mohon isikan No. PO"
                                               value="<?=$po_number;?>"
                                            <?=$po_number != '' && !$has_change_po_number ? 'disabled' : ''; ?>/>
                                    </label>
                                </div>
                                <?php } ?>

                            <?php } ?>
                            <?php } ?>

                        </div>

                        <?php if ($is_received == TRUE) { ?>

                        <div class="row column">

                            <?php if ($has_add_do_number) { ?>
                            <div class="medium-6 column">
                                <label for="product_do_number"> No. DO
                                    <?=form_error('product_do_number'); ?>
                                    <input type="text" name="product_do_number" id="product_do_number"
                                           placeholder="Mohon isikan No. DO"
                                           value="<?=$do_number;?>"
                                        <?=$do_number != '' && !$has_change_do_number ? 'disabled' : ''; ?>/>
                                </label>
                            </div>
                            <?php } ?>

                            <?php if ($is_send == TRUE) { ?>

                            <?php if ($has_add_inv_number) { ?>
                            <div class="medium-6 column">
                                <label for="product_inv_number"> No. Invoice
                                    <?=form_error('product_inv_number'); ?>
                                    <input type="text" name="product_inv_number" id="product_inv_number"
                                           placeholder="Mohon isikan No. Invoice"
                                           value="<?=$inv_number;?>"
                                        <?=$inv_number != '' && !$has_change_inv_number ? 'disabled' : ''; ?>/>
                                </label>
                            </div>
                            <?php } ?>

                            <?php } ?>

                        </div>

                        <?php } ?>

                    </fieldset>

                <input type="hidden" name="order_id" value="<?=$order_id; ?>" id="order_id" />

            </fieldset>
        </div>
        <hr />

        <div class="column">
            <div class="small-8 right">
                <button type="submit" class="button right">Perbarui</button>
                <a href="<?php echo site_url('order/update/'.$order_id) ?>" class="secondary button">Batal</a>
            </div>
        </div>
        <hr>

        <input type="hidden" name="id" value="<?php echo $id; ?>" />


    </form>
</div>

<script type="text/javascript">
    var base_url = "<?=base_url(); ?>";
</script>