<div class="row">

    <hr/>

    <div class="row columns">

        <div class="small-6 medium-8 large-8 columns">
            <h2 id="title"></h2>
        </div>

        <div class="small-6 medium-4 large-4 columns">

            <div class="row">
                <div class="small-2 medium-7 large-7 column"></div>
                <div class="small-10 medium-5 large-5 column">
                    <select name="year">
                        <?php foreach ($years as $year_item): ?>
                            <option value="<?=$year_item?>" <?=$year_item == $year_item ? 'selected' : '' ?>><?=$year_item?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

        </div>

    </div>

    <hr/>

    <div class="small-12 medium-12 large-12 columns">
        <canvas id="canvas-line"></canvas>
    </div>

    <div class="row columns">
        <div class="small-12 medium-4 large-4 column text-center">
            <h4 id="completed_order_text" style="color: rgb(0,153,51);"></h4>
            <hr/>
        </div>

        <div class="small-12 medium-4 large-4 column text-center">
            <h4 id="total_order_text"></h4>
            <hr/>
        </div>

        <div class="small-12 medium-4 large-4 column text-center">
            <h4 id="cancelled_order_text" style="color: rgb(255,0,0);"></h4>
            <hr/>
        </div>
    </div>

    <hr/>

    <input type="hidden" name="length" value="0"/>
</div>
<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
</script>