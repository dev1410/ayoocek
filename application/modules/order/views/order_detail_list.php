<!doctype html>
<html>
<head>
    <title>harviacode.com - codeigniter crud generator</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <style>
        body{
            padding: 15px;
        }
    </style>
</head>
<body>
<h2 style="margin-top:0px">Order_detail List</h2>
<div class="row" style="margin-bottom: 10px">
    <div class="col-md-4">
        <?php echo anchor(site_url('order_detail/create'),'Create', 'class="btn btn-primary"'); ?>
    </div>
    <div class="col-md-4 text-center">
        <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>
    <div class="col-md-1 text-right">
    </div>
    <div class="col-md-3 text-right">
        <form action="<?php echo site_url('order_detail/index'); ?>" class="form-inline" method="get">
            <div class="input-group">
                <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php
                            if ($q <> '')
                            {
                                ?>
                                <a href="<?php echo site_url('order_detail'); ?>" class="btn btn-default">Reset</a>
                                <?php
                            }
                            ?>
                            <button class="btn btn-primary" type="submit">Search</button>
                        </span>
            </div>
        </form>
    </div>
</div>
<table class="table table-bordered" style="margin-bottom: 10px">
    <tr>
        <th>No</th>
        <th>Order Id</th>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Shipping Cost</th>
        <th>Subtotal</th>
        <th>Description</th>
        <th>In Stock</th>
        <th>Is Processed</th>
        <th>Is Received</th>
        <th>Is Send</th>
        <th>Is Delivered</th>
        <th>Validation Number</th>
        <th>Updated Date</th>
        <th>Updated User</th>
        <th>Action</th>
    </tr><?php
    foreach ($order_detail_data as $order_detail)
    {
        ?>
        <tr>
            <td width="80px"><?php echo ++$start ?></td>
            <td><?php echo $order_detail->order_id ?></td>
            <td><?php echo $order_detail->product_name ?></td>
            <td><?php echo $order_detail->quantity ?></td>
            <td><?php echo $order_detail->price ?></td>
            <td><?php echo $order_detail->shipping_cost ?></td>
            <td><?php echo $order_detail->subtotal ?></td>
            <td><?php echo $order_detail->description ?></td>
            <td><?php echo $order_detail->in_stock ?></td>
            <td><?php echo $order_detail->is_processed ?></td>
            <td><?php echo $order_detail->is_received ?></td>
            <td><?php echo $order_detail->is_send ?></td>
            <td><?php echo $order_detail->is_delivered ?></td>
            <td><?php echo $order_detail->validation_number ?></td>
            <td><?php echo $order_detail->updated_date ?></td>
            <td><?php echo $order_detail->updated_user ?></td>
            <td style="text-align:center" width="200px">
                <?php
                echo anchor(site_url('order_detail/read/'.$order_detail->id),'Read');
                echo ' | ';
                echo anchor(site_url('order_detail/update/'.$order_detail->id),'Update');
                echo ' | ';
                echo anchor(site_url('order_detail/delete/'.$order_detail->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                ?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<div class="row">
    <div class="col-md-6">
        <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
    </div>
    <div class="col-md-6 text-right">
        <?php echo $pagination ?>
    </div>
</div>
</body>
</html>