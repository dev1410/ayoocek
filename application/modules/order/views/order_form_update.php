<div class="row">

    <h3 style="margin-top:0px">Memperbarui Status Tender No. <?=$order_number; ?> (<?=$order_name;?>)</h3>


    <form action="<?php echo $action; ?>" method="post">



        <div class="row columns">

            <div class="small-12 medium-6 large-6 columns">
                <h4>Informasi Umum</h4>
            </div>
            <div class="small-12 medium-6 large-6 columns">
                <div class="small-6 medium-9 large-9 column">
                    <label for="order_helpdesk">Helpdesk
                        <?php echo form_error('order_helpdesk') ?>
                        <select name="order_helpdesk"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>>
                            <?php foreach ($order_helpdesk as $helpdesk) { ?>
                                <option value="<?php echo $helpdesk->id; ?>"
                                    <?php echo $helpdesk->id == $helpdesk_selected->id ? 'selected':'';?>>
                                    <?php echo ucwords($helpdesk->first_name . " " . $helpdesk->last_name); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </label>
                </div>

                <div class="small-6 medium-3 large-3 column">
                    <label for="order_sales">Sales
                        <?php echo form_error('order_sales') ?>
                        <select name="order_sales"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>>
                            <?php foreach ($order_sales as $sales) { ?>
                                <option value="<?php echo $sales->id; ?>"
                                    <?php echo $sales->id == $sales_selected->id ? 'selected':'';?>>
                                    <?php echo ucwords($sales->first_name . " " . $sales->last_name); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </label>
                </div>
            </div>
            <hr>

            <div class="row">

                <div class="small-12 medium-1 large-1 column">
                    <label for="order_number">No.Tender
                        <?php echo form_error('order_number') ?>
                        <input type="number" name="order_number" id="order_number"
                               value="<?php echo $order_number; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div class="small-12 medium-2 large-2 column">
                    <label for="order_name">ID Paket
                        <?php echo form_error('order_name') ?>
                        <input type="text" name="order_name" id="order_name"
                               value="<?php echo $order_name; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div class="small-12 medium-2 large-2 column">
                    <label for="instance">Instansi
                        <?php echo form_error('instance') ?>
                        <input type="text" name="instance" id="instance" placeholder="Instansi"
                               value="<?php echo $instance; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div class="small-12 medium-2 large-2 column">
                    <label for="package_location">Lokasi Paket
                        <?php echo form_error('package_location') ?>
                        <input type="text" name="package_location" id="package_location" placeholder="Lokasi Paket"
                               value="<?php echo $package_location; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div class="small-12 medium-1 large-1 column">
                    <label for="work_unit">Satuan Kerja
                        <?php echo form_error('work_unit') ?>
                        <input type="text" name="work_unit" id="work_unit" placeholder="Work Unit"
                               value="<?php echo $work_unit; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div class="small-12 medium-2 large-2 column">
                    <label for="work_unit_address">Alamat Satuan Kerja
                        <?php echo form_error('work_unit_address') ?>
                        <textarea name="work_unit_address" id="work_unit_address"
                                  placeholder="Alamat Satuan Kerja"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>><?=$work_unit_address;?></textarea>
                    </label>
                </div>

                <div class="small-12 medium-1 large-1 column">
                    <label for="date_created">Tanggal Buat
                        <?php echo form_error('date_created') ?>
                        <input type="text" name="date_created" id="date_created" placeholder="Tanggal Buat"
                               value="<?php echo $date_created; ?>" class="fdatepicker"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div class="small-12 medium-1 large-1 column">
                    <label for="order_quantity">Kuantitas
                        <?php echo form_error('order_quantity') ?>
                        <input type="number" name="order_quantity" id="order_quantity"
                               value="<?php echo $order_quantity; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

            </div>
        </div>

        <hr>

        <div class="row columns">

            <div class="small-12 medium-6 columns">

                <div class="small-12 medium-6 columns">
                    <h4> (PP/Pemesan)</h4>
                </div>
                <hr>

                <div>
                    <label for="pemesan_name">Nama
                        <?php echo form_error('pemesan_name') ?>
                        <input type="text" name="pemesan_name" id="pemesan_name" placeholder="Nama"
                               value="<?php if (isset($pemesan_name)) echo $pemesan_name; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div>
                    <label for="pemesan_email">Email
                        <?php echo form_error('pemesan_email') ?>
                        <input type="email" name="pemesan_email" id="pemesan_email" placeholder="Email"
                               value="<?php if (isset($pemesan_email)) echo $pemesan_email; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div>
                    <label for="pemesan_phone">No. Telp
                        <?php echo form_error('pemesan_phone') ?>
                        <input type="text" name="pemesan_phone" id="pemesan_phone"
                               placeholder="Phone" value="<?php if (isset($pemesan_phone)) echo $pemesan_phone; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <input type="hidden" name="pemesan_id" id="pemesan_id"
                       value="<?php if (isset($pemesan_id)) echo $pemesan_id; ?>"
                    <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>

            </div>

            <div class="small-12 medium-6 columns">
                <div class="small-12 medium-6 columns">
                    <h4>(PPK/Pembeli)</h4>
                </div>
                <hr>

                <div>
                    <label for="pembeli_name">Nama
                        <?php echo form_error('pembeli_name') ?>
                        <input type="text" name="pembeli_name" id="pembeli_name" placeholder="Name"
                               value="<?php if (isset($pembeli_name)) echo $pembeli_name; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div>
                    <label for="pembeli_title">Jabatan
                        <?php echo form_error('pembeli_title') ?>
                        <input type="text" class="form-control" name="pembeli_title" id="pembeli_title" placeholder="Title"
                               value="<?php if (isset($pembeli_title)) echo $pembeli_title; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <div>
                    <label for="pembeli_nip">NIP
                        <?php echo form_error('pembeli_nip') ?>
                        <input type="number" name="pembeli_nip" id="pembeli_nip" placeholder="Nip"
                               value="<?php if (isset($pembeli_nip)) echo $pembeli_nip; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </label>
                </div>

                <input type="hidden" name="pembeli_id" id="pembeli_id"
                       value="<?php if (isset($pembeli_id)) echo $pembeli_id; ?>"
                    <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>

            </div>

        </div>

        <hr>

        <div class="row columns">

            <div class="small-12 medium-8 columns">
                <h4>Notifikasi oleh PPK/Pembeli</h4>
            </div>
            <hr>

            <div class="small-12 medium-6 large-12 columns">
                <div class="form-group">
                    <label for="description">Pesan <?php echo form_error('description') ?>
                        <textarea name="description" id="description"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>><?=$description; ?></textarea>
                    </label>
                </div>
            </div>

        </div>

        <hr />

        <div class="row columns">
            <div class="small-12 medium-8 columns">
                <h4>Produk Tender</h4>
            </div>
            <hr>

            <div class="row columns" id="order_products">

                <?php $count = 0; ?>

                <?php foreach ($products as $product) { ?>

                    <?php $count +=1; ?>

                <fieldset class="fieldset" id="fieldset1">
                    <legend>Produk <?php echo $count > 0 ? $count : ''; ?></legend>

                    <div class="small-4 column">
                        <label for="product_name">Nama Produk
                            <?php echo form_error('product_name') ?>
                            <input type="text" name="product_name[]" id="product_name"
                                   value="<?=$product->product_name; ?>"
                                <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_quantity">Kuantitas <?php echo form_error('product_quantity') ?>
                            <input type="number" name="product_quantity[]" id="product_quantity"
                                   value="<?=$product->quantity; ?>"
                                <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_price">Harga Satuan <?php echo form_error('price') ?>
                            <input type="number" name="product_price[]" id="product_price"
                                   value="<?=$product->price; ?>"
                                <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_shipping_cost">Ongkos Kirim
                            <?php echo form_error('product_shipping_cost') ?>
                            <input type="number" name="product_shipping_cost[]" id="product_shipping_cost"
                                   value="<?=$product->shipping_cost; ?>"
                                <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_subtotal">Subtotal
                            <?php echo form_error('product_subtotal') ?>
                            <input type="number" name="product_subtotal[]" id="product_subtotal"
                                   value="<?=$product->subtotal; ?>"
                                <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                        </label>
                    </div>

                    <div class="column">
                        <label for="product_description">Catatan Tambahan
                            <?php echo form_error('product_description') ?></label>
                        <input type="text" name="product_description[]" id="product_description"
                               placeholder="Description"
                               value="<?=$product->description; ?>"
                            <?php echo !$has_change_tender || $is_completed || $is_cancelled ? 'disabled':''; ?>/>
                    </div>

                    <fieldset class="fieldset">
                        <legend>Status</legend>

                        <div class="row columns">

                            <div class="columns">
                                <label for="product_is_processed">
                                    <ul>
                                        <li>
                                            <?php
                                                echo $product->is_processed ?
                                                    'Tender telah diproses. No. SP : ' . $product->sp_number :
                                                    'Tender belum diproses'
                                            ?>
                                        </li>
                                    </ul>
                                </label>
                            </div>

                            <!-------------   IF PROCESSED ---------------------->
                            <?php if ($product->is_processed == TRUE) { ?>

                                <div class="columns">

                                    <label for="product_so_number">

                                        <ul>
                                            <li>
                                                <?php
                                                    if ($product->so_number != null)
                                                    {
                                                        echo "No. SO telah ditambahkan : " . $product->so_number;
                                                    } else {
                                                        echo "No. SO belum ditambahkan.";
                                                    }
                                                ?>
                                            </li>
                                        </ul>

                                    </label>

                                </div>

                                <!------------ IF SO NUMBER ------------------>
                                <?php if ($product->so_number != NULL) { ?>

                                    <div class="columns">

                                        <label for="product_po_number">

                                            <ul>
                                                <li>
                                                    <?php
                                                    if ($product->po_number != null)
                                                    {
                                                        echo "No. PO telah ditambahkan : " . $product->po_number;
                                                    } else {
                                                        echo "No. PO belum ditambahkan.";
                                                    }
                                                    ?>
                                                </li>
                                            </ul>

                                        </label>

                                    </div>

                                    <?php if ($product->po_number != null) { ?>

                                        <div class="columns">

                                            <label for="product_is_received">

                                                <ul>
                                                    <li>
                                                        <?php
                                                        if ($product->is_received == null or $product->is_received == false)
                                                        {
                                                            echo "Produk belum diterima di gudang. ";
                                                        } else {
                                                            echo "Produk sudah diterima gudang.";
                                                        }
                                                        ?>
                                                    </li>
                                                </ul>

                                            </label>

                                        </div>

                                        <?php if ($product->is_received == TRUE) { ?>

                                            <div class="columns">

                                                <label for="product_do_number">

                                                    <ul>
                                                        <li>
                                                            <?php
                                                            if ($product->do_number == null)
                                                            {
                                                                echo "Produk belum memiliki No. DO. ";
                                                            } else {
                                                                echo "Produk sudah memiliki No DO : " .
                                                                    $product->do_number;
                                                            }
                                                            ?>
                                                        </li>
                                                    </ul>

                                                </label>

                                            </div>

                                            <?php if ($product->do_number != '') { ?>

                                            <div class="columns">

                                                <label for="product_is_send">

                                                    <ul>
                                                        <li>
                                                            <?php
                                                            if ($product->is_send == null)
                                                            {
                                                                echo "Produk belum dikirim ke pembeli. ";
                                                            } else {
                                                                echo "Produk sudah dikirim ke pembeli : " .
                                                                    $product->validation_number;
                                                            }
                                                            ?>
                                                        </li>
                                                    </ul>

                                                </label>

                                            </div>


                                            <?php if ($product->is_send == TRUE) { ?>

                                                <div class="columns">

                                                    <label for="product_is_paid">

                                                        <ul>
                                                            <li>
                                                                <?php
                                                                if ($product->inv_number == NULL)
                                                                {
                                                                    echo "Produk belum memiliki No. Invoice. ";
                                                                } else {
                                                                    echo "Produk sudah memiliki No. Invoice : $product->inv_number";
                                                                }
                                                                ?>
                                                            </li>
                                                        </ul>

                                                    </label>

                                                </div>

                                                <?php if ($product->inv_number != NULL) { ?>

                                                <div class="columns">

                                                    <label for="product_is_paid">

                                                        <ul>
                                                            <li>
                                                                <?php
                                                                if ($product->is_paid == null)
                                                                {
                                                                    echo "Produk belum dibayar oleh pembeli. ";
                                                                } else {
                                                                    echo "Produk sudah dibayar oleh pembeli";
                                                                }
                                                                ?>
                                                            </li>
                                                        </ul>

                                                    </label>

                                                </div>

                                                <?php } ?>

                                            <?php } ?>
                                        <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <!------------ ENDIF SO NUMBER ------------------>

                            <?php } ?>
                            <!-------------   ENDIF PROCESSED ---------------------->

                        </div>

                    </fieldset>

                    <input type="hidden" name="product_id[]" value="<?=$product->id; ?>"
                           id="product_id-<?=$count;?>" />

                    <div class="column">
                        <div class="small-8 right">
                            <a href="<?php echo site_url('order/detail/'.$product->id) ?>" class="secondary button">Edit status</a>
                        </div>
                    </div>

                </fieldset>

                <?php } ?>

            </div>

        </div>

        <hr />

        <div class="column">

            <div class="small-6 columns">

                <?php if ($has_change_tender && !$is_completed && !$is_cancelled) { ?>
                <button type="submit" class="button right"><?php echo $button ?></button>
                <?php } ?>

                <a href="<?php echo site_url('order') ?>" class="secondary button">Cancel</a>

                <?php if ($has_print_precall) { ?>
                <a href="<?php echo site_url('order/precall/'. $id) ?>" class="hollow secondary button" target="_blank">
                    <i class="fi-print"></i> Print Precall
                </a>
                <?php } ?>
            </div>

            <div class="small-3 columns">
                <label for="order_total" class="text-right"><b>Total Harga</b></label>
            </div>

            <div class="small-3 columns">
                <?php echo form_error('order_total') ?>
                <input type="number" name="order_total" id="order_total" placeholder="Total Harga"
                       value="<?php echo $order_total; ?>"
                    <?php echo !$has_change_tender ? 'disabled':''; ?>/>
            </div>

        </div>
        <hr>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
    </form>
</div>

<script type="text/javascript">
    var base_url = "<?=base_url(); ?>";
</script>