<?php
$datestring = " %d-%m-%Y : %h:%i %a";
?>

<div class="cd-container">

    <fieldset class="fieldset">

        <legend>Informasi Tender</legend>

        <div class="row columns">

            <div class="medium-6 column">
                <label for="no_tender"> No Tender
                    <input type="number" value="<?php echo $order_number; ?>" disabled/>
                </label>

                <label for="no_tender"> ID Paket
                    <input type="text" value="<?php echo $order_name; ?>" disabled/>
                </label>

                <label for="no_tender"> Instansi
                    <input type="text" value="<?php echo $instance; ?>" disabled/>
                </label>

                <label for="no_tender"> Lokasi Paket
                    <input type="text" value="<?php echo $package_location; ?>" disabled/>
                </label>

                <label for="no_tender"> Satuan Kerja
                    <input type="text" value="<?php echo $work_unit; ?>" disabled/>
                </label>

                <label for="no_tender"> Satuan Kerja
                    <input type="text" value="<?php echo $work_unit; ?>" disabled/>
                </label>

                <label for="no_tender"> Alamat Satuan Kerja
                    <textarea disabled><?php echo $work_unit_address; ?></textarea>
                </label>

            </div>

            <div class="medium-6 column">

                <label for="no_tender"> Tanggal Buat
                    <input type="text" value="<?php echo $date_created; ?>" disabled/>
                </label>

                <label for="no_tender"> Kuantitas
                    <input type="text" value="<?php echo $order_quantity; ?>" disabled/>
                </label>

                <label for="no_tender"> Total
                    <input type="text" value="<?php echo $order_total; ?>" disabled/>
                </label>

                <label for="no_tender"> Diproses ?
                    <input type="text" value="<?php echo $is_processed ? 'Iya' : 'Perlu diverifikasi Helpdesk';; ?>" disabled/>
                </label>

                <label for="no_tender"> Sudah selesai ?
                    <input type="text" value="<?php echo $is_completed ? 'Iya' : 'Masih dalam proses';; ?>" disabled/>
                </label>

                <label for="no_tender"> Notifikasi Pembeli
                    <textarea disabled><?php echo $description; ?></textarea>
                </label>

            </div>

        </div>

        <fieldset class="fieldset">

            <div class="row columns">
                <div class="column">
                    <p>Dibuat oleh <?php echo $created_user; ?> pada tanggal
                        <?php echo mdate($datestring, mysql_to_unix($created_date)); ?>.</p>
                    <br>
                    <p>Dan terakhir diperbarui oleh <?php echo $updated_user; ?> pada tanggal
                        <?php echo mdate($datestring, mysql_to_unix($updated_date)); ?></p>
                </div>
            </div>

        </fieldset>

    </fieldset>



    <div class="column">
        <table class="stacked">
            <tr><td></td><td><a href="<?php echo site_url('order') ?>" class="btn btn-default">
                        <i class="fi-arrow-left"></i> Kembali</a></td></tr>
        </table>
    </div>

</div>

<section id="cd-timeline" class="cd-container">

    <?php foreach ($status as $stat) { ?>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-picture">
            <img src="<?php echo base_url('assets/css/font-icon/svgs/fi-sound.svg'); ?>">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
            <p><?php echo $stat->operation; ?></p><br>
            <p>Oleh : <b><?php echo $stat->updated_user; ?></b></p>
            <span class="cd-date"><?php echo mdate($datestring, mysql_to_unix($stat->updated_date)); ?></span>
        </div> <!-- cd-timeline-content -->
    </div> <!-- cd-timeline-block -->
   <?php } ?>
</section> <!-- cd-timeline -->

