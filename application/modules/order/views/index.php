<h3 style="margin-top:0px">Daftar Tender</h3>

<div class="row">
    <div class="small-6 large-4 column">
        <?php if ($has_add_tender) {?>
        <i class="fi-plus"></i> <?php echo anchor(site_url('order/create'),'Tambahkan Tender Baru', 'class="btn btn-primary"'); ?>
        <?php } ?>
    </div>

    <div class="small-6 large-4 column">
        <?php if($has_see_total_summary) { ?>
        <h3>
            <?php echo isset($total_price) && $total_price > 0 ? anchor(site_url('order/total'), 'Total :') : 'Total:'; ?>
            <?php echo 'Rp '. number_format($total_price, 0, '','.'); ?>
        </h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="small-6 medium-3 large-3 columns">
        <form action="<?php echo site_url('order/index'); ?>" class="form-inline" method="get">
            <div class="input-group">

                <span class="input-group-label"><i class="fi-magnifying-glass" style="font-size: larger"></i> </span>

                <input type="text" class="input-group-field" name="q" value="<?php echo $q; ?>" placeholder="Cari No. Tender..." autocomplete="off">

                <div class="input-group-button">
                    <input type="submit" class="button" value="Cari">
                </div>

            </div>
        </form>
    </div>

    <?php if($has_filter_helpdesk) { ?>
    <div class="small-6 medium-2 large-2 columns">
        <select name="helpdesk_filter" id="helpdesk_filter">
            <option selected disabled>-- By Helpdesk --</option>
            <?php foreach ($helpdesk_list as $helpdesk_) { ?>
            <option value="<?=$helpdesk_->id;?>" <?php echo $helpdesk_->id == $helpdesk_filter_selected ? 'selected' : '';?>><?=ucwords($helpdesk_->first_name);?></option>
            <?php }?>
        </select>
    </div>
    <?php } ?>

    <?php if($has_filter_status) { ?>
    <div class="small-6 medium-2 large-2 columns">
        <select name="status_filter" id="status_filter">
            <option selected disabled>-- By Status --</option>
            <?php foreach($status_filter_option as $key => $value) { ?>
                <option value="<?=$key;?>" <?php echo $key == $status_filter_selected ? 'selected' : '';?>>
                    <?=ucfirst($value);?>
                </option>
            <?php } ?>
        </select>
    </div>
    <?php } ?>

    <?php if($has_filter_date) { ?>
        <div class="small-6 medium-1 large-1 columns">
            <select name="month_filter" id="month_filter">
                <option selected disabled>-- Month --</option>
                <?php for ($i=0; $i<count($month_filter_option); $i++) { ?>
                    <option value="<?=$month_filter_option[$i];?>" <?php echo $month_filter_option[$i] == $month_filter_selected ? 'selected' : '';?>>
                        <?=$month_filter_option[$i];?>
                    </option>
                <?php } ?>
            </select>
        </div>
    <?php } ?>

    <?php if($has_filter_date) { ?>
    <div class="small-6 medium-1 large-1 columns">
        <select name="year_filter" id="year_filter">
            <option selected disabled>-- Year --</option>
            <?php for ($i=0; $i<count($year_filter_option); $i++) { ?>
                <option value="<?=$year_filter_option[$i];?>" <?php echo $year_filter_option[$i] == $year_filter_selected ? 'selected' : '';?>>
                    <?=$year_filter_option[$i];?>
                </option>
                <option value="2017">2017</option>
            <?php } ?>
        </select>
    </div>
    <?php } ?>

    <?php if($has_filter_location) { ?>
    <div class="small-6 medium-2 large-3 columns">
        <input type="search" name="location_filter" id='location_filter' placeholder="Filter by lokasi"
            value="<?=$location_filter_selected?>"/>
    </div>
    <?php } ?>

</div>

<div class="row">
    <div class="col-md-4 text-center">
        <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
    </div>

    <div class="col-md-1 text-right columns">
    </div>
</div>

<div class="row columns">

    <table class="hover columns">
        <thead>
        <tr>
            <th>No</th>
            <th>
                <span data-tooltip aria-haspopup="true"
                      class="has-tip top" data-disable-hover="false"
                      tabindex="2" title="Klik warna untuk melihat status.">
                    Status</span>
            </th>
            <th>
                <span data-tooltip aria-haspopup="true"
                      class="has-tip top" data-disable-hover="false"
                      tabindex="2" title="Posisi terakhir pemrosesan tender.">
                    Posisi</span>
            </th>
            <th>No. Tender</th>
            <th>ID Paket</th>
            <th>Kuantitas</th>
            <th>Total</th>
            <th>Tanggal Buat</th>
            <th>Lokasi Paket</th>
            <th>Pembeli</th>
            <th>Produk</th>
            <th>Helpdesk</th>
            <th>Sales</th>
            <th>SP</th>
            <th>SO</th>
            <th>Inv</th>
            <th>Paid</th>
            <th class="text-center">Aksi</th>
        </tr>
        </thead>
        <?php
        foreach ($order_data as $order)
        {
            $no_start = ++$start;
            ?>
            <tr>
                <td width="30px"><?php echo $no_start; ?></td>
                <td width="30px">
                    <div class="reveal" id="status-<?=$order->id;?>" data-reveal>
                        <div class="row columns">
                            <table class="hover column">
                                <thead>
                                <th>Tanggal</th>
                                <th>Remark</th>
                                <th>PIC</th>
                                </thead>
                                <?php if($order->remarks) : ?>
                                    <?php foreach($order->remarks as $remark): ?>
                                        <tr>
                                            <td id="remark_created_date-<?=$order->id . '-' . $remark->created_user->id; ?>">
                                                <?=mdate('%d %M %Y %h:%i %a', mysql_to_unix($remark->created_date));?>
                                            </td>
                                            <td id="remark_remark-<?=$order->id . '-' . $remark->created_user->id; ?>">
                                                <?=$remark->remark != '' ? $remark->remark : 'Belum ada remark.';?>
                                            </td>

                                            <td id="remark_created_user-<?=$order->id . '-' . $remark->created_user->id; ?>">
                                                <?=ucwords($remark->created_user->first_name);?> <b>(<?php echo $remark->created_user->group->name;?>)</b>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php else:  ?>
                                    <tr class="text-center">
                                        <td colspan="3">
                                            Belum ada remark.
                                        </td>
                                    </tr>
                                <?php endif; ?>

                            </table>
                        </div>
                        <?php if ($has_add_remark) { ?>
                        <div class="row columns">
                            <div class="small-9 medium-9 large-9 columns">
                                <textarea hidden id="remark_add_text-<?=$order->id;?>" placeholder="Tambah catatan..."></textarea>
                            </div>

                            <div class="small-3 medium-3 large-3 columns">
                                <input type="button" id="remark_add-<?=$order->id;?>" class="hollow secondary button" value="Add remark" />
                            </div>
                        </div>
                        <?php } ?>

                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <a data-open="status-<?=$order->id;?>">
                    <div class="small-1 medium-1 large-1 column"
                        <?php
                            switch ($order->step->step) {
                                case 'helpdesk':
                                    echo 'style="background-color: #000;"';
                                    break;
                                case 'adminso':
                                    echo 'style="background-color: #663300;"';
                                    break;
                                case 'purchasing':
                                    echo 'style="background-color: #ffff00;"';
                                    break;
                                case 'warehouse':
                                    echo 'style="background-color: #3399ff;"';
                                    break;
                                case 'admindo':
                                    echo 'style="background-color: #6600cc;"';
                                    break;
                                case 'delivery':
                                    echo 'style="background-color: #0000cc;"';
                                    break;
                                case 'admininv':
                                    echo 'style="background-color: #53ff1a;"';
                                    break;
                                case 'finance':
                                    echo 'style="background-color: #009933;"';
                                    break;
                                case 'cancel':
                                    echo 'style="background-color: #FF000F;"';
                                    break;
                                case 'completed':
                                    echo 'style="background-color: #009933;"';
                                    break;
                            }

                        ?>>
                        &nbsp;</div></a>
                </td>
                <td>
                    <?php

                    switch ($order->step->step)
                    {
                        case 'adminso':
                            $order->step->step = 'admin SO';
                            break;
                        case 'admindo':
                            $order->step->step = 'admin DO';
                            break;
                        case 'admininv':
                            $order->step->step = 'admin inv';
                            break;
                    }
                    echo ucwords($order->step->step);
                    ?>
                </td>
                <td width="85px" id="order_number-<?=$order->id; ?>"
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                    <?php echo $order->order_number ?>
                </td>
                <td width="150px" id="order_name-<?=$order->id; ?>"
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                    <?=$order->order_name; ?>
                </td>
                <td width="85px" id="order_quantity-<?=$order->id; ?>"
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                    <?php echo $order->order_quantity ?>
                </td>
                <td id="order_total-<?=$order->id; ?>">
                    <?php echo number_format($order->order_total, 0, '', '.') ?>
                </td>
                <td width="100px" id="date_created" order_id="<?=$order->id; ?>"
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                    <?php echo mdate('%d %M %Y', mysql_to_unix($order->date_created)); ?>
                </td>

                <td width="30px">
                    <div class="tiny reveal" id="package_location-<?=$order->id;?>" data-reveal>

                        <table class="hover column">
                            <thead>
                            <th>Lokasi Paket</th>
                            </thead>
                            <tr>
                                <td id="order_location-<?=$order->id; ?>"
                                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                    <?=$order->package_location; ?>
                                </td>
                            </tr>
                        </table>

                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <a data-open="package_location-<?=$order->id;?>">Lihat</a>
                </td>

                <td>
                    <div class="tiny reveal" id="pembeli-<?=$order->id;?>" data-reveal>

                        <div class="row column">
                            <table class="hover column">
                                <thead>
                                    <th>Satuan Kerja</th>
                                    <th>Alamat</th>
                                    <th>Catatan</th>
                                </thead>
                                <tr>
                                    <td id="order_unit-<?=$order->id; ?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->work_unit; ?>
                                    </td>
                                    <td id="order_unit_address-<?=$order->id; ?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->work_unit_address; ?>
                                    </td>
                                    <td id="order_description-<?=$order->id; ?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?php echo $order->description != null ? $order->description : 'Tidak ada catatan.' ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <hr/>

                        <div class="row column">
                            <p>Pemesan</p>
                            <table class="hover column">
                                <thead>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telp</th>
                                </thead>
                                <tr>
                                    <td id="pemesan_name-<?=$order->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->pemesan->name; ?>
                                    </td>
                                    <td id="pemesan_email-<?=$order->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->pemesan->email; ?>
                                    </td>
                                    <td id="pemesan_phone-<?=$order->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->pemesan->phone; ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <hr/>

                        <div class="row column">
                            <p>Pembeli</p>
                            <table class="hover column">
                                <thead>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>NIP</th>
                                </thead>
                                <tr>
                                    <td id="pembeli_name-<?=$order->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->pembeli->name; ?>
                                    </td>
                                    <td id="pembeli_title-<?=$order->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->pembeli->title; ?>
                                    </td>
                                    <td id="pembeli_nip-<?=$order->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$order->pembeli->nip; ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <a data-open="pembeli-<?=$order->id;?>">Lihat</a>
                </td>

                <td>
                    <div class="large reveal" id="produk-<?=$order->id;?>" data-reveal>

                        <table class="hover column">
                            <thead>
                                <th>Nama Produk</th>
                                <th>Harga</th>
                                <th>Kuantitas</th>
                                <th>Ongkir</th>
                                <th>Subtotal</th>
                                <th>PO</th>
                                <th>Diterima gudang?</th>
                                <th>DO</th>
                                <th>Sudah dikirim ?</th>
                            </thead>
                            <?php foreach($order->produk as $item): ?>
                                <tr>
                                    <td id="product_name-<?=$order->id;?>-<?=$item->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$item->product_name; ?>
                                    </td>

                                    <td id="product_price-<?=$order->id;?>-<?=$item->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=number_format($item->price, 0, '', '.');?>
                                    </td>

                                    <td width="30px" id="product_quantity-<?=$order->id;?>-<?=$item->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=$item->quantity;?>
                                    </td>

                                    <td id="product_shipping_cost-<?=$order->id;?>-<?=$item->id;?>"
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                                        <?=number_format($item->shipping_cost, 0, '', '.');?>
                                    </td>

                                    <td id="product_subtotal-<?=$order->id;?>-<?=$item->id;?>">
                                        <?=number_format($item->subtotal, 0, '', '.');?>
                                    </td>

                                    <td id="product_po-<?=$order->id;?>-<?=$item->id;?>"
                                        <?php if ($item->po_number != null){ ?>
                                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : '' ; ?>
                                        <?php } else { ?>
                                        <?=$has_add_po ? 'editable="true"' : '';?>
                                        <?php } ?>>
                                        <?=$item->po_number ? $item->po_number : '-'; ?>
                                    </td>

                                    <td id="product_is_received-<?=$order->id;?>-<?=$item->id;?>"
                                        <?php if ($item->is_received) { ?>
                                        <?=$has_change_tender && $item->is_received && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : '' ; ?>
                                        <?php } else { ?>
                                        <?=$has_add_received ? 'editable="true"' : ''; ?> <?=$item->is_received ? 'data-checked=true' : '';?>
                                        <?php } ?>>
                                        <?=$item->is_received ? '<i class="fi-check"></i> '.mdate('%d %M %Y %h:%i %a', mysql_to_unix($item->received_date)) : '-'; ?>
                                    </td>

                                    <td id="product_do-<?=$order->id;?>-<?=$item->id;?>"
                                        <?php if ($item->do_number != null){ ?>
                                            <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : '' ; ?>
                                        <?php } else { ?>
                                            <?=$has_add_do ? 'editable="true"' : '';?>
                                        <?php } ?>>
                                        <?=$item->do_number ? $item->do_number : '-'; ?>
                                    </td>
                                    <td id="product_is_send-<?=$order->id;?>-<?=$item->id;?>"
                                        <?php if ($item->is_send){ ?>
                                            <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : '' ; ?>
                                        <?php } else { ?>
                                            <?=$has_add_send ? 'editable="true"' : '';?>
                                        <?php } ?>>
                                        <?=$item->is_send ?
                                            '<i class="fi-check"></i> ' . $item->validation_number .'<br/> '. $item->courier . '<br/>' . mdate('%d %M %Y %h:%i %a', mysql_to_unix($item->send_date)) . '</span>' : '-'; ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>
                        </table>

                        <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <a data-open="produk-<?=$order->id;?>">Detail</a>
                </td>

                <td width="100px" id="order_helpdesk-<?=$order->id;?>-<?=$order->helpdesk->id;?>"
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                    <?=$order->helpdesk->first_name;?>
                </td>
                <td width="100px" id="order_sales-<?=$order->id;?>-<?=$order->sales->id;?>"
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>>
                    <?=$order->sales->first_name;?>
                </td>
                <td id="order_sp-<?=$order->id;?>"
                    <?php if ($order->sp_number){ ?>
                    <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>
                    <?php } else { ?>
                        <?=$has_add_sp ? 'editable="true"' : ''; ?>
                    <?php } ?> curr-val="<?=$order->sp_number;?>">
                    <?=$order->sp_number ? '<span
                        data-tooltip aria-haspopup="true"
                        class="has-tip" data-disable-hover="false"
                        tabindex="1" title="' .$order->sp_number. '" id="span-sp-'.$order->id.'">
                        <i class="fi-check"></i></span> ' : '<i class="fi-x"></i>';?>
                </td>
                <td id="order_so-<?=$order->id;?>"

                    <?php if ($order->so_number != ''){ ?>
                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>
                    <?php } else { ?>
                        <?=$has_add_so ? 'editable="true"' : ''; ?>
                    <?php } ?>  curr-val="<?=$order->so_number;?>">

                    <?=$order->so_number ? '<span
                        data-tooltip aria-haspopup="true"
                        class="has-tip" data-disable-hover="false"
                        tabindex="1" title="' .$order->so_number. '" id="span-so-'.$order->id.'">
                        <i class="fi-check"></i></span> ' : '<i class="fi-x"></i>';?>
                </td>
                <td id="order_inv-<?=$order->id;?>"

                    <?php if ($order->inv_number != ''){ ?>
                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>
                    <?php } else { ?>
                        <?=$has_add_inv ? 'editable="true"' : ''; ?>
                    <?php } ?> curr-val="<?=$order->inv_number;?>">

                    <?=$order->inv_number ? '<span
                        data-tooltip aria-haspopup="true"
                        class="has-tip" data-disable-hover="false"
                        tabindex="1" title="' .$order->inv_number. '" id="span-inv-'.$order->id.'">
                        <i class="fi-check"></i></span> ' : '<i class="fi-x"></i>';?>
                </td>

                <td id="order_paid-<?=$order->id;?>"
                    <?php if ($order->is_paid){ ?>
                        <?=$has_change_tender && !$order->is_completed && !$order->is_cancelled ? 'editable="true"' : ''; ?>
                    <?php } else { ?>
                        <?=$has_add_paid ? 'editable="true"' : ''; ?>
                    <?php } ?> curr-val="<?php echo mdate('%d %M %Y', mysql_to_unix($order->paid_date));?>">
                    <?=$order->is_paid ? '<span
                        data-tooltip aria-haspopup="true"
                        class="has-tip" data-disable-hover="false"
                        tabindex="1" title="' . mdate('%d %M %Y', mysql_to_unix($order->paid_date)). '" id="span-paid-'.$order->id.'">
                        <i class="fi-check"></i></span> ' : '<i class="fi-x"></i>';?>

                </td>
                <td style="text-align:center" width="70px">
                    <?php
                    /*echo anchor(site_url('order/read/'.$order->id),'<i title="Riwayat" class="fi-calendar"></i>');
                    echo ' | ';*/
                    echo $has_cancel_tender && !$order->is_completed && !$order->is_cancelled ? anchor(
                        site_url('order/cancel/'.$order->id),
                        '<i title="Cancel tender" class="fi-x-circle"></i>','onclick="javasciprt: return confirm(\'Apakah anda yakin untuk cancel tender ini ?\')"') : '';
                    ?>
                    <?php if ($has_print_precall && !$order->is_completed && !$order->is_cancelled) { ?>
                        | <a href="<?php echo site_url('order/precall/'. $order->id) ?>" target="_blank">
                            <i title="Print precall" class="fi-print"></i>
                        </a>
                    <?php } ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>

<div class="row columns">
    <div class="medium-6 columns">
        <a href="javascript::0" class="btn btn-primary">Total Tender : <?php echo $total_rows ?> Tender</a>
    </div>
    <div class="medium-6 columns">
        <?php echo $pagination ?>
    </div>
</div>

<script type="text/javascript">
    var base_url = '<?php echo base_url();?>';
</script>