<div class="row">

    <h4 style="margin-top:0px">Menambah Tender Baru</h4>

    <form action="<?php echo $action; ?>" method="post">

        <div class="row columns">

            <div class="small-12 medium-8 columns">
                <h4>Informasi Umum</h4>
                <?php if ($this->session->userdata('message')) {?>

                <span class="warning label text-right">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </span>
                <?php } ?>
            </div>
            <hr>

            <div class="small-12 medium-5 columns">

                <div class="row">
                    <div class="small-3 columns">
                        <label for="order_number" class="text-right">No.Tender</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('order_number', '<span class="alert label">', '</span>'); ?>
                        <input type="number" name="order_number" id="order_number" placeholder="Nomor Tender"
                               value="<?php echo $order_number; ?>" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="order_name" class="text-right">ID Paket</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('order_name', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="order_name" id="order_name" placeholder="ID Paket"
                               value="<?php echo $order_name; ?>" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="instance" class="text-right">Instansi</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('instance', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="instance" id="instance" placeholder="Instansi"
                               value="<?php echo $instance; ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="package_location" class="text-right">Lokasi Paket</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('package_location', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="package_location" id="package_location" placeholder="Lokasi Paket"
                               value="<?php echo $package_location; ?>" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="work_unit" class="text-right">Satuan Kerja</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('work_unit', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="work_unit" id="work_unit" placeholder="Satuan Kerja"
                               value="<?php echo $work_unit; ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="work_unit" class="text-right">Alamat Satuan Kerja</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('work_unit_address', '<span class="alert label">', '</span>') ?>
                        <textarea name="work_unit_address" id="work_unit_address"
                                  placeholder="Alamat Satuan Kerja" required><?php echo $work_unit_address; ?></textarea>
                    </div>
                </div>

            </div>

            <div class="small-12 medium-6 columns">

                <div class="row">
                    <div class="small-3 columns">
                        <label for="date_created" class="text-left">Tanggal Buat</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('date_created', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="date_created" id="date_created" placeholder="Tanggal Buat"
                               value="<?php echo $date_created; ?>" class="fdatepicker" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="order_quantity" class="text-left">Jumlah Produk</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('order_quantity', '<span class="alert label">', '</span>') ?>
                        <input type="number" name="order_quantity" id="order_quantity" placeholder="Jumlah Produk"
                               value="<?php echo $order_quantity; ?>" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="order_helpdesk" class="text-left">Helpdesk</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('order_helpdesk', '<span class="alert label">', '</span>') ?>
                        <select name="order_helpdesk" required>
                            <?php foreach ($order_helpdesk as $helpdesk) { ?>
                                <option value="<?php echo $helpdesk->id; ?>">
                                    <?php echo ucwords($helpdesk->first_name . " " . $helpdesk->last_name); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="order_quantity" class="text-left">Sales</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('order_sales', '<span class="alert label">', '</span>') ?>
                        <select name="order_sales">
                            <?php foreach ($order_sales as $sales) { ?>
                                <option value="<?php echo $sales->id; ?>">
                                    <?php echo ucwords($sales->first_name . " " . $sales->last_name); ?>
                                </option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="order_quantity" class="text-left middle">Notifikasi oleh PPK/Pembeli</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('description', '<span class="alert label">', '</span>') ?>
                        <textarea name="description" id="description"><?php echo $description; ?></textarea>
                    </div>
                </div>

            </div>

        </div>

        <hr>

        <div class="row columns">

            <div class="small-12 medium-5 columns">

                <div class="small-12 medium-5 columns">
                    <h4> (PP/Pemesan)</h4>
                </div>
                <hr>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="pemesan_name" class="text-right">Nama</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('pemesan_name', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="pemesan_name" id="pemesan_name"
                               placeholder="Nama" value="<?php if (isset($pemesan_name)) echo $pemesan_name; ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="pemesan_email" class="text-right">Email</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('pemesan_email', '<span class="alert label">', '</span>') ?>
                        <input type="email" name="pemesan_email" id="pemesan_email"
                               placeholder="Email" value="<?php if (isset($pemesan_email)) echo $pemesan_email; ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="pemesan_phone" class="text-right">No. Telp</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('pemesan_phone', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="pemesan_phone" id="pemesan_phone"
                               placeholder="No. Telp" value="<?php if (isset($pemesan_phone)) echo $pemesan_phone; ?>" />
                    </div>
                </div>

                <input type="hidden" name="pemesan_id" id="pemesan_id"
                       value="<?php if (isset($pemesan_id)) echo $pemesan_id; ?>" />

            </div>

            <div class="small-12 medium-6 columns">
                <div class="small-12 medium-6 columns">
                    <h4>(PPK/Pembeli)</h4>
                </div>
                <hr>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="pembeli_name" class="text-left">Nama</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('pembeli_name', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="pembeli_name" id="pembeli_name" placeholder="Name"
                               value="<?php if (isset($pembeli_name)) echo $pembeli_name; ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="pembeli_title" class="text-left">Jabatan</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('pembeli_title', '<span class="alert label">', '</span>') ?>
                        <input type="text" class="form-control" name="pembeli_title" id="pembeli_title" placeholder="Jabatan"
                               value="<?php if (isset($pembeli_title)) echo $pembeli_title; ?>" />
                    </div>
                </div>

                <div class="row">
                    <div class="small-3 columns">
                        <label for="pembeli_nip" class="text-left">NIP</label>
                    </div>
                    <div class="small-9 columns">
                        <?php echo form_error('pembeli_nip', '<span class="alert label">', '</span>') ?>
                        <input type="text" name="pembeli_nip" id="pembeli_nip" placeholder="Nip"
                               value="<?php if (isset($pembeli_nip)) echo $pembeli_nip; ?>" />
                    </div>
                </div>

                <input type="hidden" name="pembeli_id" id="pembeli_id"
                       value="<?php if (isset($pembeli_id)) echo $pembeli_id; ?>" />

            </div>

        </div>

        <hr />

        <div class="row columns">
            <div class="small-12 medium-8 columns">
                <h4>Produk Tender</h4>
            </div>
            <hr>

            <div class="row columns" id="order_products">

                <fieldset class="fieldset" id="fieldset">
                    <legend>Produk 1</legend>

                    <div class="small-4 column">
                        <label for="product_name">Nama Produk
                            <?php echo form_error('product_name', '<span class="alert label">', '</span>') ?>
                            <input type="text" name="product_name[]" id="product_name"
                                   value="<?php if (isset($product_name)) echo $product_name; ?>" />
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_quantity">Kuantitas <?php echo form_error('product_quantity') ?>
                            <input type="number" name="product_quantity[]" id="product_quantity"
                                   value="<?php if (isset($product_quantity)) echo $product_quantity; ?>" />
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_price">Harga Satuan <?php echo form_error('price') ?>
                            <input type="number" name="product_price[]" id="product_price"
                                   value="<?php if (isset($product_price)) echo $product_price; ?>" />
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_shipping_cost">Ongkos Kirim
                            <?php echo form_error('product_shipping_cost') ?>
                            <input type="number" name="product_shipping_cost[]" id="product_shipping_cost"
                                   value="<?php if (isset($product_shipping_cost)) echo $product_shipping_cost; ?>" />
                        </label>
                    </div>

                    <div class="small-2 column">
                        <label for="product_subtotal">Subtotal
                            <?php echo form_error('product_subtotal') ?>
                            <input type="number" name="product_subtotal[]" id="product_subtotal"
                                   value="<?php if (isset($product_subtotal)) echo $product_subtotal; ?>" />
                        </label>
                    </div>
<!--
                    <div class="column">
                        <label for="product_description">Catatan Tambahan
                            <?php /*echo form_error('product_description') */?></label>
                        <input type="text" name="product_description[]" id="product_description"
                               placeholder="Catatan Tambahan"
                               value="<?php /*if (isset($product_description)) echo $product_description; */?>" />
                    </div>-->

                </fieldset>

            </div>

            <div class="row column">
                <button type="button" class=" secondary hollow button right" id="add_product">Tambah Produk Lagi</button>
            </div>

        </div>

        <hr />

        <div class="row">
            <div class="small-6 columns">
                <button type="submit" class="button right"><?php echo $button ?></button>
                <a href="<?php echo site_url('order') ?>" class="secondary button">Cancel</a>
            </div>

            <div class="small-3 columns">
                <label for="order_total" class="text-right"><b>Total Harga</b></label>
            </div>

            <div class="small-3 columns">
                    <?php echo form_error('order_total') ?>
                    <input type="number" name="order_total" id="order_total" placeholder="Total Harga"
                           value="<?php if (isset($order_total)) echo $order_total; ?>" />
            </div>

        </div>
        <hr>
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
    </form>
</div>

<script type="text/javascript">
    var base_url = "<?=base_url(); ?>";
</script>