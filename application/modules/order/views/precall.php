<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>
        <?=$page_title . " - " . $order_name;?> | Ayoocek.com
    </title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/foundation.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-icon/foundation-icons.css'); ?>">
</head>

<body class="page-A4">

<div class="row columns" style="margin-top: 5%">

    <div class="large-12 columns">

        <article>

            <div class="row">

                <div class="medium-4 columns">
                        <img class="thumbnail" width="200" src="<?php echo base_url('assets/img/logo.jpg'); ?>">
                </div>

                <div class="medium-8 columns">
                    <div class="small-7 column">
                        <p><?=$page_title . " - " . $order_name;?> | Ayoocek.com</p>
                    </div>
                    <div class="small-5 column">
                        <p class="text-right"> Tanggal Buat : <?=date("d M Y",mysql_to_unix($date_created));?></p>
                    </div>
                </div>

            </div>

            <hr/>

            <div class="row columns">

                <div class="small-6 columns">

                    <div class="row columns">
                        <div class="small-6 column">
                            <p> NO. TENDER</p>
                        </div>
                        <div class="small-6 column">
                            <p>: <?=$order_number;?></p>
                        </div>
                    </div>

                    <div class="row columns">
                        <div class="small-6 column">
                            <p> ID Paket</p>
                        </div>
                        <div class="small-6 column">
                            <p>: <?=$order_name; ?></p>
                        </div>
                    </div>

                    <div class="row columns">
                        <div class="small-6 column">
                            <p> Kuantitas </p>
                        </div>
                        <div class="small-6 column">
                            <p>: <?=$order_quantity;?></p>
                        </div>
                    </div>

                    <div class="row columns">
                        <div class="small-6 column">
                            <p> Total </p>
                        </div>
                        <div class="small-6 column">
                            <p>: Rp <?php echo number_format($order_total, 0, '', '.');?>,-</p>
                        </div>
                    </div>

                </div>

                <div class="small-6 columns">

                    <div class="row columns">
                        <div class="small-4 column">
                            <p> Lokasi Paket </p>
                        </div>
                        <div class="small-8 column">
                            <p>: <?=$package_location;?></p>
                        </div>
                    </div>

                    <div class="row columns">
                        <div class="small-4 column">
                            <p> Helpdesk </p>
                        </div>
                        <div class="small-8 column">
                            <p>: <?=$helpdesk;?></p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row columns">
                <table class="hover column">
                    <thead>
                        <tr>
                            <th> No</th>
                            <th> Nama Produk </th>
                            <th> Kuantitas </th>
                            <th> Harga Satuan </th>
                            <th> Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $i=1; ?>

                        <?php foreach ($products as $product) : ?>
                            <tr>
                                <td><?php echo $i++;?></td>
                                <td><?=$product->product_name; ?></td>
                                <td><?=$product->quantity; ?></td>
                                <td><?=$product->price; ?></td>
                                <td><?=$product->subtotal; ?></td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>

            <hr/>

            <div class="row columns">
                <p>Note :</p>
            </div>

        </article>

    </div>

</div>
<script src="<?php echo base_url('assets/js/vendor/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/foundation.min.js');?>"></script>

<script type="text/javascript">
    $(document).ready(function () {
        window.print();
    });
</script>
</body>

</html>