-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 20, 2016 at 12:54 AM
-- Server version: 5.5.48-cll
-- PHP Version: 5.4.31

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ayoocek_db`
--

--
-- Truncate table before insert `groups`
--

TRUNCATE TABLE `groups`;
--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'Superadmin', 'Super user that used to control the whole thing on the website.'),
(2, 'Helpdesk', 'Helpdesk Group'),
(3, 'Purchasing', 'Purchasing Group'),
(4, 'Warehouse', 'Warehouse Group'),
(5, 'Finance', 'Finance Group'),
(6, 'Admin', 'Admin Group'),
(7, 'Adminso', 'Adminso Group'),
(8, 'Admindo', 'Admindo Group'),
(9, 'Admininv', 'Admininv Group'),
(10, 'Sales', 'Sales Group'),
(11, 'Delivery', 'Delivery Group');

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, '9CAnMVdVJHIodoCkAxD9e.', 1268889823, 1461066013, 1, 'Super', 'Admin', 'Ayoocek', '0'),
(2, '203.174.14.182', '', '$2y$08$6/C7qWVZDfjKgEQCE25gWeCwQ8wAloxohoDCNEgVbqjMzLCUZdNI6', '', 'turyanti@ayooklik.com', NULL, NULL, NULL, NULL, 1459737288, 1460989493, 1, 'Turyanti', 'Ayooklik', 'Ayooklik', '123'),
(3, '203.174.14.182', '', '$2y$08$RkV.vD0fCdUBkj1yrOui6.y0bpwgkE3v0lrBLISNW8Zi.oedJg8OK', '', 'eka@ayooklik.com', NULL, NULL, NULL, NULL, 1459737333, 1461066074, 1, 'Eka', 'Ayooklik', 'Ayooklik', '123'),
(4, '203.174.14.182', '', '$2y$08$He5EE/Ik7r1NxORtRM5RHu2XdNBbqiUdfKEhdDO/MS5AXHZ32OpUW', '', 'bertha@ayooklik.com', NULL, NULL, NULL, NULL, 1459737378, 1461062474, 1, 'Bertha', 'Ayooklik', 'Ayooklik', '12345678'),
(5, '203.174.14.182', '', '$2y$08$SR3jr7VQB3jf/Ot14XN/XOJgrPqg0F29zJlt7B5ytiQQDuJdAF4yK', '', 'parlin@ayooklik.com', NULL, NULL, NULL, NULL, 1459737488, 1460183479, 1, 'Parlin', 'Ayooklik', 'Ayooklik', ''),
(6, '203.174.14.182', '', '$2y$08$6EwfUli1viQFiiewQ.rRheO8Is.yoxuWuAKpL4e4AbF/KlqU7I2MO', '', 'amanda@ayooklik.com', NULL, NULL, NULL, NULL, 1459737528, NULL, 1, 'Amanda', 'Ayooklik', 'Ayooklik', ''),
(7, '203.174.14.182', '', '$2y$08$qC0pU5YNX1UcEkXBEt2ynuIlYVRm3nh6ruekUbb0zDSl36vC2mF4G', '', 'wina@ayooklik.com', NULL, NULL, NULL, NULL, 1459737570, NULL, 1, 'Wina', 'Ayooklik', 'Ayooklik', ''),
(8, '203.174.14.182', '', '$2y$08$CqsSyDKymOxUMHWotKrcJO1YlwGGwXN/uGWohlndqytE8FIZgVuOm', '', 'fitria@ayooklik.com', NULL, NULL, NULL, NULL, 1459737665, NULL, 1, 'Fitria', 'Ayooklik', 'Ayooklik', ''),
(9, '203.174.14.182', '', '$2y$08$UeuVa6QmSOoU21DbCsYsV.r4EHg3MQmbouFSAYH5MhtpKn0NfeugS', '', 'devis@ayooklik.com', NULL, NULL, NULL, NULL, 1459737841, 1460186153, 1, 'Devis', 'Ayooklik', 'Ayooklik', ''),
(10, '203.174.14.182', '', '$2y$08$sMAdoPNVHsHteYy/2uFx.OK5XVjeRqGRZIZ7A92ZYF2Lnx7Uk8dF.', '', 'ocha@ayooklik.com', NULL, NULL, NULL, NULL, 1459737885, NULL, 1, 'Ocha', 'Ayooklik', 'Ayooklik', ''),
(11, '203.174.14.182', '', '$2y$08$t/G.Ejc0S3Rb08ehnSexSu45mUWMOffDsNbtkUEFbsTcfyFw42u9O', '', 'tari@ayooklik.com', NULL, NULL, NULL, NULL, 1459737919, NULL, 1, 'Tari', 'Ayooklik', 'Ayooklik', ''),
(12, '203.174.14.182', '', '$2y$08$CP3Meo3QLtT2N3dpbWmF6.7HPw1A1onb7LKbtiuK7yep755gRbwP.', '', 'tiwi@ayooklik.com', NULL, NULL, NULL, NULL, 1459737972, 1460186322, 1, 'Tiwi', 'Ayooklik', 'Ayooklik', ''),
(13, '203.174.14.182', '', '$2y$08$tdalvxnEIwPrTCbUepr9R.AKiyG9iHn93IvOmAkAsdEJrFUJExiRq', '', 'iwan@ayooklik.com', NULL, NULL, NULL, NULL, 1459738055, 1460186260, 1, 'Iwan', 'Ayooklik', 'Ayooklik', ''),
(14, '203.174.14.182', '', '$2y$08$jtNUWLvIviSxvpmtiHd1qOpEj68h6BrObcA0BXh/zqYhH/UDKAF2e', '', 'pray@ayooklik.com', NULL, NULL, NULL, NULL, 1459738103, 1460186405, 1, 'Pray', 'Ayooklik', 'Ayooklik', ''),
(15, '203.174.14.182', '', '$2y$08$ODB7m23oBL6u0LoCixtZc.wtWMOnishdHhNbWE3njoU1ZH3FrhE4u', '', 'fauzi@ayooklik.com', NULL, NULL, NULL, NULL, 1459738149, 1460187570, 1, 'Fauzi', 'Ayooklik', 'Ayooklik', ''),
(16, '203.174.14.182', '', '$2y$08$pY6kk21sHdhg7nBgqftnbekdA8De8fJprIaGyysGDxS7kGgmTUHta', '', 'dillah@ayooklik.com', NULL, NULL, NULL, NULL, 1459738179, 1460187553, 1, 'Dillah', 'Ayooklik', 'Ayooklik', ''),
(17, '203.174.14.182', '', '$2y$08$M7/pDHvwP4fh3Po.KoAqluQ1GIzedN09EPHq7iwSd9y/sqVN8/xJG', '', 'ali@ayooklik.com', NULL, NULL, NULL, NULL, 1459738209, NULL, 1, 'Ali', 'Ayooklik', 'Ayooklik', ''),
(18, '203.174.14.182', '', '$2y$08$cquPVw02Puq9It4YEzkdq.Sq4qlDuT8ySgzCXJx8blyac6QarpFJm', '', 'anwar@ayooklik.com', NULL, NULL, NULL, NULL, 1459738239, NULL, 1, 'Anwar', 'Ayooklik', 'Ayooklik', ''),
(19, '203.174.14.182', '', '$2y$08$P6C5Uu7jff1hDVY7e/GVa.Dvy5944XczW/UWPEWL9XVuS29MwhiTy', '', 'edy@ayooklik.com', NULL, NULL, NULL, NULL, 1459738294, NULL, 1, 'Edy', 'Ayooklik', 'Ayooklik', ''),
(20, '203.174.14.182', '', '$2y$08$BBDsRdUbQevIc/2N/k8W4uy9Ns0gWS69.13wQ0/eThSJS41SCNJ0.', '', 'ken@ayooklik.com', NULL, NULL, NULL, NULL, 1459738335, NULL, 1, 'Ken', 'Ayooklik', 'Ayooklik', ''),
(21, '203.174.14.182', '', '$2y$08$mjaCkADAOBbL14ad7QbCEuLOipF0pQdiZDjO7o7I45bXxhISvMDVa', '', 'jerry@ayooklik.com', NULL, NULL, NULL, NULL, 1459738371, NULL, 1, 'Jerry', 'Ayooklik', 'Ayooklik', ''),
(22, '203.174.14.182', '', '$2y$08$yk82ZsxHA1VWA27veUuVbuCdz.ZX4etcbfTts9l8uTsgHv7bl9Xp6', '', 'aswil@ayooklik.com', NULL, NULL, NULL, NULL, 1459738404, NULL, 1, 'Aswil', 'Ayooklik', 'Ayooklik', ''),
(23, '203.174.14.182', '', '$2y$08$m0oLnl4.X5uRS1Vlv99yB.bb1VMWvMuTOVxMlMtYn4besO2fVSLk6', '', 'hariyanto@ayooklik.com', NULL, NULL, NULL, NULL, 1459738451, NULL, 1, 'Hariyanto', 'Ayooklik', 'Ayooklik', ''),
(24, '203.174.14.182', '', '$2y$08$FilCTwTCqFONWRwiChQ1KOcB7MtzCP44yKcWJKvXBjyXJzYP/4QRm', '', 'basuki@ayooklik.com', NULL, NULL, NULL, NULL, 1459738561, NULL, 1, 'Basuki', 'Ayooklik', 'Ayooklik', ''),
(25, '203.174.14.182', '', '$2y$08$5PQOUi.WSGKnUQZmF94QF.H1XnFLmBZxlmrJnkMDPKWERFyMXR1k2', '', 'ine@ayooklik.com', NULL, NULL, NULL, NULL, 1460186569, 1460186720, 1, 'Ine', 'Ayooklik', 'Ayooklik', ''),
(26, '203.174.14.182', '', '$2y$08$ZHVKuNKoxjxcgmCj1jdWAO7ISqWgInMsfA/3fe9YctJebZTkwebKq', '', 'debby@ayooklik.com', NULL, NULL, NULL, NULL, 1460186615, 1460187600, 1, 'Debby', 'Ayooklik', 'Ayooklik', '');

--
-- Truncate table before insert `users_groups`
--

TRUNCATE TABLE `users_groups`;
--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(5, 5, 2),
(6, 6, 2),
(7, 7, 2),
(8, 8, 2),
(9, 9, 3),
(10, 10, 3),
(11, 11, 3),
(12, 12, 8),
(13, 13, 4),
(14, 14, 11),
(15, 15, 10),
(16, 16, 10),
(17, 17, 10),
(18, 18, 10),
(19, 19, 10),
(20, 20, 10),
(21, 21, 10),
(22, 22, 10),
(23, 23, 10),
(24, 24, 10),
(25, 25, 9),
(26, 26, 5),
(27, 2, 6),
(28, 4, 2),
(29, 3, 7);
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
