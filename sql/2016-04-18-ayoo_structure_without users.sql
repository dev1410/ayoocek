-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2016 at 08:47 AM
-- Server version: 5.6.28-1ubuntu3
-- PHP Version: 7.0.4-7ubuntu2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ayoocek_db`
--
CREATE DATABASE IF NOT EXISTS `ayoocek_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ayoocek_db`;

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `kota` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_number` mediumint(100) NOT NULL,
  `order_name` varchar(50) NOT NULL,
  `instance` varchar(50) NOT NULL,
  `package_location` varchar(50) DEFAULT NULL,
  `work_unit` varchar(50) DEFAULT NULL,
  `work_unit_address` varchar(256) DEFAULT NULL,
  `date_created` date NOT NULL,
  `order_quantity` mediumint(5) NOT NULL,
  `order_total` int(13) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT NULL,
  `is_completed` tinyint(1) DEFAULT NULL,
  `is_cancelled` tinyint(1) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user` mediumint(8) NOT NULL,
  `updated_user` mediumint(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `product_name` varchar(80) DEFAULT NULL,
  `quantity` mediumint(8) NOT NULL,
  `price` int(15) NOT NULL,
  `shipping_cost` int(15) NOT NULL,
  `subtotal` int(15) NOT NULL,
  `description` varchar(80) DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT NULL,
  `sp_number` varchar(30) DEFAULT NULL,
  `so_number` varchar(30) DEFAULT NULL,
  `po_number` varchar(30) DEFAULT NULL,
  `is_received` tinyint(1) DEFAULT NULL,
  `received_date` datetime DEFAULT NULL,
  `do_number` varchar(30) DEFAULT NULL,
  `is_send` tinyint(1) DEFAULT NULL,
  `validation_number` varchar(30) DEFAULT NULL,
  `courier` varchar(30) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `inv_number` varchar(30) DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT NULL,
  `paid_date` date DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_helpdesk`
--

DROP TABLE IF EXISTS `order_helpdesk`;
CREATE TABLE `order_helpdesk` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `helpdesk_id` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_negotiation`
--

DROP TABLE IF EXISTS `order_negotiation`;
CREATE TABLE `order_negotiation` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `buyer` varchar(80) NOT NULL,
  `buyer_date` datetime DEFAULT NULL,
  `supplier` varchar(30) NOT NULL,
  `supplier_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_pembeli`
--

DROP TABLE IF EXISTS `order_pembeli`;
CREATE TABLE `order_pembeli` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `name` varchar(80) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `nip` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_pemesan`
--

DROP TABLE IF EXISTS `order_pemesan`;
CREATE TABLE `order_pemesan` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `name` varchar(80) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_remark`
--

DROP TABLE IF EXISTS `order_remark`;
CREATE TABLE `order_remark` (
  `id` mediumint(9) NOT NULL,
  `order_id` mediumint(9) NOT NULL,
  `remark` varchar(200) DEFAULT NULL,
  `created_user` mediumint(9) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_sales`
--

DROP TABLE IF EXISTS `order_sales`;
CREATE TABLE `order_sales` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `sales_id` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
CREATE TABLE `order_status` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `operation` varchar(80) NOT NULL,
  `remark` varchar(120) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_user` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_step`
--

DROP TABLE IF EXISTS `order_step`;
CREATE TABLE `order_step` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `order_id` mediumint(8) NOT NULL,
  `step` varchar(80) DEFAULT NULL,
  `updated_user` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_number` (`order_number`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_helpdesk`
--
ALTER TABLE `order_helpdesk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_negotiation`
--
ALTER TABLE `order_negotiation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`);

--
-- Indexes for table `order_pembeli`
--
ALTER TABLE `order_pembeli`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`);

--
-- Indexes for table `order_pemesan`
--
ALTER TABLE `order_pemesan`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_id` (`order_id`);

--
-- Indexes for table `order_remark`
--
ALTER TABLE `order_remark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_sales`
--
ALTER TABLE `order_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_step`
--
ALTER TABLE `order_step`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `order_helpdesk`
--
ALTER TABLE `order_helpdesk`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_negotiation`
--
ALTER TABLE `order_negotiation`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_pembeli`
--
ALTER TABLE `order_pembeli`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_pemesan`
--
ALTER TABLE `order_pemesan`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_remark`
--
ALTER TABLE `order_remark`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `order_sales`
--
ALTER TABLE `order_sales`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `order_step`
--
ALTER TABLE `order_step`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
